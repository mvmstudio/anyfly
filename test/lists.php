<?
$stars = array(
    array(
        'STAR' => '0',
    ),
    array(
        'STAR' => '1',
    ),
    array(
        'STAR' => '2',
    ),
    array(
        'STAR' => '3',
        'CHECKED' => true
    ),
    array(
        'STAR' => '4',
        'CHECKED' => true
    ),
    array(
        'STAR' => '5',
    ),
);

$locations = array(
    array(
        'NAME' => 'Городской центр',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Недалеко от центра города',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Ж/д вокзал',
    ),
    array(
        'NAME' => 'Аэропорт',
    ),
    array(
        'NAME' => 'Морской порт',
    ),
    array(
        'NAME' => 'Пляж',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Сельская местность',
    ),
    array(
        'NAME' => 'Горы',
    ),
    array(
        'NAME' => 'Периферия',
    ),
);

$zones = array(
    array(
        'NAME' => strtolower('AEROPORTO CIAMPINO')
    ),
    array(
        'NAME' => strtolower('AEROPORTO FIUMICINO'),
    ),
    array(
        'NAME' => strtolower('AURELIO'),
    ),
    array(
        'NAME' => strtolower('COLOSSEO - MONTI'),
    ),
    array(
        'NAME' => strtolower('EUR - GARBATELLA'),
    ),
    array(
        'NAME' => strtolower('FLAMINIO - PARIOLI'),
    ),
    array(
        'NAME' => strtolower('MONTE MARIO - STADIO OLIMPICO'),
    ),
    array(
        'NAME' => strtolower('MONTESACRO - PIETRALATA'),
    ),
    array(
        'NAME' => strtolower('MONTEVERDE'),
    ),
);

$facilities = array(
    array(
        'NAME' => 'Интернет',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Фитнес'
    ),
    array(
        'NAME' => 'Парковка',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Беспроводной интернет',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Номера для некурящих'
    ),
    array(
        'NAME' => 'Сейф'
    ),
    array(
        'NAME' => 'Кондиционер',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Телевизор'
    ),
    array(
        'NAME' => 'Фен'
    ),
    array(
        'NAME' => 'Ресторан'
    ),
    array(
        'NAME' => 'Фитнес-центр'
    ),
    array(
        'NAME' => 'Прачечная'
    ),
    array(
        'NAME' => 'Спа и оздоровительный центр'
    ),
    array(
        'NAME' => 'Крытый бассейн'
    ),
    array(
        'NAME' => 'Открытый бассейн',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Номера/Удобства для гостей с ограниченными возможностями'
    ),
);


$food = array(
    array(
        'NAME' => 'Без питания (RO)',
    ),
    array(
        'NAME' => 'Завтрак (BB)',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Полупансион (HB)',
        'CHECKED' => true
    ),
    array(
        'NAME' => 'Полный пансион (FB)',
    ),
    array(
        'NAME' => 'Всё включено (AI)',
    ),
);


header('Content-Type: application/json');
if (isset($_GET['type'])) {
    $prep = array();
    if ($_GET['type'] == 'stars')
        $prep['list'] = $stars;
    if ($_GET['type'] == 'locations')
        $prep['list'] = $locations;
    if ($_GET['type'] == 'zones')
        $prep['list'] = $zones;
    if ($_GET['type'] == 'facilities')
        $prep['list'] = $facilities;
    if ($_GET['type'] == 'food')
        $prep['list'] = $food;
    echo json_encode($prep);
}
die();