<?
$protoTypes = array(
    array(
        'HOTEL' => array(
            'ID' => '1',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/eurostars-international-palace/',
            'BOOK_LINK' => '/italy/rome/eurostars-international-palace/book/',
            'NAME' => 'Eurostars International Palace',
            'ADDRESS' => 'Via Nazionale, 46 00184 Rome',
            'PICTURE' => '/i/demo/hl1.png',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Городской центр',
            'RATING' => '7,1',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue',
            'BOOK_INFO' => 'Бронировался более 6 раза за последнее время',
            'ROOMS_LEFT' => 'Осталось 5 номеров',
            'GPS' => array(
                'LAT' => '41.9002343678',
                'LNG' => '12.4923066701'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '2',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/eurostars-roma-aeterna/',
            'BOOK_LINK' => '/italy/rome/eurostars-roma-aeterna/book/',
            'NAME' => 'Eurostars Roma Aeterna',
            'ADDRESS' => 'Via Casilina, 125 00176',
            'PICTURE' => '/i/demo/hl2.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Городской центр',
            'RATING' => '9,3',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue',
            'FEEDBACK_COUNT' => mt_rand(20, 300) . ' отзыва',
            'BOOK_INFO' => '',
            'ROOMS_LEFT' => 'Осталось 2 номера',
            'GPS' => array(
                'LAT' => '41.888866',
                'LNG' => '12.5231936'
            ),
        ),
    ),
    array(
        'BANNER' => array(
            'NAME' => 'Share the journey',
            'PICTURE' => '/i/demo/b1.jpg',
            'LINK' => 'http://www.anyfly.ru',
            'TARGET' => '_blank'
        ),
    ),
    array(
        'HOTEL' => array(
            'ID' => '3',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/la-griffe-luxury/',
            'BOOK_LINK' => '/italy/rome/la-griffe-luxury/book/',
            'NAME' => 'La Griffe Luxury',
            'ADDRESS' => 'Via Nazionale, 13 00187',
            'PICTURE' => '/i/demo/hl3.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Городской центр',
            'RATING' => '3,8',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_orange', //АКЦИЯ
            'SPECIAL_OFFER_ITEM_CLASS' => 'e_hotel_item_special',
            'FEEDBACK_COUNT' => mt_rand(20, 300) . ' отзыва',
            'BOOK_INFO' => 'Бронировался более 2 раз за последнее время',
            'ROOMS_LEFT' => 'Осталось 2 номера',
            'SPECIAL_OFFER_ROOMS_LEFT_CLASS' => 'e_orange',
            'GPS' => array(
                'LAT' => '41.9014682788',
                'LNG' => '12.4942289256'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '4',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/bernini-bristol/',
            'BOOK_LINK' => '/italy/rome/bernini-bristol/',
            'NAME' => 'Bernini Bristol',
            'ADDRESS' => 'Piazza Barberini, 23 00187',
            'PICTURE' => '/i/demo/hl4.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Городской центр',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue',
            'FEEDBACK_COUNT' => mt_rand(20, 300) . ' отзыва',
            'BOOK_INFO' => 'Бронировался более 2 раз за последнее время',
            'ROOMS_LEFT' => 'Осталось 2 номера',
            'GPS' => array(
                'LAT' => '41.903778200000000',
                'LNG' => '12.489031300000000'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '5',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/mediterraneo/',
            'BOOK_LINK' => '/italy/rome/mediterraneo/book/',
            'NAME' => 'Mediterraneo',
            'ADDRESS' => 'Piazza Barberini, 23 00187',
            'PICTURE' => '/i/demo/hl5.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Городской центр',
            'RATING' => '4,8',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue', //АКЦИЯ
            'GPS' => array(
                'LAT' => '41.896431676800000',
                'LNG' => '12.495036195400000'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '6',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/flaminio-village-residence/',
            'BOOK_LINK' => '/italy/rome/flaminio-village-residence/book/',
            'NAME' => 'Flaminio Village Residence',
            'ADDRESS' => 'Via Flaminia Nuova, 821 00189',
            'PICTURE' => '/i/e/avatar.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Периферия',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue', //АКЦИЯ
            'GPS' => array(
                'LAT' => '41.956075358048516',
                'LNG' => '12.482442855834961'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '7',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/radisson-blu/',
            'BOOK_LINK' => '/italy/rome/radisson-blu/book/',
            'NAME' => 'Radisson Blu',
            'ADDRESS' => 'Via Filippo Turati 171 00185 Rome',
            'PICTURE' => '/i/demo/hl6.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Периферия',
            'RATING' => '4,7',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue', //АКЦИЯ
            'GPS' => array(
                'LAT' => '41.897461509100000',
                'LNG' => '12.504273895800000'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '8',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/massimo-azeglio/',
            'BOOK_LINK' => '/italy/rome/massimo-azeglio/book/',
            'NAME' => 'Massimo D\'Azeglio',
            'ADDRESS' => 'Via Cavour, 18 00184',
            'PICTURE' => '/i/demo/hl7.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Периферия',
            'RATING' => '5,2',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_orange', //АКЦИЯ
            'SPECIAL_OFFER_ITEM_CLASS' => 'e_hotel_item_special',
            'FEEDBACK_COUNT' => mt_rand(20, 300) . ' отзыва',
            'BOOK_INFO' => 'Бронировался более 2 раз за последнее время',
            'ROOMS_LEFT' => 'Осталось 2 номера',
            'SPECIAL_OFFER_ROOMS_LEFT_CLASS' => 'e_orange',
            'GPS' => array(
                'LAT' => '41.896497075500000',
                'LNG' => '12.495108121800000'
            ),
        )
    ),
    array(
        'HOTEL' => array(
            'ID' => '9',
            'COUNTRY' => 'Italy',
            'CITY' => 'Rome',
            'COUNTRY_RU' => 'Италия',
            'CITY_RU' => 'Рим',
            'LINK' => '/italy/rome/nord-nuova-roma/',
            'BOOK_LINK' => '/italy/rome/nord-nuova-roma/',
            'NAME' => 'Nord Nuova Roma',
            'ADDRESS' => 'Via G. Amendola, 3 00185',
            'PICTURE' => '/i/demo/hl8.jpg',
            'BEST_PRICE_STR' => number_format(mt_rand(20000, 2000000), 0, ',', ' '),
            'CURRENCY' => array(
                'CLASS' => 'e_ruble',
                'VALUE' => 'a'
            ),
            'LOCATION' => 'Периферия',
            'RATING' => '5,2',
            'STARS' => mt_rand(1, 5),
            'BTN_CLASS' => 'btn_book_l_blue',
            'FEEDBACK_COUNT' => mt_rand(20, 300) . ' отзыва',
            'BOOK_INFO' => 'Бронировался более 2 раз за последнее время',
            'ROOMS_LEFT' => 'Осталось 2 номера',
            'GPS' => array(
                'LAT' => '41.968180528500000',
                'LNG' => '12.534462042900000'
            ),
        )
    ),
);


$perPage = 8;

$bannersCount = 1;
$fullList = $protoTypes;
$length = sizeof($fullList);

$prepared = array();

if($_GET['page'] == 'gps') {
    $prepared = $protoTypes;
} else {
    $start = ($_GET['page']-1)*$perPage;
    $end = $start+$perPage;
    for($i = $start; $i < $end; $i++ ) {
        if(isset($fullList[$i])) {
            $prepared[] = $fullList[$i];
        }
    }
}

$totalPages = ceil($length/$perPage);
$hotels = array(
    'show' => $totalPages!=$_GET['page'] ? ($_GET['page']*$perPage)-$bannersCount : $length-$bannersCount,
    'total' => $length-$bannersCount,
    'per_page' => $perPage,
    'pages' => $totalPages,
    'list' => $prepared
);



header('Content-Type: application/json');
echo json_encode($hotels);
die();