<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/skin.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets:true});
    </script>
</head>
<body class="inner_page hotel_card_page hotels_list">
<input type="hidden" name="currency" value="RUB"/>
<input type="hidden" name="hotel_hash"
       value="<?=$_GET['hotel_hash'] ? $_GET['hotel_hash'] : 'eurostars-international-palace'?>"/>

<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->


<div class="main_wrapper">
<div class="printonly">
    <h3 class="e_mh" id="__location" style="margin-bottom: 9px">Рим, Италия</h3>
    <h4 class="e_mh" id="__days">18 марта — 4 апреля (17 ночей)</h4>
    <h4 class="e_mh" id="__people">2 взрослых, 3 детей</h4>
    <h4 class="e_mh" id="__nights_count_text" style="display: none">17 ночей</h4>

</div>
<div class="page_header noprint">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                   class="btn btn_facebook_white">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                   class="btn btn_vkontakte_white">Vkontakte</a>
            </div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left subsribe_block">
                <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                   data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
            </div>

            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link" data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                     class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<? include('includes/book_form_inner.php') ?>


<div class="index_content_block e_texture_black">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">

                <div class="hotel_card_wrapper">
                    <div class="top">
                        <div class="e_left e_back_to noprint"><a href="/italy/rome/" class="e_bl w_icon e_wicon e_back_to_dark"><i></i>Вернуться
                            к результатам поиска</a> <span></span></div>


                        <a href="#" class="e_rating_mini big">Рейтинг отеля <i>8,9</i></a>

                        <div class="e_rating_info e_bs_dark e_fs_s ">
                            <ul class="e_flat_list">
                                <li class="e_floatable">
                                    <p class="e_left">Чистота</p>

                                    <p class="e_right value">9,0</p>
                                </li>
                                <li class="e_floatable">
                                    <p class="e_left">Комфорт</p>

                                    <p class="e_right value">8,8</p>
                                </li>
                                <li class="e_floatable">
                                    <p class="e_left">Расположение</p>

                                    <p class="e_right value">9,3</p>
                                </li>
                                <li class="e_floatable">
                                    <p class="e_left">Услуги</p>

                                    <p class="e_right value">8,4</p>
                                </li>
                                <li class="e_floatable">
                                    <p class="e_left">Персонал</p>

                                    <p class="e_right value">8,4</p>
                                </li>
                            </ul>
                            <p class="info e_ta_center">Оценка основана на отзывах и комментариях посетителей портала
                                системы AnyFly.</p>

                            <p class="e_ta_center noprint"><a href="#" class="lb_link e_aj" data-rel="feedback_window">Оценить
                                отель</a></p>

                            <div class="bottom"></div>
                        </div>

                        <div class="extra_links e_right">
                            <a class="e_wicon e_hotels_map_link_dark map_lb_link light noprint" href="#" data-rel="map_window">
                                <i></i><span class="e_aj">Отели на карте</span>
                            </a>
                            <a class="e_wicon e_hotels_share_link_dark light noprint" href="#">
                                <i></i><span class="e_aj">Поделиться с друзьями</span>
                            </a>

                            <div class="e_share_block e_bs_dark noprint">
                                <a href="#" class="lb_close lb_hide"></a>
                                <? $link = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] ?>
                                <ul class="e_flat_list e_floatable">
                                    <li class="e_left"><a
                                            href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&amp;t.s=1&amp;st.comments=AnyFly.ru%20—%20Рим,%20Италия&amp;st._surl=<?=$link?>"
                                            target="_blank" title="Поделиться в Одноклассниках"
                                            class="btn btn_od_share">Поделиться в Одноклассниках</a></li>
                                    <li class="e_left"><a href="http://vkontakte.ru/share.php?url=<?=$link?>"
                                                          target="_blank" title="Поделиться вКонтакте"
                                                          class="btn btn_vk_share">Поделиться вКонтакте</a></li>
                                    <li class="e_left"><a href="http://www.facebook.com/sharer.php?u=<?=$link?>"
                                                          target="_blank" title="Поделиться в Facebook"
                                                          class="btn btn_fb_share">Поделиться на Facebook</a></li>
                                    <li class="e_left"><a
                                            href="http://twitter.com/share?text=AnyFly.ru%20—%20Рим,%20Италия%20—&amp;url=<?=$link?>"
                                            title="Добавить в Twitter" target="_blank"
                                            class="btn btn_tw_share">Твитнуть</a></li>
                                    <li class="e_left"><a href="https://plus.google.com/share?url=<?=$link?>"
                                                          title="Поделиться в Google Plus" target="_blank"
                                                          class="btn btn_gg_share">Поделиться в Google Plus</a></li>
                                </ul>
                                <input type="text" class="e_text_field e_fs_m" id="share_link" value="<?=$link?>"/>
                                <a href="#" class="btn btn_copy_s" id="copy_share_btn">Скопировать</a>
                            </div>
                            <a class="e_wicon e_hotels_print_link_dark light noprint" href="#"
                               onclick="javascript:window.print()">
                                <i></i><span class="e_aj">Распечатать</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="middle">
                        <div class="e_left main_info">
                            <div class="e_stars_line big e_left">4</div>
                            <div class="hotel_category e_left bold e_fs_xl">
                                <span>Туристический отель высшего уровня</span></div>
                            <div class="clearfix"></div>
                            <h2 class="e_mh e_bliss_light">The Inn at the Roman Forum — Small Luxury Hotels</h2>

                            <div class="address e_hotel_map_focus" data-rel="map_window" data-hid="2">
                                <i></i><span class="e_aj">Via Nazionale, 46 00184 Rome</span>
                            </div>
                        </div>
                        <div class="e_right main_facilities">
                            <ul class="e_right e_flat_list e_facilities_icons">
                                <li class="e_left" style="background-image: url('/i/demo/srv/parking.png')">
                                    <img src="/i/demo/srv/parking_print.png" class="printonly"/>
                                    <i>Парковка</i></li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/wifi.png')">
                                    <img src="/i/demo/srv/wifi_print.png" class="printonly"/>
                                    <i>Wi-Fi</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/food.png')">
                                    <img src="/i/demo/srv/food_print.png" class="printonly"/>
                                    <i>Обед</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/gym.png')">
                                    <img src="/i/demo/srv/gym_print.png" class="printonly"/>
                                    <i>Фитнес</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/parking.png')">
                                    <img src="/i/demo/srv/parking_print.png" class="printonly"/>
                                    <i>Парковка</i></li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/gym.png')">
                                    <img src="/i/demo/srv/gym_print.png" class="printonly"/>
                                    <i>Фитнес</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/parking.png')">
                                    <img src="/i/demo/srv/parking_print.png" class="printonly"/>
                                    <i>Парковка</i></li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/food.png')">
                                    <img src="/i/demo/srv/food_print.png" class="printonly"/>
                                    <i>Обед</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/wifi.png')">
                                    <img src="/i/demo/srv/wifi_print.png" class="printonly"/>
                                    <i>Wi-Fi</i>
                                </li>
                                <li class="e_left" style="background-image: url('/i/demo/srv/gym.png')">
                                    <img src="/i/demo/srv/gym_print.png" class="printonly"/>
                                    <i>Фитнес</i>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="second_info">
                        <div class="e_left e_fs_x location">Расположение: <strong>Городской центр</strong></div>
                        <div class="e_right noprint"><a href="#description" class="e_aj">Описание</a>

                            <a href="#feedback"
                                                                                                class="e_aj feedback_anchor"></a></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="gallery invisible noprint">
                        <ul id="hotelcardgal" class="e_gallery jcarousel-skin-tango">
                            <li><img src="/i/demo/card/1.jpg" alt="" width="221" height="160"/></li>
                            <li><img src="/i/demo/card/2.jpg" alt="" width="234" height="160"/></li>
                            <li><img src="/i/demo/card/3.jpg" alt="" width="107" height="160"/></li>
                            <li><img src="/i/demo/card/4.jpg" alt="" width="240" height="160"/></li>
                            <li><img src="/i/demo/card/5.jpg" alt="" width="221" height="160"/></li>
                            <li><img src="/i/demo/card/6.jpg" alt="" width="233" height="160"/></li>
                            <li><img src="/i/demo/card/7.jpg" alt="" width="221" height="160"/></li>
                            <li><img src="/i/demo/card/8.jpg" alt="" width="240" height="160"/></li>
                            <li><img src="/i/demo/card/9.jpg" alt="" width="240" height="160"/></li>
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>


        </div>
    </div>
</div>
<div class="hotel_card_numbers e_texture_gray">
<div class="content_block">
<div class="index_wrapper">
<div class="wrapper_content">
<h3 class="e_mh e_mh_striked e_ts_white e_bliss_light e_ta_center">
    <span>Все номера</span></h3>

<div class="numbers_list">
<div class="e_numbers_block single e_bs_dark special_offer">
    <i class="e_action_s"></i>

    <div class="e_left list">
        <div class="num_line e_floatable">
            <div class="description e_left"><span
                    class="e_bliss e_fs_xxxl">Executive Junor Suite</span><i
                    class="num_left e_fs_m e_orange">Осталось 5 номеров</i></div>
            <div class="people e_people_count e_right invisible">2</div>
        </div>
        <div class="num_line e_floatable">
            <div class="description e_left"><span
                    class="e_bliss e_fs_xxxl">Executive Junor Suite</span></div>
            <div class="people e_people_count e_right invisible">3</div>
        </div>
        <div class="num_line e_floatable">
            <div class="description e_left"><span
                    class="e_bliss e_fs_xxxl">Executive Junor Suite</span></div>
            <div class="people e_people_count e_right invisible">1</div>
        </div>
        <div class="book_options">
            <div class="food_options e_left e_fs_x">
                Включены завтрак и ужин (HB)
            </div>
            <div class="fast_book e_right">
                <a href="#" class="e_aj e_tip">Отель включен в обслуживание Быстрого Бронирования<i
                        style="left: -20px">Отель Быстрого бронирования, в момент резервации по<br>запросу
                    будет подтвержден или отклонен в течение 30 минут!</i></a>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <div class="e_right price">
        <div class="total">
            <span class="nights_count">За 5 ночей</span>
            <i class="e_bliss_bold e_fs_big price_tag"><a href="#" class="e_aj">932 705</a><span
                    class="e_ruble">a</span></i>

            <div class="e_price_details e_bs_dark">
                <span class="total_word">Итого</span>
                <ul class="e_flat_list e_bold">
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">2</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 100 000 <span
                                    class="e_ruble">j</span></div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">3</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 10 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">1</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 30 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">150 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                </ul>
                <div class="vat_includes e_ta_center e_fs_s">Цены включают в себя: Налоги, завтрак и ужин.</div>
                <div class="btns e_ta_center">
                    <a href="#" class="e_aj close">Закрыть</a>
                </div>
            </div>
        </div>
        <div class="buttons">
            <a href="<?=$_SERVER['REQUEST_URI']?>book/"
               class="btn btn_book_l_orange">Забронировать</a>
        </div>
        <div class="e_available_fast">Немедленное подтверждение</div>
    </div>
    <div class="clearfix"></div>
</div>


<div class="e_numbers_block single e_bs_dark">
    <div class="e_left list">
        <div class="num_line e_floatable">
            <div class="description e_left"><span
                    class="e_bliss e_fs_xxxl">2x Junior Suite Room</span></div>
            <div class="people e_people_count e_right invisible">2</div>
            <div class="num_count e_bliss e_fs_big e_right invisible">2x</div>
        </div>
        <div class="num_line e_floatable">
            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Room</span><i
                    class="num_left e_fs_m e_red">Остался 1 номер</i></div>
            <div class="people e_people_count e_right invisible">1</div>
        </div>
        <div class="num_line e_floatable">
            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Suite</span>
            </div>
            <div class="people e_people_count e_right invisible">1</div>
        </div>
        <div class="book_options">
            <div class="food_options e_left e_fs_x">
                Включён завтрак (BB)
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <div class="e_right price">
        <div class="total">
            <span class="nights_count">За 5 ночей</span>
            <i class="e_bliss_bold e_fs_big price_tag"><a href="#" class="e_aj">23 270</a><span
                    class="e_ruble">a</span></i>

            <div class="e_price_details e_bs_dark">
                <span class="total_word">Итого</span>
                <ul class="e_flat_list e_bold">
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">2</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 100 000 <span
                                    class="e_ruble">j</span></div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">3</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 10 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">1</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 30 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">150 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                </ul>
                <div class="vat_includes e_ta_center e_fs_s">Цены включают в себя: Налоги, завтрак и ужин.</div>
                <div class="btns e_ta_center">
                    <a href="#" class="e_aj close">Закрыть</a>
                </div>
            </div>
        </div>
        <div class="buttons">
            <a href="<?=$_SERVER['REQUEST_URI']?>book/"
               class="btn btn_book_l_blue">Забронировать</a>
        </div>
        <div class="e_available_fast">Немедленное подтверждение</div>
    </div>
    <div class="clearfix"></div>
</div>


<div class="e_numbers_block single e_bs_dark">
    <div class="e_left list">
        <div class="num_line e_floatable">
            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Room</span><i
                    class="num_left e_fs_m e_red">Остался 1 номер</i></div>
            <div class="people e_people_count e_right invisible">1</div>
        </div>
        <div class="book_options">
            <div class="food_options e_left e_fs_x">
                Включён завтрак (BB)
            </div>
            <div class="fast_book e_right">
                <a href="#" class="e_aj e_tip">Отель включен в обслуживание Быстрого Бронирования<i
                        style="left: -20px">Отель Быстрого бронирования, в момент резервации по<br>запросу
                    будет подтвержден или отклонен в течение 30 минут!</i></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_right price">
        <div class="total">
            <span class="nights_count">За 5 ночей</span>
            <i class="e_bliss_bold e_fs_big price_tag"><a href="#" class="e_aj">23 270</a><span
                    class="e_ruble">a</span></i>

            <div class="e_price_details e_bs_dark">
                <span class="total_word">Итого</span>
                <ul class="e_flat_list e_bold">
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">2</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 100 000 <span
                                    class="e_ruble">j</span></div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">3</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 10 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Executive Junor Suite</div>
                            <div class="e_right e_people_count mini invisible">1</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 30 000 <span class="e_ruble">j</span>
                            </div>
                            <div class="e_right">150 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                </ul>
                <div class="vat_includes e_ta_center e_fs_s">Цены включают в себя: Налоги, завтрак и ужин.</div>
                <div class="btns e_ta_center">
                    <a href="#" class="e_aj close">Закрыть</a>
                </div>
            </div>
        </div>
        <div class="buttons">
            <a href="<?=$_SERVER['REQUEST_URI']?>book/"
               class="btn btn_book_l_blue">Забронировать</a>
        </div>
        <div class="e_available_fast">Немедленное подтверждение</div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="e_numbers_block single e_bs_dark special_offer">
    <i class="e_action_s"></i>

    <div class="e_left list">
        <div class="num_line e_floatable">
            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Deluxe Room</span></div>
            <div class="people e_people_count e_right invisible">2</div>
        </div>
        <div class="book_options">
            <div class="food_options e_left e_fs_x">
                Включён завтрак (BB)
            </div>
            <div class="fast_book e_right">
                <a href="#" class="e_aj e_tip">Отель включен в обслуживание Быстрого Бронирования<i
                        style="left: -20px">Отель Быстрого бронирования, в момент резервации по<br>запросу
                    будет подтвержден или отклонен в течение 30 минут!</i></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_right price">
        <div class="total">
            <span class="nights_count">За 5 ночей</span>
            <i class="e_bliss_bold e_fs_big price_tag"><a href="#" class="e_aj">18 270</a><span
                    class="e_ruble">a</span></i>

            <div class="e_price_details e_bs_dark">
                <span class="total_word">Итого</span>
                <ul class="e_flat_list e_bold">
                    <li>
                        <div class="epd_top e_floatable">
                            <div class="e_left">Deluxe Room</div>
                            <div class="e_right e_people_count mini invisible">2</div>
                        </div>
                        <div class="epd_bottom e_floatable">
                            <div class="e_left">5 <span class="nights_word">ночей</span> * 100 000 <span
                                    class="e_ruble">j</span></div>
                            <div class="e_right">100 000 <span class="e_ruble">j</span></div>
                        </div>
                    </li>
                </ul>
                <div class="vat_includes e_ta_center e_fs_s">Цены включают в себя: Налоги, завтрак и ужин.</div>
                <div class="btns e_ta_center">
                    <a href="#" class="e_aj close">Закрыть</a>
                </div>
            </div>
        </div>
        <div class="buttons">
            <a href="<?=$_SERVER['REQUEST_URI']?>book/"
               class="btn btn_book_l_orange">Забронировать</a>
        </div>
        <div class="e_available_fast">Немедленное подтверждение</div>
    </div>
    <div class="clearfix"></div>
</div>
</div>
<div class="e_help_line e_bs_dark e_bliss e_fs_xl">&mdash; Если вам нужна помощь в бронировании номера,
    позвоните по номеру <span class="e_orange">8 800 800 80 80 (Москва)</span> и <span class="e_orange">8 800 800 80 70 (Регионы)</span>.
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<div class="hotel_card_description e_texture_white" id="description">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">
                <div class="description_list">
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Главное</h4>

                        <div class="content">
                            <p>Парковка, интернет, кондиционер, ресторан.</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">В&nbsp;номере</h4>

                        <div class="content">
                            <p>Беспроводной интернет, телефон, фен, кондиционер.</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">В&nbsp;отеле</h4>

                        <div class="content">
                            <p>Прачечная, спа и&nbsp;оздоровительный центр, крытый бассейн, открытый бассейн,
                                фитнес-центр,
                                сейф, номера для курящих, номера/удобства для гостей с&nbsp;ограниченными физическими
                                возможностями.</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Описание отеля</h4>

                        <div class="content">
                            <p>Расположение отеля очень удобно: он&nbsp;находится в&nbsp;престижном районе, который
                                считается миланским центром моды, неподалеку от&nbsp;магазинов, ночных клубов и&nbsp;нескольких
                                остановок общественного транспорта, на&nbsp;котором&nbsp;Вы без труда сможете добраться
                                до&nbsp;исторического центра города. Nasco находится всего в&nbsp;300 метрах от&nbsp;выставочного
                                центра Milanocity неподалеку от&nbsp;главных шоссе&nbsp;— если&nbsp;Вы планируете
                                деловую командировку в&nbsp;Милан и&nbsp;если&nbsp;Вы цените отдых в&nbsp;изысканной и&nbsp;комфортной
                                атмосфере, это идеальный вариант для Вас. Типы номеров в&nbsp;отеле: Одноместные номера:
                                3; Двухместные номера с&nbsp;отдельными кроватями: 5: Трехместные номера: 4;
                                Четырехместные номера: 3. </p>
                        </div>
                    </div>


                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Порядок проживания</h4>

                        <div class="content">
                            <p>Начало регистрации: 14:00, портье: 24&nbsp;часа, услуги в&nbsp;номер: 24&nbsp;часа</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Номера</h4>

                        <div class="content">

                            <p>
                                <nobr>4-звездочное</nobr>
                                размещение, 118&nbsp;комн. для гостей, комнаты после косметического ремонта, элегантные
                                номера.
                            </p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Ресторан</h4>

                        <div class="content">
                            <p>L’uovo di&nbsp;Colombo restaurant: breakfast served from 7.00am to&nbsp;10.00am, lunch
                                from 12.30pm to&nbsp;2.00pm and dinner from 7.30pm to&nbsp;10.00pm.</p>
                        </div>
                    </div>

                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Расположение</h4>

                        <div class="content">
                            <p>Расположен перед Porta Venezia subway , отель находится в&nbsp;пешеходной
                                доступности от&nbsp;corso buenos aires (number&nbsp;3), торговая зона всего в&nbsp;нескольких
                                минутах
                                ходьбы.</p>
                        </div>
                    </div>

                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Общие</h4>

                        <div class="content">
                            <p>После полной реконструкции,
                                <nobr>4-звездочное</nobr>
                                размещение, вблизи от&nbsp;основных достопримечательностей, прекрасное сочетание
                                возможностей для работы и&nbsp;развлечений. Местные достопримечательности: duomo&nbsp;—
                                1&nbsp;миль , la&nbsp;scala opera house&nbsp;— 1&nbsp;миль , sforzesco castle&nbsp;— 2&nbsp;миль
                                , porta venezia&nbsp;— 5&nbsp;мин.
                            </p>
                        </div>
                    </div>


                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Зал</h4>

                        <div class="content">
                            <p>Среднего размера вестибюль меблирован в&nbsp;традиционном стиле. Стойка администратора
                                выполнена из&nbsp;дерева. Слева от&nbsp;нее приятное место для отдыха частично с&nbsp;красными
                                мягкими креслами и&nbsp;деревянными стульями с&nbsp;кофейными столами. Есть еще одно
                                место для отдыха в&nbsp;том&nbsp;же стиле с&nbsp;камином и&nbsp;картинами на&nbsp;стене.
                                В&nbsp;углу зала американский бар.</p>
                        </div>
                    </div>


                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Внешний вид</h4>

                        <div class="content">
                            <p>Ancient mansion</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Аэропорт</h4>

                        <div class="content">
                            <p>Fiumicino Airport, 39&nbsp;км, 25&nbsp;минут пешком</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Вокзал</h4>

                        <div class="content">
                            <p>3км, 30&nbsp;минут пешком</p>
                        </div>
                    </div>
                    <div class="e_desc_block dontsplit">
                        <h4 class="e_mh e_bliss_light">Метро</h4>

                        <div class="content">
                            <p>25&nbsp;минут пешком</p>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="attention">
                    <span class="e_orange">ВНИМАНИЕ:</span> Полная сумма платежа включает в&nbsp;себя стоимость
                    выбранного Вами номера за&nbsp;указанный Вами период проживания, а&nbsp;также все налоги и&nbsp;сборы,
                    кроме локальных (городских) налогов и&nbsp;сборов. Любые дополнительные услуги (Extras), такие как:
                    парковка, телефонные звонки, обслуживание в&nbsp;номерe и&nbsp;тд., оплачиваются гостями
                    непосредственно в&nbsp;отеле. Описание и&nbsp;фотографии отелей предоставлены самими отелями и/или
                    сетями отелей. Вся информация и&nbsp;визуальные материалы проверены, но&nbsp;<a
                        href="http://www.anyfly.ru/" class="e_sl">AnyFly.ru</a> не&nbsp;несет ответственности за&nbsp;их&nbsp;возможную
                    неточность или неполноту.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hotel_card_feedback e_texture_gray" id="feedback">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content" id="feedbackList">
                <h3 class="e_mh e_mh_striked e_ts_white e_bliss_light e_ta_center">
                    <span>Отзывы</span></h3>

                <div class="feedback_list">

                </div>
                <div class="clearfix"></div>
                <div class="controls">
                    <div class="e_left noprint e_texture_gray" style="padding-right: 22px;"><span
                            class="fb_count e_ts_white"></span></div>
                    <div class="feedback_btn_wrapper e_texture_gray">
                        <a href="#" data-rel="feedback_window" class="btn btn_feedback lb_link">Оставить отзыв</a>
                    </div>
                    <div class="e_right e_texture_gray noprint" style="padding-left: 22px;">
                        <a href="#" class="fb_show_more e_aj">Показать ещё</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="e_ta_center scrltotop">
                        <a href="#" class="e_ta_right e_aj e_scroll_to_top noprint">Подняться наверх</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>
<div class="lb-overlay" id="feedback_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>

        <div class="lb_content e_ta_left">
            <a href="#" class="e_rating_mini big noreact">Рейтинг отеля <i>8,9</i></a>

            <form method="post" action="/test/fb_post.php" class="ax_form" data-callback="on_feedback_success">
                <div class="top e_texture_gray">
                    <h3 class="e_mh_lb e_bliss_light e_ts_white e_ta_center">The Inn at the Roman Forum Small Luxury
                        Hotels</h3>
                    <div class="sliders">
                        <div class="slider">
                            <div class="label e_left"><label for="fb_cleanness">Чистота</label></div>
                            <div class="slider_wrapper e_left"><div class="slider_container"></div></div>
                            <div class="slider_alt e_left"><input type="text" name="cleanness" class="e_text_field" id="fb_cleanness" value="0" /></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_comfort">Комфорт</label></div>
                            <div class="slider_wrapper e_left"><div class="slider_container"></div></div>
                            <div class="slider_alt e_left"><input type="text" name="comfort" class="e_text_field" id="fb_comfort" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_location">Расположение</label></div>
                            <div class="slider_wrapper e_left"><div class="slider_container"></div></div>
                            <div class="slider_alt e_left"><input type="text" name="location" class="e_text_field" id="fb_location" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_service">Услуги</label></div>
                            <div class="slider_wrapper e_left"><div class="slider_container"></div></div>
                            <div class="slider_alt e_left"><input type="text" name="service" class="e_text_field" id="fb_service" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_personal">Персонал</label></div>
                            <div class="slider_wrapper e_left"><div class="slider_container"></div></div>
                            <div class="slider_alt e_left"><input type="text" name="personal" class="e_text_field" id="fb_personal" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="bottom e_texture_white">
                    <div class="comment_area">
                        <label for="fb_comment">Комментарий</label>
                        <textarea class="e_textarea" name="comment" id="fb_comment"></textarea>
                    </div>
                    <div class="buttons_area center">
                        <input type="submit" name="submit" value="Отправить" class="btn btn_send" maxlength="3" tabindex="10"/>

                        <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="lb-overlay" id="feedback_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Спасибо!</h3>

            <div class="lb_text">
                <p>Мы учли ваш голос.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>


<div class="lb-overlay" id="map_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Отели на карте</h3>
            <h4 class="e_fs_m e_nobold"></h4>

            <div class="map_area" id="map_window_map" style="height: 508px; width: 702px;">

            </div>
        </div>
    </div>
</div>


<script id="feedbackItem" type="text/html">
    {{#list}}
    <div class="e_hotel_feedback">
        <div class="author e_left">
            <div class="photo e_left"><img src="{{PHOTO}}" alt="{{NAME}}"/></div>
            <div class="creditionals e_left">
                <div class="date">{{DATE}}</div>
                <div class="name e_fs_xl e_bold e_ts_white">{{NAME}}</div>
                {{#POSITION}}
                <div class="position">{{POSITION}}</div>
                {{/POSITION}}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="text e_left e_bs_dark e_fs_x">
            <i class="pipe"></i>
            {{#RATING}}<a href="#" class="e_rating_mini maxi noreact"><i>{{RATING}}</i></a>{{/RATING}}
            <div class="wrap">
                <p {{#RATING}}class="wrating"{{/RATING}}>{{{TEXT}}}</p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {{/list}}
</script>

<script id="hotelInfobox" type="text/html">
    {{#list}}
    {{#HOTEL}}
    <div class="infobox-wrapper " data-hid="{{HOTEL.ID}}">
        <div class="infobox_content e_bs_dark" id="h_infobox_{{HOTEL.ID}}">
            <div class="ic_wrapper">
                <div class="top">
                    <div class="e_stars_line e_left">{{HOTEL.STARS}}</div>
                    {{#HOTEL.RATING}}<a href="{{HOTEL.LINK}}" class="e_rating_mini">{{HOTEL.RATING}}</a>{{/HOTEL.RATING}}
                    <div class="clearfix"></div>
                </div>
                <h4 class="e_mh e_bliss_medium"><a href="{{HOTEL.LINK}}">{{HOTEL.NAME}}</a></h4>

                <div class="middle">
                    <div class="e_left location">{{HOTEL.LOCATION}}</div>
                    {{#HOTEL.FEEDBACK_COUNT}}
                    <div class="e_right feedback_count"><a href="{{HOTEL.LINK}}#feedback" class="e_bl noprint">{{HOTEL.FEEDBACK_COUNT}}</a>
                    </div>
                    {{/HOTEL.FEEDBACK_COUNT}}

                    <div class="e_right"><a href="{{HOTEL.LINK}}" class="e_bl">Описание</a></div>
                    <div class="clearfix"></div>
                </div>

                <div class="photos e_floatable">
                    <img src="/i/demo/hib1.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib2.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib3.jpg" alt="" class="e_left">
                </div>
                <div class="buttons_area">
                    <div class="e_left price">
                        <div class="price e_bliss_medium e_fs_xxxl"><i>{{HOTEL.BEST_PRICE_STR}}</i><span
                                class="{{HOTEL.CURRENCY.CLASS}}">{{HOTEL.CURRENCY.VALUE}}</span></div>
                        <div class="nights e_fs_s">За <i>7 ночей</i></div>
                    </div>
                    <div class="e_right book">
                        <a href="{{HOTEL.BOOK_LINK}}" target="_blank" class="btn btn_book_s_blue">Забронировать</a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="pipe"></div>
        </div>
    </div>
    {{/HOTEL}}
    {{/list}}
</script>


<script id="filterStars" type="text/html">
    {{#list}}
    <label class="e_tf_checkbox_label e_tf_checkbox_label_top fc_stars"
           for="fc_stars_{{STAR}}"><input type="checkbox" name=""
                                          id="fc_stars_{{STAR}}"
                                          class="e_f_checkbox"{{#CHECKED}} checked="checked"{{/CHECKED}}/>
        <span>{{STAR}}</span></label>
    {{/list}}
</script>


<script id="filterOptionsList" type="text/html">
    {{#list}}
    <li><label class="e_tf_checkbox_label">
        <input type="checkbox" name="" value="{{ID}}" class="e_f_checkbox"{{#CHECKED}} checked="checked"{{/CHECKED}}>
        <span>{{NAME}}</span></label>
    </li>
    {{/list}}
</script>


<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/vendor/jquery.jcarousel.min.js"></script>
<script src="/js/vendor/jquery.columnizer.min.js"></script>

<script src="/js/lib/ax_controls.js?<?=time()?>"></script>
<script src="/js/lib/ax_lightbox.js?<?=time()?>"></script>
<script src="/js/ax_rules.js?<?=time()?>"></script>
<script src="/js/lib/ax_form.js?<?=time()?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


