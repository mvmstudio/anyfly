<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/skin.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets: true});
    </script>
</head>
<body class="inner_page hotel_card_page hotels_list book_page pay_page">
<input type="hidden" name="currency" value="RUB"/>
<input type="hidden" name="hotel_hash"
       value="<?= $_GET['hotel_hash'] ? $_GET['hotel_hash'] : 'eurostars-international-palace' ?>"/>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->

<div class="noprint " style="display: none">
    <h3 class="e_mh" id="__location" style="margin-bottom: 9px">Рим, Италия</h3>
    <h4 class="e_mh" id="__days">18 марта — 4 апреля (17 ночей)</h4>
    <h4 class="e_mh" id="__people">2 взрослых, 3 детей</h4>
    <h4 class="e_mh" id="__nights_count_text" style="display: none">17 ночей</h4>
</div>
<div class="main_wrapper">
<div class="page_header noprint">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                   class="btn btn_facebook_white">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                   class="btn btn_vkontakte_white">Vkontakte</a>
            </div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left subsribe_block">
                <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                   data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
            </div>

            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link"
                                                                                                      data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                     class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<div class="index_book_block noprint">

    <div class="content_block">
        <div class="e_left site_logo"><a href="/"><img src="/i/logo_mini.png" alt="AnyFly"/></a></div>
        <div class="e_phones_block e_ts_dark">
            <div class="e_floatable e_light_blue">
                <div class="e_left"><b class="e_white"><a href="tel:88008008080" class="phone">8 800 800 80
                            80</a></b><br>из Москвы
                </div>
                <div class="e_right"><b class="e_white"><a href="tel:88008008070" class="phone">8 800 800 80
                            70</a></b><br>из
                    России
                </div>
            </div>
        </div>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active" data-ref="/"><span>Отели</span><i></i>
                    </li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                    <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                </ul>
            </div>
            <form method="post" action="/italy/rome/">
                <div class="e_search_form_content">
                    <div class="e_search_line">

                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="book_bg"></div>
</div>


<div class="index_content_block e_texture_black">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">

                <div class="hotel_card_wrapper hotel_pay_numbers_info book_wrapper" style="min-height: 90px">
                    <div class="top">
                        <div class="e_left e_back_to noprint"><a
                                href="/italy/rome/eurostars-international-palace/"
                                class="e_bl w_icon e_wicon e_back_to_dark"><i></i>Вернуться
                                на страницу отеля</a></div>
                        <div class="extra_links e_right">
                            <a class="e_wicon e_hotels_print_link_dark light noprint" href="#"
                               onclick="javascript:window.print()">
                                <i></i><span class="e_aj">Распечатать</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="h_pay_block">
                        <div class="e_left hpn_info">
                            <div class="middle e_fs_x">
                                <div class="e_left checkin"><b>Заезд:</b> Воскресенье, 17 февраля 2013 г.</div>
                                <div class="nights_count e_ta_center">7 ночей</div>
                                <div class="e_right checkout"><b>Выезд:</b> Воскресенье, 24 февраля 2013 г.</div>
                            </div>
                            <div class="e_book_info ">
                                <div class="top">
                                    <div class="e_stars_line big white_bg e_left">4</div>
                                    <div class="hotel_category e_left bold e_fs_xl">
                                        <span>Туристический отель высшего уровня</span></div>
                                    <div class="clearfix"></div>
                                    <h2 class="e_mh e_bliss_light">The Inn at the Roman Forum — Small Luxury Hotels,
                                        Рим,
                                        Италия</h2>

                                    <div class="address e_hotel_map_focus" data-rel="map_window" data-hid="2">
                                        <i></i><span class="e_aj">Via Nazionale, 46 00184 Rome</span>
                                        <span class="addr_phone">Телефон: <b>+39 0648939960</b></span>
                                    </div>

                                    <div class="numbers">
                                        <div class="e_numbers_block single">
                                            <div class="e_left list">
                                                <div class="num_line e_floatable">
                                                    <div class="description e_left"><span class="e_bliss e_fs_xxxl">2x Junior Suite Room</span>
                                                    </div>
                                                    <div class="people e_people_count e_right invisible">2</div>
                                                    <div class="num_count e_bliss e_fs_big e_right">2x</div>
                                                </div>
                                                <div class="num_line e_floatable">
                                                    <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Room</span><i
                                                            class="num_left e_fs_m e_red">Остался 1 номер</i></div>
                                                    <div class="people e_people_count e_right invisible" title="">
                                                        3
                                                    </div>
                                                </div>
                                                <div class="num_line e_floatable">
                                                    <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Suite</span>
                                                    </div>
                                                    <div class="people e_people_count e_right invisible">1</div>
                                                </div>
                                                <div class="book_options">
                                                    <div class="food_options e_left e_fs_x">
                                                        Включён завтрак (BB)
                                                    </div>
                                                    <div class="e_available_fast">Немедленное подтверждение</div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="e_book_info e_book_user_info e_bliss">
                                <h3 class="e_bliss_light">Ваши данные</h3>

                                <div class="name">MR KONSTANTINOPOLSKY KONSTANTIN</div>
                                <div class="location">Russia, Moscow</div>
                                <div class="contact">Контактный телефон: <b class="e_bliss_bold">+7 916
                                        202-87-02</b></div>
                                <div class="contact">E-mail: <b class="e_bliss_bold"><a
                                            href="mailto:workfeld@gmail.com" class="e_sl">workfeld@gmail.com</a></b>
                                </div>
                            </div>
                            <div class="e_book_info e_book_user_info">
                                <h3 class="e_bliss_light">Имена туристов</h3>
                                <ol class="users_list">
                                    <li>MR <b>KONSTANTINOPOLSKY KONSTANTIN</b> <span
                                            class="leader">(Лидер группы)</span></li>
                                    <li>MRS <b>LUDMILA FELDMAN</b></li>
                                    <li>MR <b>MAXIM MALYSHEV</b></li>
                                    <li>MRS <b>ANNA MALYSHEVA</b></li>
                                    <li>MR <b>ILYA FELDMAN</b></li>
                                </ol>
                            </div>
                        </div>
                        <div class="e_left e_pay_column">
                            <div class="epc_main">
                                <div class="epc_wrapper">
                                    <div class="book_code bi_block">
                                        <h4 class="e_bliss_light">Код брони</h4>

                                        <div class="code e_ta_right">
                                            <b>BOOK2013031104345841</b>
                                        </div>
                                        <div class="status e_italic e_ta_right">
                                            Статус оплаты: <b class="e_red">Не оплачено</b>
                                        </div>
                                    </div>
                                    <div class="book_price bi_block">
                                        <h4 class="e_bliss_light">Сумма к оплате</h4>

                                        <div class="price e_ta_right">
                                            <i class="e_bliss_bold e_fs_big price_tag"><a href="#" class="e_aj">23
                                                    270</a><span class="e_ruble">a</span></i>

                                            <div class="e_price_details e_bs_dark">
                                                <? /*<span class="total_word">Итого</span>*/ ?>
                                                <ul class="e_flat_list e_bold">
                                                    <li>
                                                        <div class="epd_top e_floatable">
                                                            <div class="e_left">Executive Junor Suite</div>
                                                            <div class="e_right e_people_count mini" title="2 человека">
                                                                <img src="/i/e/man_mini.png" alt=""><img
                                                                    src="/i/e/man_mini.png" alt=""></div>
                                                        </div>
                                                        <div class="epd_bottom e_floatable">
                                                            <div class="e_left">5 <span class="nights_word">ночей</span>
                                                                * 100 000 <span class="e_ruble">j</span></div>
                                                            <div class="e_right">100 000 <span class="e_ruble">j</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="epd_top e_floatable">
                                                            <div class="e_left">Executive Junor Suite</div>
                                                            <div class="e_right e_people_count mini" title="3 человека">
                                                                <img src="/i/e/man_mini.png" alt=""><img
                                                                    src="/i/e/man_mini.png" alt=""><img
                                                                    src="/i/e/man_mini.png" alt=""></div>
                                                        </div>
                                                        <div class="epd_bottom e_floatable">
                                                            <div class="e_left">5 <span class="nights_word">ночей</span>
                                                                * 10 000 <span class="e_ruble">j</span>
                                                            </div>
                                                            <div class="e_right">100 000 <span class="e_ruble">j</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="epd_top e_floatable">
                                                            <div class="e_left">Executive Junor Suite</div>
                                                            <div class="e_right e_people_count mini" title="1 человек">
                                                                <img src="/i/e/man_mini.png" alt=""></div>
                                                        </div>
                                                        <div class="epd_bottom e_floatable">
                                                            <div class="e_left">5 <span class="nights_word">ночей</span>
                                                                * 30 000 <span class="e_ruble">j</span>
                                                            </div>
                                                            <div class="e_right">150 000 <span class="e_ruble">j</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="vat_includes e_ta_center e_fs_s">Цены включают в себя:
                                                    Налоги, завтрак и ужин.
                                                </div>
                                                <div class="btns e_ta_center">
                                                    <a href="#" class="e_aj close">Закрыть</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="book_pay_opts bi_block noprint">
                                        <h4 class="e_bliss_light">Способы оплаты</h4>

                                        <div class="buttons e_ta_right">
                                            <a href="#" class="btn btn_pay_card lb_link" data-rel="pay_card_window">Банковская
                                                карта</a>
                                            <a href="#" class="btn btn_pay_bank lb_link" data-rel="pay_bank_window">Безналичный перевод</a>
                                        </div>

                                        <div class="cancel_book e_ta_center">
                                            <a href="/test/cancel_book.php" class="e_aj">Отменить бронирование</a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="bottom"></div>

                            <div class=" e_attention">
                                <i></i>Отмена брони без штрафных санкций до установленных сроков (день, время)&nbsp;—
                                <b>12.02.2013 00:00</b>
                            </div>

                            <div class="e_pay_info">
                                Оплата производится в рублях. При оплате банковской картой в случае, если валюта Вашего
                                счёта отлична от валюты оплаты, то конвертация будет происходить по курсу банка,
                                выпустившего Вашу карту.
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>

<? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>
<div class="lb-overlay" id="map_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Отели на карте</h3>
            <h4 class="e_fs_m e_nobold"></h4>

            <div class="map_area" id="map_window_map" style="height: 508px; width: 702px;">

            </div>
        </div>
    </div>
</div>







<div class="lb-overlay agr_window" id="pay_bank_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>
        <div class="lb_content e_ta_center">
            <div class="e_text_window" >
                <div class="content e_ta_left">
                    <h3>Правила оплаты и возврата денежных средств за отмененные бронирования</h3>
                    <h2>Безналичный банковский перевод </h2>
                    <p>Оплата банковским переводом возможна как для юридических, так и&nbsp;для физических лиц. Для этого Вам необходимо получить счет на&nbsp;оплату заказа, нажав кнопку «Получить счет в&nbsp;формате PDF» на&nbsp;этой страничке внизу. Счет Вы&nbsp;сможете распечатать или получить на&nbsp;электронную почту. После оплаты счета банковским переводом для того чтобы ускорить обработку заказа, Вы&nbsp;можете выслать нам копию (скан) платежного поручение с&nbsp;отметкой банка об&nbsp;оплате.</p>
                    <h2>Отмена бронирования и&nbsp;условия возврата денежных средств</h2>
                    <p>Пользователь может отменить произведенное бронирование проживания в&nbsp;гостинице и&nbsp;другие услуги, предоставляемые партнерами сайта. При условии отмены бронирования в&nbsp;каждом конкретном случае возможно вступление в&nbsp;силу штрафных санкций в&nbsp;зависимости от&nbsp;условий, предъявляемых поставщиками услуг, вплоть до&nbsp;применения правила о&nbsp;безотзывности тарифа, при котором штрафы составляют сто процентов платежа за&nbsp;бронирование. Указанные специальные условия бронирования приведены в&nbsp;информации о&nbsp;бронируемых отелях и&nbsp;других бронируемых в&nbsp;системе услугах.</p>
                    <p>Факт оплаты забронированных услуг в&nbsp;системе онлайн бронирования AnyFly.ru (<a href="http://www.anyfly.ru">www.anyfly.ru</a>) является фактом согласия с&nbsp;правилами и&nbsp;условиями бронирования, отмены бронирования и&nbsp;возврата денежных средств в&nbsp;случае отмены забронированных услуг.</p>

                    <p><strong>Важно!</strong> При оплате бронирования безналичным банковским переводом банки, участвующие в&nbsp;перечислении денежных средств могут взимать комиссию в&nbsp;размере, согласно своим правилам и&nbsp;условиям.</p>
                </div>
            </div>
        </div>
        <div class="lb_pay_btns_area">
            <div class="top"></div>
            <div class="middle">
                <a href="http://anyfly.ru/profile/bill.php?u=25307&amp;b=c7c1426cf248969d5339258729d09bb0" class="btn btn_new_window" target="_blank">Открыть в новом окне</a>
                <a href="/test/send_bill.php" class="btn btn_send_to_mail">Отправить на почту</a>
                <b>Счёт отправлен на почту</b>

                <a class="e_wicon e_hotels_print_link_dark pagePrintBtn" href="http://anyfly.ru/profile/bill.php?u=253077&amp;b=c7c1426cf248969d5339258729d09bb0">
                    <i></i><span class="e_aj">Распечатать</span>
                </a>
            </div>
        </div>
    </div>
</div>



<div class="lb-overlay agr_window" id="pay_card_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>
        <div class="lb_content e_ta_center">
            <div class="e_text_window" >
                <div class="content e_ta_left">
                    <h3>Правила оплаты и возврата денежных средств за отмененные бронирования</h3>
                    <h2>Оплата банковскими картами </h2>

                    <p>Обработка платежей по&nbsp;банковским картам и&nbsp;электронными деньгами обеспечивается
                        компанией RBK Money. К&nbsp;оплате принимаются банковские карты (кредитные и&nbsp;дебетовые): «MasterCard»
                        и&nbsp;«Visa»<br/>
                    <h5>Процесс оплаты</h5>
                    <p>После выбора и&nbsp;бронирования услуг на&nbsp;сайте www.anyfly.ru выбранных Вами услуг&nbsp;Вы
                        будете перенаправлены на&nbsp;сервер компании RBK Money, которая имеет все необходимые лицензии
                        и&nbsp;сертификаты на&nbsp;проведение платежей через Интернет.</p>

                    <p>Напоминаем, что единственным официальным сайтом компании AnyFly™ (торговая марка ООО «Трэвел
                        Эстейт») является сайт www.anyfly.ru.<br/><br/>
                        Оплата по&nbsp;банковским картам в&nbsp;сети Интернет производится путем переадресации на&nbsp;сайт
                        системы электронных платежей RBK Money. Передача данных происходит в&nbsp;защищенном режиме, на&nbsp;сервер
                        компании RBK Money, при котором полностью исключена возможность перехвата информации о&nbsp;вашей
                        карте.
                    </p>
                    <h2>Отмена бронирования и&nbsp;условия возврата денежных средств</h2>

                    <p>Пользователь может отменить произведенное бронирование проживания в&nbsp;гостинице и&nbsp;другие
                        услуги, предоставляемые партнерами сайта. При условии отмены бронирования в&nbsp;каждом
                        конкретном случае возможно вступление в&nbsp;силу штрафных санкций в&nbsp;зависимости от&nbsp;условий,
                        предъявляемых поставщиками услуг, вплоть до&nbsp;применения правила о&nbsp;безотзывности тарифа,
                        при котором штрафы составляют сто процентов платежа за&nbsp;бронирование. Указанные специальные
                        условия бронирования приведены в&nbsp;информации о&nbsp;бронируемых отелях и&nbsp;других
                        бронируемых в&nbsp;системе услугах.</p>
                    <br/>

                    <p>В&nbsp;случае любой отмены бронирования пользователем, подразумевающий возврат денежных средств,
                        система электронных платежей RBK Money удерживает&nbsp;3% от&nbsp;суммы платежа за&nbsp;каждую
                        проведенную транзакцию (оплата/возврат).</p>

                    <p>Факт оплаты забронированных услуг в&nbsp;системе онлайн бронирования AnyFly.ru (<a
                            href="http://www.anyfly.ru">www.anyfly.ru</a>) является фактом согласия с&nbsp;правилами и&nbsp;условиями
                        бронирования, отмены бронирования и&nbsp;возврата денежных средств в&nbsp;случае отмены
                        забронированных услуг.</p>
                </div>
            </div>
        </div>
        <div class="lb_pay_btns_area">
            <div class="top"></div>
            <div class="middle">
                <form action="https://rbkmoney.ru/acceptpurchase.aspx" method="post" name="pay_conf" id="pay_conf">
                    <input type="hidden" name="book_total_price"
                           value="13983.61"/>
                    <input type="hidden" name="book_ref_code" value="BOOK2013031909324556"/>
                    <input type="hidden" name="_payment_cost"
                           value="13983.61"/>
                    <input type="hidden" name="eshopId" value="2011706">
                    <input type="hidden" name="recipientAmount"
                           value="13983.61">
                    <input type="hidden" name="recipientCurrency" value="RUR">
                    <input type="hidden" name="orderId" value="107945">
                    <input type="hidden" name="serviceName" value="Hotel booking BOOK2013031909324556">
                    <input type="hidden" name="preference" value="bankCard">
                    <input type="hidden" name="user_email" value="mvmstudio@gmail.com">
                    <input type="hidden" name="successUrl" value="http://anyfly.ru/book_conf.php?code=c7c1426cf248969d5339258729d09bb07&amp;addOk=true">
                    <input type="hidden" name="failUrl" value="http://anyfly.ru/book_conf.php?code=c7c1426cf248969d5339258729d09bb07&amp;addErr=true">

                    <input type="submit" value="Банковской картой" class="btn btn_go_to_pay"/>
                    <div class="ortext">или <a href="#" class="lb_hide e_bl">закрыть</a></div>
                    <span><img src="/i/e/ajax-loader.gif" alt="Загрузка"/>Производится переход на сайт RBK Money</span>
                </form>
            </div>
        </div>
    </div>
</div>

<script id="hotelInfobox" type="text/html">
    {{#list}}
    {{#HOTEL}}
    <div class="infobox-wrapper " data-hid="{{HOTEL.ID}}">
        <div class="infobox_content e_bs_dark" id="h_infobox_{{HOTEL.ID}}">
            <div class="ic_wrapper">
                <div class="top">
                    <div class="e_stars_line e_left">{{HOTEL.STARS}}</div>
                    {{#HOTEL.RATING}}<a href="{{HOTEL.LINK}}" class="e_rating_mini">{{HOTEL.RATING}}</a>{{/HOTEL.RATING}}
                    <div class="clearfix"></div>
                </div>
                <h4 class="e_mh e_bliss_medium"><a href="{{HOTEL.LINK}}">{{HOTEL.NAME}}</a></h4>

                <div class="middle">
                    <div class="e_left location">{{HOTEL.LOCATION}}</div>
                    {{#HOTEL.FEEDBACK_COUNT}}
                    <div class="e_right feedback_count"><a href="{{HOTEL.LINK}}#feedback" class="e_bl noprint">{{HOTEL.FEEDBACK_COUNT}}</a>
                    </div>
                    {{/HOTEL.FEEDBACK_COUNT}}

                    <div class="e_right"><a href="{{HOTEL.LINK}}" class="e_bl">Описание</a></div>
                    <div class="clearfix"></div>
                </div>

                <div class="photos e_floatable">
                    <img src="/i/demo/hib1.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib2.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib3.jpg" alt="" class="e_left">
                </div>
                <div class="buttons_area">
                    <div class="e_left price">
                        <div class="price e_bliss_medium e_fs_xxxl"><i>{{HOTEL.BEST_PRICE_STR}}</i><span
                                class="{{HOTEL.CURRENCY.CLASS}}">{{HOTEL.CURRENCY.VALUE}}</span></div>
                        <div class="nights e_fs_s">За <i>7 ночей</i></div>
                    </div>
                    <div class="e_right book">
                        <a href="{{HOTEL.BOOK_LINK}}" target="_blank" class="btn btn_book_s_blue">Забронировать</a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="pipe"></div>
        </div>
    </div>
    {{/HOTEL}}
    {{/list}}
</script>


<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/vendor/jquery.jcarousel.min.js"></script>
<script src="/js/vendor/jquery.columnizer.min.js"></script>

<script src="/js/lib/ax_controls.js?<?= time() ?>"></script>
<script src="/js/lib/ax_lightbox.js?<?= time() ?>"></script>
<script src="/js/ax_rules.js?<?= time() ?>"></script>
<script src="/js/lib/ax_form.js?<?= time() ?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?= time() ?>"></script>
<script src="/js/main.js?<?= time() ?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


