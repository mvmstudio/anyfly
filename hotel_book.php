<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/skin.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets:true});
    </script>
</head>
<body class="inner_page hotel_card_page hotels_list book_page">
<input type="hidden" name="currency" value="RUB"/>
<input type="hidden" name="hotel_hash"
       value="<?=$_GET['hotel_hash'] ? $_GET['hotel_hash'] : 'eurostars-international-palace'?>"/>

<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->


<div class="main_wrapper">
<div class="printonly">
    <h3 class="e_mh" id="__location" style="margin-bottom: 9px">Рим, Италия</h3>
    <h4 class="e_mh" id="__days">18 марта — 4 апреля (17 ночей)</h4>
    <h4 class="e_mh" id="__people">2 взрослых, 3 детей</h4>
    <h4 class="e_mh" id="__nights_count_text" style="display: none">17 ночей</h4>
</div>
<div class="page_header noprint">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                   class="btn btn_facebook_white">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                   class="btn btn_vkontakte_white">Vkontakte</a>
            </div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left subsribe_block">
                <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                   data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
            </div>

            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link" data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                     class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<div class="index_book_block noprint">

    <div class="content_block">
        <div class="e_left site_logo"><a href="/"><img src="/i/logo_mini.png" alt="AnyFly"/></a></div>
        <div class="e_phones_block e_ts_dark">
            <div class="e_floatable e_light_blue">
                <div class="e_left"><b class="e_white"><a href="tel:88008008080" class="phone">8 800 800 80
                    80</a></b><br>из Москвы
                </div>
                <div class="e_right"><b class="e_white"><a href="tel:88008008070" class="phone">8 800 800 80
                    70</a></b><br>из
                    России
                </div>
            </div>
        </div>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active" data-ref="/"><span>Отели</span><i></i>
                    </li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                    <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                </ul>
            </div>
            <form method="post" action="/italy/rome/">
                <div class="e_search_form_content">
                    <div class="e_search_line">

                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="book_bg"></div>
</div>


<div class="index_content_block e_texture_black">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">

                <div class="hotel_card_wrapper book_wrapper" style="min-height: 90px">
                    <div class="top">
                        <div class="e_left e_back_to noprint"><a href="/italy/rome/eurostars-international-palace/"
                                                                 class="e_bl w_icon e_wicon e_back_to_dark"><i></i>Вернуться
                            на страницу отеля</a></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="middle e_fs_x">
                        <div class="e_left checkin"><b>Заезд:</b> Воскресенье, 17 февраля 2013 г.</div>
                        <div class="nights_count e_ta_center">7 ночей</div>
                        <div class="e_right checkout"><b>Выезд:</b> Воскресенье, 24 февраля 2013 г.</div>
                    </div>
                    <div class="e_book_info ">
                        <div class="top">
                            <div class="e_stars_line big white_bg e_left">4</div>
                            <div class="hotel_category e_left bold e_fs_xl">
                                <span>Туристический отель высшего уровня</span></div>
                            <div class="clearfix"></div>
                            <h2 class="e_mh e_bliss_light">The Inn at the Roman Forum — Small Luxury Hotels, Рим, Италия</h2>

                            <div class="address e_hotel_map_focus" data-rel="map_window" data-hid="2">
                                <i></i><span class="e_aj">Via Nazionale, 46 00184 Rome</span>
                            </div>

                            <div class="numbers">
                                <div class="e_numbers_block single">
                                    <div class="e_left list">
                                        <div class="num_line e_floatable">
                                            <div class="description e_left"><span class="e_bliss e_fs_xxxl">2x Junior Suite Room</span>
                                            </div>
                                            <div class="people e_people_count e_right invisible">2</div>
                                            <div class="num_count e_bliss e_fs_big e_right">2x</div>
                                        </div>
                                        <div class="num_line e_floatable">
                                            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Room</span><i
                                                    class="num_left e_fs_m e_red">Остался 1 номер</i></div>
                                            <div class="people e_people_count e_right invisible" title="">3</div>
                                        </div>
                                        <div class="num_line e_floatable">
                                            <div class="description e_left"><span class="e_bliss e_fs_xxxl">Standart Suite</span>
                                            </div>
                                            <div class="people e_people_count e_right invisible">1</div>
                                        </div>
                                        <div class="book_options">
                                            <div class="food_options e_left e_fs_x">
                                                Включён завтрак (BB)
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="e_special_offer_info e_bs_dark">
                                <i class="e_action_s"></i>

                                <div class="text"><b>MIN 3 NIGHTS — 15% DISCOUNT</b>. Free use fitness at Sina Fitness
                                    Club: sauna, steam bath and gym.
                                </div>
                            </div>

                            <div class="price_block">
                                <div class="e_left fast_book e_left">Отель включен в обслуживание Быстрого Бронирования
                                    <span>– Отель Быстрого бронирования,<br>в момент резервации по запросу будет подтвержден или отклонен в течении 30 минут!</span>
                                </div>
                                <div class="price e_right e_bliss_bold e_ta_right">232 705 <span
                                        class="e_ruble">a</span></div>
                                <div class="e_available_fast">Немедленное подтверждение</div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="e_ta_center scrltobottom e_fs_x">
                                <a href="#book_form" class="e_aj e_scroll_to_bottom noprint">Оформить бронь</a>
                            </div>
                        </div>
                    </div>
                    <div class="pay_info e_fs_s">
                        Оплата производится в рублях. При оплате банковской картой в случае, если валюта Вашего счёта
                        отлична от валюты оплаты, то конвертация будет происходить по курсу банка, выпустившего Вашу
                        карту.
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<form method="post" class="ax_form" id="book_form" action="/test/book.php" data-callback="cb_book_success">
    <div class="hotel_card_description e_texture_gray noprint">
        <div class="content_block">
            <div class="index_wrapper">
                <div class="wrapper_content">
                    <div class="e_booking_form">

                        <div class="section personal_data">
                            <div class="e_left form">
                                <h3 class="e_mh e_mh_striked  e_bliss_light"><span>Ваши данные</span></h3>

                                <div class="fields">
                                    <div class="f_line two_fields">
                                        <div class="e_left f_block">
                                            <label for="t_name" class="e_bliss_light e_fs_xl">Имя</label>
                                            <input type="text" class="e_text_field" name="t_name" id="t_name"
                                                   maxlength="100"/>
                                            <span class="annotation">Пример: Anton</span>
                                        </div>
                                        <div class="e_left f_block">
                                            <label for="t_surname" class="e_bliss_light e_fs_xl">Фамилия</label>
                                            <input type="text" class="e_text_field" name="t_surname" id="t_surname"
                                                   maxlength="100"/>
                                            <span class="annotation">Пример: Antonov</span>
                                        </div>


                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="f_line three_fields">


                                        <div class="e_left f_block">
                                            <label for="t_country" class="e_bliss_light e_fs_xl">Страна</label>
                                            <select name="t_country" id="t_country" style="width: 250px"
                                                    class="e_chosen">
                                                <option>Россия</option>
                                                <option>Украина</option>
                                                <option>Белоруссия</option>
                                            </select>
                                        </div>

                                        <div class="e_left f_block">
                                            <label for="t_city" class="e_bliss_light e_fs_xl">Город</label>
                                            <input type="text" class="e_text_field" name="t_city" id="t_city"
                                                   maxlength="100"/>
                                        </div>
                                        <div class="e_right f_block phone_block">
                                            <label for="t_phone" class="e_bliss_light e_fs_xl">Телефон</label>
                                            <input type="text" class="e_text_field" name="t_phone" id="t_phone"
                                                   maxlength="20"/>
                                            <span class="annotation">Пример: +74959261800</span>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="f_line two_fields">
                                        <div class="e_left f_block">
                                            <label for="t_email" class="e_bliss_light e_fs_xl">E-mail</label>
                                            <input type="text" class="e_text_field no_tt" name="t_email" id="t_email"
                                                   maxlength="100"/>
                                        </div>
                                        <div class="e_left f_block">
                                            <label for="t_email_confirm" class="e_bliss_light e_fs_xl">Повторить
                                                e-mail</label>
                                            <input type="text" class="e_text_field no_tt" name="t_email_confirm"
                                                   id="t_email_confirm" maxlength="100"/>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="e_right info">
                                <div class="text e_bs_dark">
                                    <i class="pipe"></i>
                                    Данные клиента заполняются латиницей, как в <b>загранпаспорте</b>.
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="section tourists_data">
                            <div class="e_left form">
                                <h3 class="e_mh e_mh_striked  e_bliss_light"><span>Имена туристов</span></h3>

                                <div class="fields">

                                    <div class="f_line three_fields">
                                        <div class="e_left f_block f_chb_block">
                                            <a href="#" class="e_aj e_tip">?<i>Лидер группы, на которого оформляется
                                                бронирование</i></a>
                                            <label class="e_tf_checkbox_label">
                                                <input type="checkbox" name="tt_data[1]['is_leader]"
                                                       class="e_f_checkbox"
                                                       checked="checked"></label>
                                        </div>
                                        <div class="e_left f_block f_tt_sel">
                                            <label for="t_title_1" class="e_bliss_light e_fs_xl">Титул</label>
                                            <select name="tt_data[1][title]" id="t_title_1" class="e_chosen"
                                                    style="width: 65px">
                                                <option>MR</option>
                                                <option>MRS</option>
                                                <option>MS</option>
                                            </select>
                                        </div>

                                        <div class="e_left f_block">
                                            <label for="tt_name_1" class="e_bliss_light e_fs_xl">Имя</label>
                                            <input type="text" class="e_text_field" name="tt_data[1][name]"
                                                   id="tt_name_1"
                                                   maxlength="100"/>
                                        </div>
                                        <div class="e_right f_block">
                                            <label for="tt_surname_1" class="e_bliss_light e_fs_xl">Фамилия</label>
                                            <input type="text" class="e_text_field" name="tt_data[1][surname]"
                                                   id="tt_surname_1" maxlength="100"/>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="f_line three_fields">
                                        <div class="e_left f_block f_chb_block">
                                            <a href="#" class="e_aj e_tip">?<i>Лидер группы, на которого оформляется
                                                бронирование</i></a>
                                            <label class="e_tf_checkbox_label">
                                                <input type="checkbox" name="tt_data[2]['is_leader]"
                                                       class="e_f_checkbox"></label>
                                        </div>
                                        <div class="e_left f_block f_tt_sel">
                                            <label for="t_title_2" class="e_bliss_light e_fs_xl">Титул</label>
                                            <select name="tt_data[2][title]" id="t_title_2" class="e_chosen"
                                                    style="width: 65px">
                                                <option>MR</option>
                                                <option>MRS</option>
                                                <option>MS</option>
                                            </select>
                                        </div>

                                        <div class="e_left f_block">
                                            <label for="tt_name_2" class="e_bliss_light e_fs_xl">Имя</label>
                                            <input type="text" class="e_text_field" name="tt_data[2][name]"
                                                   id="tt_name_2"
                                                   maxlength="100"/>
                                        </div>
                                        <div class="e_right f_block">
                                            <label for="tt_surname_2" class="e_bliss_light e_fs_xl">Фамилия</label>
                                            <input type="text" class="e_text_field" name="tt_data[2][surname]"
                                                   id="tt_surname_2" maxlength="100"/>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="e_right info">
                                <div class="text e_bs_dark">
                                    <i class="pipe"></i>
                                    Укажите фамилии и имена туристов (как в заграничном паспорте), для которых
                                    производится
                                    бронирование. Данные туристов будут необходимы для получения визы.
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="booking_form_continued e_texture_white noprint">
        <div class="rush"></div>
        <div class="content_block">
            <div class="index_wrapper">
                <div class="wrapper_content">
                    <div class="e_booking_form_continued">
                        <div class="book_conditions noprint">
                            <div class="e_text_window" id="et_pub_agreement_window">
                                <h4 class="e_bliss_light">Условия договора публичной оферты</h4>
                                <a href="#" class="lb_close"></a>
                                <div class="content">
                                    <?include('includes/agr.php')?>
                                </div>
                            </div>
                            <div class="e_text_window" id="et_payment_cond_window">
                                <h4 class="e_bliss_light">Условия оплаты бронирования</h4>
                                <a href="#" class="lb_close"></a>
                                <div class="content">
                                    <?include('includes/pay.php')?>
                                </div>
                            </div>
                        </div>
                        <div class="book_commons">
                            <div class="e_left">
                                <label class="e_tf_checkbox_label">
                                    <input type="checkbox" name="agr_confirmed"
                                           class="e_f_checkbox">Я принимаю общие <a href="#" class="e_aj etb_link" data-rel="et_pub_agreement_window">условия договора публичной оферты</a></label>
                                <label class="e_tf_checkbox_label">
                                    <input type="checkbox" name="book_cond_confirmed"
                                           class="e_f_checkbox">Я принимаю <a href="#" class="e_aj etb_link" data-rel="et_payment_cond_window">условия оплаты бронирования</a></label>
                            </div>
                            <div class="e_right e_attention">
                                <i></i>Отмена брони без штрафных санкций до установленных сроков (день, время) — <b>12.02.2013 00:00</b>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="promo_code_area">
                            <div class="border"></div>
                            <div class="cont">
                                <div class="e_promocode_wrapper e_bs_dark">
                                    <label class="e_bliss_light e_fs_xxxl" for="t_promo_code">Промокод</label>
                                    <input type="text" class="e_text_field e_bliss_light e_fs_xxxl" id="t_promo_code" name="t_promo_code"/>
                                </div>
                                <i class="proc"></i>
                                <div class="info e_fs_x">
                                    Если у Вас есть промо код, пожалуйста, введите его, чтобы получить скидку.
                                </div>
                            </div>
                            <div class="border"></div>
                        </div>
                        <div class="price_area">
                            <div class="e_left">
                                Для <a href="#" class="e_aj lb_link" data-rel="registration_window">зарегистрированных</a> пользователей<br>предоставляется скидка.
                            </div>
                            <div class="price_total e_ta_center">
                                <p>Итого к оплате</p>
                                <p class="price_wrapper e_bliss">232 705 <span
                                        class="e_ruble">a</span></p>
                            </div>
                            <div class="e_right">
                                <a href="#" class="e_ta_right e_aj e_scroll_to_top noprint">Подняться наверх</a>
                            </div>
                        </div>
                        <div class="buttons_area e_ta_center">
                            <input name="submit" value="Забронировать" type="submit" class="btn btn_book_xxl_blue"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

<? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>



<div class="lb-overlay" id="map_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Отели на карте</h3>
            <h4 class="e_fs_m e_nobold"></h4>

            <div class="map_area" id="map_window_map" style="height: 508px; width: 702px;">

            </div>
        </div>
    </div>
</div>


<script id="hotelInfobox" type="text/html">
    {{#list}}
    {{#HOTEL}}
    <div class="infobox-wrapper " data-hid="{{HOTEL.ID}}">
        <div class="infobox_content e_bs_dark" id="h_infobox_{{HOTEL.ID}}">
            <div class="ic_wrapper">
                <div class="top">
                    <div class="e_stars_line e_left">{{HOTEL.STARS}}</div>
                    {{#HOTEL.RATING}}<a href="{{HOTEL.LINK}}" class="e_rating_mini">{{HOTEL.RATING}}</a>{{/HOTEL.RATING}}
                    <div class="clearfix"></div>
                </div>
                <h4 class="e_mh e_bliss_medium"><a href="{{HOTEL.LINK}}">{{HOTEL.NAME}}</a></h4>

                <div class="middle">
                    <div class="e_left location">{{HOTEL.LOCATION}}</div>
                    {{#HOTEL.FEEDBACK_COUNT}}
                    <div class="e_right feedback_count"><a href="{{HOTEL.LINK}}#feedback" class="e_bl noprint">{{HOTEL.FEEDBACK_COUNT}}</a>
                    </div>
                    {{/HOTEL.FEEDBACK_COUNT}}

                    <div class="e_right"><a href="{{HOTEL.LINK}}" class="e_bl">Описание</a></div>
                    <div class="clearfix"></div>
                </div>

                <div class="photos e_floatable">
                    <img src="/i/demo/hib1.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib2.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib3.jpg" alt="" class="e_left">
                </div>
                <div class="buttons_area">
                    <div class="e_left price">
                        <div class="price e_bliss_medium e_fs_xxxl"><i>{{HOTEL.BEST_PRICE_STR}}</i><span
                                class="{{HOTEL.CURRENCY.CLASS}}">{{HOTEL.CURRENCY.VALUE}}</span></div>
                        <div class="nights e_fs_s">За <i>7 ночей</i></div>
                    </div>
                    <div class="e_right book">
                        <a href="{{HOTEL.BOOK_LINK}}" target="_blank" class="btn btn_book_s_blue">Забронировать</a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="pipe"></div>
        </div>
    </div>
    {{/HOTEL}}
    {{/list}}
</script>


<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/vendor/jquery.jcarousel.min.js"></script>
<script src="/js/vendor/jquery.columnizer.min.js"></script>

<script src="/js/lib/ax_controls.js?<?=time()?>"></script>
<script src="/js/lib/ax_lightbox.js?<?=time()?>"></script>
<script src="/js/ax_rules.js?<?=time()?>"></script>
<script src="/js/lib/ax_form.js?<?=time()?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


