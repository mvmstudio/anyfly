<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets: true});
    </script>
</head>
<body class="index_page ">
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->



<div class="main_wrapper">
<div class="page_header">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left site_logo"><a href="/"><img src="/i/logo.png" alt="AnyFly"/></a></div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank" class="btn btn_facebook">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank" class="btn btn_vkontakte">Vkontakte</a>
            </div>
            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link" data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                                      class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="index_book_block">
    <div class="index_book_map_block"></div>
    <div class="content_block">
        <h1 class="e_mh e_bliss_bold e_fs_xxl_c e_ts_dark">Бронирование отелей по всему миру</h1>
        <h6 class="e_mh e_bliss e_fs_xxl_c e_ts_dark">Более 100 000 отелей в 200 странах</h6>
        <div class="e_phones_block e_ts_dark">
            <h4 class="e_mh e_bliss_light e_white">Служба поддержки AnyFly</h4>
            <div class="e_floatable e_white">
                <div class="e_left"><b><a href="tel:88008008080" class="phone">8 800 800 80 80</a></b><br>из Москвы</div>
                <div class="e_right"><b><a href="tel:88008008070" class="phone">8 800 800 80 70</a></b><br>из России</div>
            </div>
        </div>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active"><span>Отели</span><i></i></li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                    <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                </ul>
                <div class="e_social_line">
                    <div id="vk_like"></div>
                    <script type="text/javascript">
                        VK.Widgets.Like("vk_like", {type: "button", height: 18});
                    </script>

                    <div class="fb-like" data-href="http://www.anyfly.ru" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>


                    <a href="#" class="btn btn_social_more">Ещё</a>
                    <div class="e_more_social">
                        <div class="odonclass">
                            <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share" data-mrc-config="{'cm' : '1', 'ck' : '1', 'sz' : '20', 'st' : '1', 'tp' : 'ok'}">Нравится</a>
                            <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                        </div>
                        <div class="twitter">                    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                        <div class="g_plus">
                            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                            <!-- Place this tag where you want the +1 button to render. -->
                            <div class="g-plusone" data-annotation="inline" data-width="100"></div>

                            <!-- Place this tag after the last +1 button tag. -->
                            <script type="text/javascript">
                                window.___gcfg = {lang: 'ru'};

                                (function() {
                                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                    po.src = 'https://apis.google.com/js/plusone.js';
                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                })();
                            </script>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <form method="post" action="/italy/rome/">
                <div class="e_search_form_content">
                    <div class="e_search_line">
                        <div class="e_left name_search">
                            <i></i>
                            <label class="e_tf_label e_bold" for="hs_search_name">Страна, город или отель</label>
                            <input class="e_text_field e_fs_x e_bold" name="name" value="" id="hs_search_name" autocomplete="off" tabindex="1"/>
                            <input type="hidden" id="__city_code" name="city_code" value="" />
                        </div>
                        <div class="e_left date_checkin">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkin">Заезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkin_str" value="" id="hs_date_checkin" autocomplete="off" tabindex="2"/>
                            <input type="text" name="date_checkin" id="__date_checkin" class="visuallyhidden" value="<?=date('Y/m/d')?>"/>
                        </div>
                        <div class="e_left e_date_days_count e_ta_center">
                            <b>0</b><span>ночей</span>
                            <input type="hidden" id="__nights_count" name="nights_count" value="0" />
                        </div>
                        <div class="e_left date_checkout">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkout">Выезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkout_str" value="" id="hs_date_checkout" autocomplete="off" tabindex="3"/>
                            <input type="text" name="date_checkout" id="__date_checkout" class="visuallyhidden" value="<?=date('Y/m/d', strtotime('+1 day'))?>"/>
                        </div>
                        <div class="e_left e_sl_count_selector adults_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_adults_count">Взрослых</label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="adults_count" id="hs_adults_count" autocomplete="off" tabindex="4"/>
                        </div>
                        <div class="e_left e_sl_count_selector children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_children_count">Детей <span class="e_fs_s e_nobold">2–15 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="children_count" id="hs_children_count" autocomplete="off" tabindex="5"/>
                        </div>
                        <div class="e_left e_sl_count_selector l_children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_l_children_count"><span class="e_fs_s e_nobold">до 2 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="l_children_count" id="hs_l_children_count" autocomplete="off" tabindex="6"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="e_cities_list">
                        <p class="e_bold e_fs_m">Популярные города</p>
                        <div class="e_cl_wrapper">
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Австрия" data-city-code="1"><i
                                        style="background-image: url('/i/demo/flags/au.png')"></i>Вена</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Чехия" data-city-code="2"><i
                                        style="background-image: url('/i/demo/flags/cz.png')"></i>Прага</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Монте-Карло" data-city-code="3"><i
                                        style="background-image: url('/i/demo/flags/mc.png')"></i>Монте-Карло</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Франция" data-city-code="4"><i
                                        style="background-image: url('/i/demo/flags/fr.png')"></i>Париж</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Германия" data-city-code="5"><i
                                        style="background-image: url('/i/demo/flags/de.png')"></i>Берлин</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Греция" data-city-code="6"><i
                                        style="background-image: url('/i/demo/flags/gr.png')"></i>Крит</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Греция" data-city-code="7"><i
                                        style="background-image: url('/i/demo/flags/gr.png')"></i>Родос</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Венгрия" data-city-code="8"><i
                                        style="background-image: url('/i/demo/flags/ve.png')"></i>Бадапешт</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="9"><i
                                        style="background-image: url('/i/demo/flags/it.png')"></i>Флоренция</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="10"><i
                                        style="background-image: url('/i/demo/flags/it.png')"></i>Милан</a></li>
                                <li class="active"><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="11"><i
                                        style="background-image: url('/i/demo/flags/it.png')"></i>Рим</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="12"><i
                                        style="background-image: url('/i/demo/flags/it.png')"></i>Сорренто-Неаполь</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="13"><i
                                        style="background-image: url('/i/demo/flags/it.png')"></i>Венеция</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Россия" data-city-code="14"><i
                                        style="background-image: url('/i/demo/flags/ru.png')"></i>Москва</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Испания" data-city-code="15"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Бареселона</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Испания" data-city-code="16"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Мадрид</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Великобритания" data-city-code="17"><i
                                        style="background-image: url('/i/demo/flags/gn.png')"></i>Лондон</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="США" data-city-code="18"><i
                                        style="background-image: url('/i/demo/flags/us.png')"></i>Нью-Йорк</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <div class="e_earch_buttons_area e_ta_center">
                    <label class="e_tf_checkbox_label e_tf_chb_dark e_white" for="hs_search_aval_only"><input type="checkbox" name="search_aval_only" id="hs_search_aval_only" class="e_f_checkbox" checked="checked">
                        <span>поиск только доступных отелей</span></label>
                    <input type="submit" name="submit" value="Искать" class="btn btn_search" />
                </div>
            </form>
        </div>
    </div>
</div>
<div class="index_promo_block e_texture_gray">
    <div class="content_block">
        <div class="index_wrapper">
            <h3 class="e_mh e_bliss_light e_mh_striked e_ta_center e_mh_starred"><span><i><a href="#" class="e_blh">Спецпредложения</a></i></span></h3>

            <div class="wrapper_content">
                <div class="e_block_spec e_bs_dark e_left">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_1.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Венецианский карнавал 2013</a></h4>

                            <p class="e_price e_fs_xxxl">от 2 409 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="e_block_spec e_bs_dark e_left">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_2.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Миланский мебельный салон</a></h4>

                            <p class="e_price e_fs_xxxl">от 1 200 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="e_block_spec e_bs_dark e_right">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_3.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Карнавал в Ницце</a></h4>

                            <p class="e_price e_fs_xxxl">от 6 685 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<div class="index_content_block e_texture_white">
<div class="content_block">
<div class="index_wrapper">
<div class="wrapper_content">
<div class="e_left cont_item">
    <h3 class="e_mh e_bliss_light">Рекомендуемые<br/>отели</h3>

    <div class="e_block e_block_sug_hotel e_bs_dark">
        <div class="cont">
            <div class="img e_left"><a href="#"><img src="/i/demo/sug_hotel_1.png"
                                                     alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
            </div>
            <div class="info e_right e_ta_left">
                <div class="stars_line">
                    <div class="e_stars_line e_left">3</div>
                    <div class="e_price e_fs_xl e_right e_ta_right">от 82 409 <span class="e_ruble">a</span></div>
                    <div class="clearfix"></div>
                </div>
                <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite Days
                    Inn Southwest</a></div>
                <div class="location e_fs_s">Рим, Италия</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_block e_block_sug_hotel e_bs_dark">
        <i class="e_action_s"></i>

        <div class="cont">
            <div class="img e_left"><a href="#"><img src="/i/demo/sug_hotel_2.jpg"
                                                     alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
            </div>
            <div class="info e_right e_ta_left">
                <div class="stars_line">
                    <div class="e_stars_line e_left">3</div>
                    <div class="e_price e_fs_xl e_right e_ta_right">от 1 409 <span class="e_ruble">a</span></div>
                    <div class="clearfix"></div>
                </div>
                <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite Days
                    Inn Southwest</a></div>
                <div class="location e_fs_s">Рим, Италия</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_block e_block_sug_hotel e_bs_dark">
        <div class="cont">
            <div class="img e_left"><a href="#"><img src="/i/demo/sug_hotel_3.jpg"
                                                     alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
            </div>
            <div class="info e_right e_ta_left">
                <div class="stars_line">
                    <div class="e_stars_line e_left">1</div>
                    <div class="e_price e_fs_xl e_right e_ta_right">от 102 568 <span class="e_ruble">a</span></div>
                    <div class="clearfix"></div>
                </div>
                <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite Days
                    Inn Southwest</a></div>
                <div class="location e_fs_s">Рим, Италия</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_block e_block_sug_hotel e_bs_dark">
        <i class="e_action_s"></i>

        <div class="cont">
            <div class="img e_left"><a href="#"><img src="/i/demo/sug_hotel_4.png"
                                                     alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
            </div>
            <div class="info e_right e_ta_left">
                <div class="stars_line">
                    <div class="e_stars_line e_left">5</div>
                    <div class="e_price e_fs_xl e_right e_ta_right">от 12 300 <span class="e_ruble">a</span></div>
                    <div class="clearfix"></div>
                </div>
                <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite Days
                    Inn Southwest</a></div>
                <div class="location e_fs_s">Рим, Италия</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="e_block e_block_sug_hotel e_bs_dark">
        <div class="cont">
            <div class="img e_left"><a href="#"><img src="/i/demo/sug_hotel_5.png"
                                                     alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
            </div>
            <div class="info e_right e_ta_left">
                <div class="stars_line">
                    <div class="e_stars_line e_left">2</div>
                    <div class="e_price e_fs_xl e_right e_ta_right">от 82 409 <span class="e_ruble">a</span></div>
                    <div class="clearfix"></div>
                </div>
                <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite Days
                    Inn Southwest</a></div>
                <div class="location e_fs_s">Рим, Италия</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<div class="e_left cont_item">
    <h3 class="e_mh e_bliss_light">Города<br/>и страны</h3>

    <div class="e_block e_block_popular_city">
        <div class="img e_bs_dark">
            <a href="#"><img src="/i/demo/pop_city_1.png" alt="Амстердам"/></a>
        </div>
        <div class="info e_ta_left">
            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Амстердам</a> <span
                    class="hotels_count e_arial e_nobold e_fs_m">– <b>147</b> отелей</span></h4>

            <div class="country"><a href="#" class="e_slh e_flag"><i
                    style="background-image: url('/i/demo/flags/nl.png')"></i>Нидерланды</a></div>
        </div>
    </div>
    <div class="e_block e_block_popular_city">
        <div class="img e_bs_dark">
            <a href="#"><img src="/i/demo/pop_city_2.png" alt="Амстердам"/></a>
        </div>
        <div class="info e_ta_left">
            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Барселона</a> <span
                    class="hotels_count e_arial e_nobold e_fs_m">– <b>532</b> отеля</span></h4>

            <div class="country"><a href="#" class="e_slh e_flag"><i
                    style="background-image: url('/i/demo/flags/spain.png')"></i>Испания</a></div>
        </div>
    </div>
    <div class="e_block e_block_popular_city">
        <div class="img e_bs_dark">
            <a href="#"><img src="/i/demo/pop_city_3.png" alt="Амстердам"/></a>
        </div>
        <div class="info e_ta_left">
            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Хельсинки</a> <span
                    class="hotels_count e_arial e_nobold e_fs_m">– <b>2031</b> отель</span></h4>

            <div class="country"><a href="#" class="e_slh e_flag"><i
                    style="background-image: url('/i/demo/flags/fi.png')"></i>Финляндия</a></div>
        </div>
    </div>
    <div class="show_all e_fs_x"><a href="#" class="e_sl">Полный список</a></div>
</div>
<div class="e_right cont_item">
    <div class="e_block e_bs_dark e_block_subscribe e_ta_left e_news_subscribe">
        <div class="top"></div>
        <div class="form">
            <h5 class="e_mh e_bliss_light"><label for="subscribe_email">Подписаться на новости</label></h5>

            <form method="get" id="subscribe_form">
                <input type="text" name="email" class="e_text_field e_tf_mini" id="subscribe_email"
                       placeholder="Ваша электронная почта"/>
                <input type="submit" value="Подписаться" class="btn btn_subscribe lb_link" data-rel="subscribe_success_window"/>
            </form>
        </div>
    </div>
    <div class="promo_wrapper">
        <a href="#"><img src="/i/demo/promo.png" alt="Встречайте осень-зиму с нами!"/></a>
    </div>
    <div class="social_block">
        <div class="fb-like-box" data-href="https://www.facebook.com/anyfly.ru" data-width="310" data-height="212"
             data-show-faces="true" data-stream="false" data-border-color="aaaaaa" data-header="false"></div>
    </div>
    <div class="social_block">
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

        <!-- VK Widget -->
        <div id="vk_groups"></div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode:0, width:"310", height:"215"}, 31694316);
        </script>
    </div>
</div>
<div class="clearfix"></div>
</div>
<div class="wrapper_content ">
    <div class="e_left cont_item news_item">
        <h3 class="e_mh e_bliss_light">Новости</h3>

        <div class="e_block e_news_item e_floatable e_ta_left">
            <div class="date e_left e_ta_center">
                <div class="day e_bliss_bold e_ts_white e_bliss_bold e_fs_xxl">29</div>
                <div class="month e_fs_s">февраля</div>
            </div>
            <div class="info e_left e_ta_left">
                <h5 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Визу в Турцию упростили</a></h5>

                <div class="cont">
                    <a href="#">МИД Турецкой республики проинформировал, что с&nbsp;1&nbsp;января 2013 года гражданам, у
                        которых нет в&nbsp;паспорте свободного места для штампа, визовой марки или отметки, не&nbsp;смогут
                        въехать в&nbsp;страну.</a>
                </div>
            </div>
        </div>
        <div class="e_block e_news_item e_floatable e_ta_left">
            <div class="date e_left e_ta_center">
                <div class="day e_bliss_bold e_ts_white e_bliss_bold e_fs_xxl">1</div>
                <div class="month e_fs_s">декабря</div>
            </div>
            <div class="info e_left e_ta_left">
                <h5 class="e_mh e_bliss_bold"><a href="#" class="e_blh">В Абу Даби откроется новый аквапарк Yas
                    Waterworld</a></h5>

                <div class="cont">
                    <a href="#">Согласно предварительной информации, в&nbsp;новом аквапарке будет несколько
                        аттракционов, которые не&nbsp;имеют аналогов на&nbsp;Ближнем Востоке и&nbsp;в&nbsp;мире, не&nbsp;говоря
                        о&nbsp;том, что это будет самый большой аквапарк на&nbsp;Ближнем Востоке.</a>
                </div>
            </div>
        </div>
        <div class="show_all e_fs_x"><a href="/news/" class="e_sl">Все новости</a></div>
    </div>
    <div class="e_left cont_item news_item">
        <h3 class="e_mh e_bliss_light">Путеводитель</h3>

        <div class="e_block e_news_item e_floatable e_ta_left">
            <div class="date e_left e_ta_center">
                <div class="day e_bliss_bold e_ts_white e_bliss_bold e_fs_xxl">16</div>
                <div class="month e_fs_s">ноября</div>
            </div>
            <div class="info e_left e_ta_left">
                <h5 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Дискотеки и ночные клубы Милана</a></h5>

                <div class="cont">
                    <a href="#">МИД Турецкой республики проинформировал, что с&nbsp;1&nbsp;января 2013 года гражданам, у
                        которых нет в&nbsp;паспорте свободного места для штампа, визовой марки или отметки, не&nbsp;смогут
                        въехать в&nbsp;страну.</a>
                </div>
            </div>
        </div>
        <div class="e_block e_news_item e_floatable e_ta_left">
            <div class="date e_left e_ta_center">
                <div class="day e_bliss_bold e_ts_white e_bliss_bold e_fs_xxl">31</div>
                <div class="month e_fs_s">явнваря</div>
            </div>
            <div class="info e_left e_ta_left">
                <h5 class="e_mh e_bliss_bold"><a href="#" class="e_blh">В Гонконге стартует ежегодный зимний
                    фестиваль</a></h5>

                <div class="cont">
                    <a href="#">Основные мероприятия проходят в&nbsp;двух районах, излюбленных туристами: Цим-Ша-Цуе
                        (Tsim Sha Tsui) и&nbsp;Сентрале (Central). В&nbsp;одном из&nbsp;парков организуется огромный
                        каток, бесплатный для всех желающих</a>
                </div>
            </div>
        </div>
        <div class="show_all e_fs_x"><a href="/news/" class="e_sl">Все статьи</a></div>
    </div>
    <div class="e_right cont_item">
        <div class="e_feedback e_ta_left">
            <div class="cont e_ta_center">
                <div class="text e_fs_x">
                    <table>
                        <tr>
                            <td>«Нельзя просто так взять и&nbsp;решить куда поехать отдыхать, но&nbsp;портал AnyFly
                                решил эту проблему, за&nbsp;это ему очень благодарен!»
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="sig">
                <div class="photo e_left"><img src="/i/demo/photo.png" alt="Константинопольский Констатин"/></div>
                <div class="name e_left">
                    <h5 class="e_mh e_bliss_bold">Константинопольский Константин</h5>

                    <p>ОАО &laquo;Русское географическое общество&raquo;<br/>Путешественник</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<h3 class="e_mh e_bliss_light e_mh_striked e_ta_center index_press_header"><span><a href="#" class="e_blh">Пресса о
    нас</a></span></h3>

<div class="wrapper_content">
    <table class="press_table">
        <tr>
            <td><a href="#"><img src="/i/demo/press_1.jpg" alt="CNews"/></a></td>
            <td><a href="#"><img src="/i/demo/press_2.jpg" alt="Wired"/></a></td>
            <td><a href="#"><img src="/i/demo/press_3.jpg" alt="MacDigger"/></a></td>
        </tr>
        <tr>
            <td><a href="#"><img src="/i/demo/press_4.jpg" alt="Look At Me"/></a></td>
            <td><a href="#"><img src="/i/demo/press_5.jpg" alt="Hopes &amp; Fears"/></a></td>
            <td><a href="#"><img src="/i/demo/press_6.jpg" alt="Avia.ru"/></a></td>
        </tr>
    </table>
</div>

<h3 class="e_mh e_bliss_light index_about_book_header">О бронировании</h3>

<div class="wrapper_content e_floatable">
    <div class="about_cont_block e_left">
        <p>Чем мы&nbsp;руководствуемся при бронировании отеля, помимо звездности и&nbsp;расположения? Конечно, стоимостью гостиницы. Островок гарантирует по-настоящему низкие цены на&nbsp;бронирование отелей. Как нам это удается? Все просто&nbsp;— мы&nbsp;не&nbsp;берем с&nbsp;клиентов комиссий и&nbsp;работаем по&nbsp;прямой договоренности с&nbsp;отелями, предоставляющими нам лучшие условия. С&nbsp;нашей помощью можете не&nbsp;только забронировать отель, но&nbsp;и&nbsp;получить помощь и&nbsp;необходимую информацию в&nbsp;любой момент поездки и&nbsp;по&nbsp;возвращению из&nbsp;нее. Чем мы&nbsp;руководствуемся при бронировании отеля, помимо звездности и&nbsp;расположения? Конечно, стоимостью гостиницы. Островок гарантирует по-настоящему низкие цены.</p>
    </div>
    <div class="about_cont_block e_right">
        <p>Почему бронирование отелей онлайн удобнее? Во-первых, Островок предоставляет вам отличный сервис по&nbsp;бронированию гостиниц. Мы&nbsp;делаем все, чтобы выбор отеля проходил быстро и&nbsp;легко. Наш отдел по&nbsp;работе с&nbsp;клиентами круглосуточно предоставляет всю необходимую информацию о&nbsp;гостиницах и&nbsp;помогает в&nbsp;решении любых вопросов, связанных с&nbsp;бронированием отелей. Во-вторых, в&nbsp;нашей базе более 130 тысяч отелей в&nbsp;200 странах мира. Вы&nbsp;с&nbsp;легкостью сможете выбрать и&nbsp;забронировать отель всего за&nbsp;пару минут и&nbsp;несколько кликов.</p>
    </div>
</div>

</div>
</div>
</div>
<div class="page_useful_links">
    <div class="content_block">
        <table>
            <tr>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">О компании</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Контактная информация</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Реквизиты</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Пресс-кит</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Корпоративным клиентам</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Частые вопросы</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Заявка на договор</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Помощь</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Условия использования сервиса</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Как забронировать отель?</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Частые вопросы</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Агенствам</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Войти</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Зарегистрироваться</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Партнерская программа</a></li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="page_footer">
    <div class="content_block">
        <div class="footer_content e_white e_floatable">
            <div class="e_left copyright e_fs_11">Система онлайн бронирования AnyFly™<br>AnyFly™ — торговая марка ООО
                «Трэвел Эстейт»
            </div>
            <div class="e_left pay_methods"><a href="http://visa.com.ru/main.jsp" target="_blank" rel="nofollow"><img
                    src="/i/card_visa.png" alt="VISA"/></a><a href="http://www.mastercard.com/ru/consumer/index.html"
                                                              target="_blank" rel="nofollow"><img
                    src="/i/card_master_card.png" alt="Master Card"/></a></div>
            <div class="e_right inxl e_fs_11 e_ta_right">
                <a href="http://www.inxl.ru" target="_blank" class="e_white">Разработка сайта<br>интерактивное агентство
                    <img src="/i/inxl.png" alt="inXL"/></a>
            </div>
        </div>
    </div>
</div>
</div>

<div class="lb-overlay" id="registration_window">
    <div>
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <form method="post" class="lb_form ax_form" id="registration_form" action="/test/reg.php" data-callback="cb_registration_success">
                <h3 class="e_mh_lb e_bliss_light e_ts_white">Регистрация</h3>

                <div class="lb_text">
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_email">E-mail</label>
                        <input type="text" name="email" class="e_text_field" id="reg_email" autocomplete="off"/>
                        <p class="e_tf_annotation e_fs_s">На указанный выше e-mail придет<br />запрос на подтверждение регистрации</p>
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_label e_fs_x" for="reg_password">Пароль</label>
                        <input type="password" name="password" class="e_text_field" id="reg_password" autocomplete="off" />
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_password_confirm">Подтверждение пароля</label>
                        <input type="password" name="password_confirm" class="e_text_field" id="reg_password_confirm" />
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_promo_code">Промо-код</label>
                        <input type="text" name="promo_code" class="e_text_field" id="reg_promo_code" />
                        <i class="discount"></i>
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_checkbox_label" for="reg_jur_lico"><input type="checkbox" name="jur_lico" id="reg_jur_lico" class="e_f_checkbox"/>
                            <span>Юридическое лицо</span></label>
                    </div>
                </div>

                <div class="err_handler"></div>


                <div class="buttons_area center">
                    <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_reg_xl"/>

                    <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="lb-overlay" id="login_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <form method="post" class="lb_form ax_form" id="login_form" action="/test/login.php" data-callback="cb_login_success">
                <h3 class="e_mh_lb e_bliss_light e_ts_white">Авторизация</h3>

                <div class="lb_text">
                    <div class="e_input_line ">
                        <label class="e_tf_label e_fs_x" for="login_email">E-mail</label>
                        <input type="text" name="email" class="e_text_field" id="login_email" />
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="login_password">Пароль</label>
                        <input type="password" name="password" class="e_text_field" id="login_password" />
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_checkbox_label" for="login_remember_me"><input type="checkbox" name="remember_me" id="login_remember_me" class="e_f_checkbox"/><span>Запомнить меня</span></label>
                    </div>
                </div>

                <div class="err_handler"></div>

                <div class="buttons_area center">
                    <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_enter_xl"/>

                    <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="lb-overlay" id="subscribe_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Поздравляем!</h3>

            <div class="lb_text">
                <p>Вы успешно подписались на нашу рассылку.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>
<div class="lb-overlay" id="registration_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Спасибо!</h3>

            <div class="lb_text">
                <p>На ваш адрес отправлено письмо<br>с подтверждением регистрации.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>


<div class="lb_bg"></div>

<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/lib/ax_controls.js?<?=time()?>"></script>
<script src="/js/lib/ax_lightbox.js?<?=time()?>"></script>
<script src="/js/ax_rules.js?<?=time()?>"></script>
<script src="/js/lib/ax_form.js?<?=time()?>"></script>
<script src="/js/plugins.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>

<script>
    $(function(){

    });
</script>
</body>
</html>
