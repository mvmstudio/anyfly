function testCSS(prop) {
    return prop in document.documentElement.style;
}
var os = (function () {
    var ua = navigator.userAgent.toLowerCase();
    return {
        isWin: /windows/.test(ua)
    };
}());
var isFirefox = testCSS('MozBoxSizing');
var isOpera = !!(window.opera && window.opera.version);
if (os.isWin) {
    $('html').addClass('win-os');
}
if (isFirefox) {
    $('html').addClass('firefox');
}
if (isOpera) {
    $('html').addClass('opera');
}


var currencySettings = {
    RUB: {
        step: 500
    },
    EUR: {
        step: 10
    },
    USD: {
        step: 20
    }
};


$(function () {
    var currency = $('input[name=currency]').val();

    $('.lb_link').axLightbox();
    $('a[data-rel=registration_window]').focusAndClearForm('reg_email');
    $('a[data-rel=login_window]').focusAndClearForm('login_email');
    $('.e_search_tabs').loadTabContent();
    $('#hs_search_name').axControls('search');
    $('#hs_children_count').axControls('counter', {
        'maxNumber': 4,
        'minNumber': 0,
        'defaultValue': 0
    });
    $('#hs_l_children_count').axControls('counter', {
        'maxNumber': 4,
        'minNumber': 0,
        'defaultValue': 0
    });
    $('#hs_adults_count').axControls('counter', {
        'maxNumber': 8,
        'minNumber': 1,
        'defaultValue': 1
    });
    $('#__nights_count').axControls('dates', { initFromInputs: true });
    $('.e_cities_list li').axControls('cities', { nameInput: '#hs_search_name', codeInput: '#__city_code'});
    $('.e_social_line .btn_social_more').showSocialMore();

    /* NEW */
    $('.inner_page .index_book_block').staticSearchForm();
    $('.e_num_fromat').axControls('formatNumber');
    $('.filters_column h3.e_vis_toggler').axControls('toggleNextBlock', {storage: true});
    $('.filters_column h4.e_vis_toggler').axControls('toggleNextBlock', {storage: true, hidden: true});
    $('.e_stars_line').createStarsLine();

    if (currencySettings[currency]) {
        $('#fc_prices_range').axControls('rangeSlider', {
            min: parseInt($('#fc_price_from').val()),
            max: parseInt($('#fc_price_to').val()),
            step: currencySettings[currency].step,
            values: [30000, 80000],
            altTo: 'fc_price_to',
            altFrom: 'fc_price_from',
            onCreate: function () {
                $('.e_price_slider').removeClass('invisible');

            }
        });
        $('#hotelsSorting').axControls('sortTabs', {
            onSelect: function () {
                //get data
            }
        });
    }
    if ($("#currencySelector").length > 0) {
        $("#currencySelector").chosen();
    }

    $().loadFilters();
    if ($('#hotels_list').length) {
        $('#hotels_list').loadHotelsItems(1);
        $('.hotels_bottom .btn_show_more').on('click', function (e) {
            e.preventDefault();
            var page = $(this).attr('data-next');
            $('#hotels_list').loadHotelsItems(page);
        });
    }
    $('.e_scroll_to_top').on('click', function (e) {
        e.preventDefault();
        $('body, html').scrollTop(0);
    });
    $('#copy_share_btn').copyToClipBoard('share_link');
    $('.e_hotels_share_link_dark').on('click', function (e) {
        e.preventDefault();
        $(this).next('div').toggle();
        $(this).toggleClass('active').toggleClass('light');
    });
    $('.e_share_block .lb_close').on('click', function (e) {
        $(this).parents('.e_share_block').hide();
        $('.e_hotels_share_link_dark').removeClass('active').addClass('light');
    });
    $('.btn_print_hcard').printPage();


    if ($('#hotelcardgal').length > 0) {
        $.fn.calcGalleryWidth = function () {
            var width = 0;
            $('li', this).each(function () {
                width += parseInt($(this).outerWidth());
            });
            return width;
        };
        $('#hotelcardgal').jcarousel({
            wrap: 'circular',
            initCallback: function () {
                var twidth = $('#hotelcardgal').calcGalleryWidth();
                if (twidth < $('.gallery .jcarousel-clip-horizontal').width()) {
                    $('.jcarousel-prev-horizontal, .jcarousel-next-horizontal').hide();
                    $('.gallery .jcarousel-container').addClass('min');
                }
                $('.gallery').removeClass('invisible');
            }
        });
    }

    $('.hotel_card_wrapper .e_rating_mini').on('click', function (e) {
        e.preventDefault();
        $(this).next('.e_rating_info').toggleClass('active');
    });


    if ($('.hotel_card_description').length) {
        $('.hotel_card_description .description_list').columnize({ columns: 2 });
    }

    $('#feedbackList').loadFeedbackItems(1);

    $('.e_people_count').showPeopleCount();

    $('.e_numbers_block .price_tag, .book_price .price_tag').on('click', function (e) {
        e.preventDefault();
        var visible = $('.e_price_details:visible');
        var next = $(this).next('div');
        if (visible.length && visible.get(0) !== next.get(0)) {
            visible.parents('.e_numbers_block').removeClass('active')
            visible.hide();
        }

        next.toggle();
        $(this).parents('.e_numbers_block').toggleClass('active');


    });

    $('.e_price_details .close').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.e_price_details').hide();
        $(this).parents('.e_numbers_block').removeClass('active')
    });

    $('.e_rating_mini.noreact').on('click', function (e) {
        e.preventDefault();
        return false;
    });


    $('#feedback_window .slider').feedbackSlider();
    $('.e_chosen').each(function () {
        $(this).chosen();
    });
    $('.e_text_window .lb_close').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.e_text_window').slideUp(140);
    });
    $('.etb_link').on('click', function (e) {
        e.preventDefault();
        var wid = $(this).attr('data-rel'),
            block = $('#' + wid);
        block.slideToggle(140);
        var offset = block.offset().top;
        $('body, html').scrollTop(offset - 10);
    });

    $('.cancel_book a').cancelBook();

    $('#pay_conf').on('submit', function () {
        $('#pay_conf .ortext').remove();
        $('#pay_conf .btn_go_to_pay').remove();
        $('#pay_conf span').show();
    });

    $('.btn_send_to_mail').bind('click', function (e) {
        e.preventDefault();
        var lnk = $(this).attr('href');
        var obj = $(this);
        $.get(lnk, function () {
            obj.hide().next('b').show();
        });
    });

    $(".pagePrintBtn").printPage();
    $('.e_pay_column .btn_pay_bank').on('click', function (){
        $('.lb_pay_btns_area b').hide();
        $('.lb_pay_btns_area .btn_send_to_mail').show();
    });

    /* Demo code */
    $('.btn_news_subscribe').on('click', function () {
        $('.e_news_subscribe, .subsribe_block').hide();
    });
    $('.btn_cat_subscribe').on('click', function () {
        $('.e_block_catalog_sub').hide();
    });
    $('.btn_logout').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('authorized');
    });
});


function cb_registration_success() {
    $('#registration_success_window').openLightbox();
}

function on_feedback_success() {
    $('#feedback_success_window').openLightbox();
    $('.hotel_card_wrapper .e_rating_info a, .hotel_card_feedback .feedback_btn_wrapper').remove(); //Нужно ли? Или пользователь может флудить сколько захочет?
}
function cb_login_success() {
    $('body').toggleClass('authorized');
    $().closeLightBox();
}
function cb_book_success() {
    window.location = window.location.pathname.replace('/book/', '/pay/');
}