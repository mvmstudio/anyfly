/* RULES SECTION */
var SITE_LANG = 'ru';
var AXF_RULES = {
    registration_form:{
        reg_email:{
            email:{
                ru:'Неверный формат e-mail'
            },
            req:{
                ru:'Обязательное поле'
            },
            min:{
                val:6,
                ru:'Минимум 6 символов'
            }
        },
        reg_password:{
            req:{
                ru:'Обязательное поле',
                en:'Required field'
            },
            password_equal:{
                to:'reg_password_confirm',
                ru:'Пароли не совпадают'
            },
            min:{
                val:6,
                ru:'Минимум 6 символов'
            }
        }
    }
};


