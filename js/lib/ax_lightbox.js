(function( $ ) {
    var LB_MOUSE_IS_INSIDE = false;
    $.fn.axLightbox = function(options) {
        var settings = $.extend( {
            'closeOnClick'  : true,
            'closeOnEscape' : true,
            'beforeShow'    : false,
            'afterShow'    : false
        }, options);




        $(this).on('click', function (e) {
            e.preventDefault();
            var tar = $('#' + $(this).attr('data-rel'));
            tar.openLightbox();
        });

        $.fn.calculatePaddings = function() {
            var scrollTop = $(document).scrollTop();
            var tar = $('.lb_opened');
            var div = $('.lb_opened > div');
            if (tar.length) {
                var top_val = ($(window).height() -  div.height()) / 2 + scrollTop;
                var left_val = ($(window).width() -  div.width()) / 2;
                if (top_val > 0) {
                    tar.width($(window).width());
                    div.css('top', top_val);
                    div.css('left', left_val);
                    $('.lb_bg').height($(document).height()).fadeIn(140);
                }
            }
        };

        $.fn.openLightbox = function (options)
        {

            if (this.length) {
                var obj = this;
                if($('.lb_opened').length) {
                    $('.lb_opened').hide().removeClass('lb_opened');
                }

                if(settings.beforeShow) {
                    settings.beforeShow(obj);
                }


                this.fadeIn(240).addClass('lb_opened');
                $().calculatePaddings();
                if(settings.afterShow) {
                    settings.afterShow(obj);
                }

            }
        };

        $.fn.closeLightBox = function() {
            var tar = $('.lb_opened');
            if (tar.length) {
                if($('input.error', tar).length) {
                    $('form', tar).clearForm();
                }
                $('.lb_bg').fadeOut(140);
                tar.fadeOut(240).removeClass('lb_opened');
            }
        };

        $(window).on('resize', function(){
            $().calculatePaddings();
        });

        $('.lb_trigger').on('click', function (e) {
            e.preventDefault();
            var tar = $('a.lb_link[data-rel=' + $(this).attr('data-rel') + ']');
            if (tar.length) {
                tar.trigger('click');
            }
        });

        $('.lb_hide').on('click', function (e) {
            e.preventDefault();
            $().closeLightBox();
        });

        if(settings.closeOnClick) {
            $('.lb-overlay > div').on('mouseenter mouseleave', function(e){
                if(e.type == 'mouseenter') {
                    LB_MOUSE_IS_INSIDE = true;
                } else {
                    LB_MOUSE_IS_INSIDE = false;
                }
            });
            $('.lb-overlay').on('click', function(){
                if(!LB_MOUSE_IS_INSIDE) {
                    $().closeLightBox();
                }
            });
        }

        $(document).keyup(function (e) {
            if(settings.closeOnEscape) {
                if (e.keyCode == 27) {
                    $('.lb_hide').trigger('click');
                }
            }
        });
    };
})( jQuery );