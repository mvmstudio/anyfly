function strip_tags(str) {
    str = $.trim(str);
    return str.replace(/<\/?[^>]+>/gi, '');
}


$(function () {
    $.fn.clearForm = function () {
        $('input', this).each(function () {
            $(this).removeClass('error');
            $(this).next('span').remove();
        });
    }

    $('.ax_form input[type=submit]').on('click', function (e) {
        e.preventDefault();
        var form_obj = $(this).parents('form');

        var fid = $(form_obj).attr('id');
        var rules = AXF_RULES[fid];
        var formAction = $(form_obj).attr('action');
        var formData = $(form_obj).serialize();
        var backurl = $('input[name=backurl]', form_obj).val();
        var callback = $(form_obj).attr('data-callback');
        var validCount = 0;
        $('input, textarea, .chzn-single-with-drop', form_obj).each(function () {
            if (rules) {
                $(this).ax_checkRequired(rules, 'submit');
                $(this).ax_checkMin(rules, 'submit');
                $(this).ax_checkEmail(rules, 'submit');
                $(this).ax_checkPassEqual(rules, 'submit');
            }
        });

        var errLength = $('input.error', form_obj).length;
        if (errLength == 0) {
            if ($(form_obj).hasClass('noajax')) {
                $(form_obj).submit();
            } else {
                $('#' + fid + ' input[type=submit]').addClass('disabled').attr('disabled', 'disabled');
                $('#' + fid + ' img.loader').show();
                $.ajax({
                    type:"POST",
                    url:formAction,
                    data:formData,
                    success:function (data) {
                        if (data.ERROR) {
                            $('#' + fid + ' input[type=submit]').removeClass('disabled').removeAttr('disabled');
                            $('#' + fid + ' img.loader').hide();
                            console.log(data);
                            if (data.RULES) {
                                $().setErrorFromServer(data.RULES);
                            }
                            if (data.ERROR_TEXT) {
                                $('#' + fid + ' .err_handler').html(data.ERROR_TEXT).slideDown()
                                setTimeout(function () {
                                    $('#' + fid + ' .err_handler').slideUp();
                                }, 3000);
                            }
                        } else {
                            if ($(form_obj).hasClass('resetForm')) {
                                $(form_obj).resetForm();
                            }

                            $('input[type=submit]', form_obj).removeClass('disabled').removeAttr('disabled');
                            $('img.loader', form_obj).hide();
                            if (backurl) {
                                window.location = backurl;
                            }
                            if (callback) {

                                window[callback](data);
                            }
                        }
                    },
                    dataType:'json'
                });
            }
        } else {

        }
    });


    $('.password_field i').on('click', function () {
        $('#show-password').trigger('click');
        $(this).toggleClass('show');
        var parent = $(this).parents('div.password_field');
        var input = parent.find('input');
    });


    $('input, textarea ', '.ax_form').on('blur', function () { // keyup
        var fid = $(this).parents('form').attr('id');
        var rules = AXF_RULES[fid];
        if (rules) {
            $(this).ax_checkRequired(rules, 'blur');
            $(this).ax_checkMin(rules, 'blur');
            $(this).ax_checkEmail(rules, 'blur');
            $(this).ax_checkPassEqual(rules, 'blur');
        }
    });
    $('input#reg_password_confirm').on('blur', function () {
        $('input#reg_password').trigger('blur');
    });

    $.fn.resetForm = function () {
        $('input, textarea', this).each(function () {
            if (!$(this).hasClass('green_big')) {
                $(this).val('');
            }
        });
        $('select option:selected', this).each(function () {
            $(this).removeAttr('selected');
        });
    };

    $.fn.ax_checkRequired = function (rules, e) {
        var fid = $(this).attr('id');
        if (rules[fid]) {
            var req = rules[fid].req;
            if (req) {
                var val = $(this).val();
                if (strip_tags(val).length == 0) {
                    var errText = rules[fid].req[SITE_LANG];
                    if (e != 'blur') {
                        $(this).showErrorText(errText);
                        $(this).removeClass('valid').addClass('error');
                    }
                } else {
                    $(this).removeClass('error').addClass('valid');
                    $(this).removeErrorText();
                    return true;
                }
            }
        }
        return false;
    };

    $.fn.ax_checkMin = function (rules, e) {
        var fid = $(this).attr('id');
        if (rules[fid]) {
            var min = rules[fid].min;
            if (min && min.val) {
                var val = $(this).val();
                if (strip_tags(val).length < min.val) {
                    var errText = rules[fid].min[SITE_LANG];
                    if (e != 'blur') {
                        $(this).showErrorText(errText);
                        $(this).removeClass('valid').addClass('error');
                    }
                } else {
                    $(this).removeClass('error').addClass('valid');
                    $(this).removeErrorText();
                    return true;
                }
            }
        }
        return false;
    };

    $.fn.ax_checkEmail = function (rules, e) {
        var fid = $(this).attr('id');
        if (rules[fid]) {
            var email = rules[fid].email;

            if (email) {
                var val = $(this).val();
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!re.test(val)) {
                    var errText = rules[fid].email[SITE_LANG];
                    if (e != 'blur') {
                        $(this).showErrorText(errText);
                        $(this).removeClass('valid').addClass('error');
                    }
                } else {
                    $(this).removeClass('error').addClass('valid');
                    $(this).removeErrorText();
                    return true;
                }
            }
        }
        return false;
    };

    $.fn.ax_checkPassEqual = function (rules, e) {
        var fid = $(this).attr('id');
        if (rules[fid]) {
            var passEqual = rules[fid].password_equal;

            if (passEqual && $('#' + passEqual.to).val()) {
                var val = $(this).val();
                var conf_val = $('#' + passEqual.to).val();

                if (conf_val && (strip_tags(val) != strip_tags(conf_val))) {
                    var errText = rules[fid].password_equal[SITE_LANG];
                    if (e != 'blur') {
                        $(this).showErrorText(errText);
                        $(this).removeClass('valid').addClass('error');
                    }
                } else {
                    $(this).removeClass('error').addClass('valid');
                    $(this).removeErrorText();
                    return true;
                }
            }
        }
        return false;
    };

    $.fn.setErrorFromServer = function (errRules) {
        //
        for (var name in errRules) {
            var value = errRules[name];
            var input = $('#' + name);
            if (input.length) {
                var errText = value.MSG;
                $(input).addClass('error').showErrorText(errText);


                setTimeout(function () {
                    $(input).removeClass('error').removeErrorText(errText);
                }, 2000);
            }
        }
    };

    $.fn.showErrorText = function (text) {
        $(this).removeErrorText();
        $('<span class="error_text">' + text + '</span>').insertAfter(this);
    };

    $.fn.removeErrorText = function (text) {
        $(this).next('span.error_text').remove();
    };

    $.fn.createCheckboxes = function () {
        $(this).on('change',function () {
            if ($(this).is(':checked')) {
                $(this).next('i').addClass('active');
            } else {
                $(this).next('i').removeClass('active');
            }
        }).each(function () {
                if ($(this).is(':checked')) {
                    $(this).after('<i class="active"></i>');
                } else {
                    $(this).after('<i></i>');
                }
                $(this).parent('label').css('visibility', 'visible');
        });
        return this;
    };

    $('.e_tf_checkbox_label input').createCheckboxes();
});
