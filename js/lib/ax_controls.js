(function ($) {
    function dateDiff(str1, str2) {
        var diff = Date.parse(str2) - Date.parse(str1);
        return isNaN(diff) ? NaN : {
            diff:diff,
            ms:Math.ceil(diff % 1000),
            s:Math.ceil(diff / 1000 % 60),
            m:Math.ceil(diff / 60000 % 60),
            h:Math.ceil(diff / 3600000 % 24),
            d:Math.ceil(diff / 86400000)
        };
    }

    var methods = {
        init:function (options) {
            return this.each(function () {
            });
        },
        counter:function (options) {
            var settings = $.extend({
                'disabled':false,
                'onlyDigits':true,
                'allowNegative':false,
                'maxNumber':false,
                'minNumber':false,
                'defaultValue':false
            }, options);
            $(this).data('options', settings);
            return this.each(function () {
                var axSettings = $(this).data('options');
                var curVal = $(this).val();
                $.fn.incrementCount = function (options) {
                    var next_val = parseInt($(this).val()) + 1;
                    if (options.maxNumber && next_val > options.maxNumber)
                        next_val = options.maxNumber;
                    $(this).val(next_val);
                    return this;
                };
                $.fn.decrementCount = function (options) {
                    var next_val = parseInt($(this).val()) - 1;
                    if (!options.allowNegative && next_val < 0)
                        next_val = 0;
                    if (options.minNumber && next_val < options.minNumber)
                        next_val = options.minNumber;
                    $(this).val(next_val);
                    return this;
                };
                if (axSettings.disabled) {
                    $(this).attr('disabled', 'disabled');
                }
                if (axSettings.defaultValue !== false) {
                    if (!curVal) {
                        $(this).val(axSettings.defaultValue);
                    }

                    $(this).on('blur', function () {
                        if (!$(this).val()) {
                            $(this).val(axSettings.defaultValue);
                        }
                    });
                }
                $(this).after('<a href="#" class="btn btn_count_increment">Больше</a>');
                $(this).before('<a href="#" class="btn btn_count_decrement">Меньше</a>');
                var field = $(this),
                    btnIncrement = $(this).next('.btn'),
                    btnDecrement = $(this).prev('.btn');

                btnIncrement.on('click', axSettings, function (e) {
                    e.preventDefault();
                    $(field).incrementCount(axSettings);
                });
                btnDecrement.on('click', axSettings, function (e) {
                    e.preventDefault();
                    $(field).decrementCount(e.data);
                });
                if (!axSettings.disabled && axSettings.onlyDigits) {
                    field.on('keydown', function (e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
                        return (
                            key == 8 ||
                                key == 9 ||
                                key == 46 ||
                                (key >= 37 && key <= 40) ||
                                (key >= 48 && key <= 57) ||
                                (key >= 96 && key <= 105));
                    });
                }
                if (!axSettings.disabled && axSettings.onlyDigits && axSettings.maxNumber) {
                    field.on('keyup', function () {
                        var val = $(this).val();
                        if (val > axSettings.maxNumber) {
                            $(this).val(axSettings.maxNumber);
                        }
                    });
                }
                if (!axSettings.disabled && axSettings.onlyDigits && axSettings.minNumber) {
                    field.on('keyup', function () {
                        var val = $(this).val();
                        if (val && val < axSettings.minNumber) {
                            $(this).val(axSettings.minNumber);
                        }
                    });
                }
            });
        },
        search:function (options) {
            var settings = $.extend({

            }, options);
            $(this).data('options', settings);
            return this.each(function () {
                var axSettings = $(this).data('options');
                $(this).after('<a class="btn btn_close_name_search" href="#">Закрыть</a>');
                var a_ele = $(this).next('a'),
                    obj = $(this);
                $.fn.showHide = function () {
                    if ($(this).val()) {
                        a_ele.fadeIn(140);
                    } else {
                        a_ele.fadeOut(140);
                    }
                };
                $.fn.removeVal = function () {
                    $(obj).val('');
                    $(this).showHide();
                };
                $(this).showHide();
                $(this).on('keyup', function () {
                    $(this).showHide();
                });
                a_ele.on('click', function (e) {
                    e.preventDefault();
                    obj.removeVal();
                    $(obj).focus();
                });
                $('.e_search_form_content .name_search i').on('click', function (e) {
                    $(this).next('label').trigger('click');
                });
            });
        },
        dates:function (options) {
            var settings = $.extend({
                initFromInputs:false,
                nightsWord:['ночь', 'ночи', 'ночей'],
                daysWord:['день', 'дня', 'дней'],
                fullDaysWord:['сутки', 'суток', 'суток']
            }, options);
            $(this).data('options', settings);
            return this.each(function () {
                var axSettings = $(this).data('options');
                var obj = $(this),
                    word_span = $(this).prev('span'),
                    days_span = word_span.prev('b'),
                    d_checkin = $(this).parent('div').prev('div').find('.e_date_input'),
                    d_checkout = $(this).parent('div').next('div').find('.e_date_input'),
                    i_checkin = d_checkin.next('input'),
                    i_checkout = d_checkout.next('input');


                $.fn.calculateDays = function (start, end) {


                    var days = dateDiff(start, end);
                    $(this).val(days.d);
                    days_span.text(days.d);
                    var word = get_correct_str(days.d, axSettings.nightsWord[0], axSettings.nightsWord[1], axSettings.nightsWord[2]);
                    word_span.text(word);
                };

                $.fn.incrementDate = function (true_date, inc_val) {
                    var day = new Date(true_date);
                    var next = day.setDate(day.getDate() + inc_val);
                    return $.datepicker.formatDate('yy/mm/dd', new Date(next));
                };
                $.fn.decrementDate = function (true_date, inc_val) {
                    var day = new Date(true_date);
                    var next = day.setDate(day.getDate() - inc_val);
                    return $.datepicker.formatDate('yy/mm/dd', new Date(next));
                };

                $.fn.enableCalendars = function () {
                    var changed = false;
                    i_checkin.datepicker({
                        dateFormat:'yy/mm/dd',
                        showOtherMonths:true,
                        minDate:0,
                        defaultDate:0,
                        changeMonth:false,
                        numberOfMonths:1,
                        stepMonths:1,
                        altField:d_checkin,
                        altFormat:"d M",
                        selectOtherMonths:true,
                        showAnim:false,
                        beforeShowDay:function (date) {
                            var this_day = $.datepicker.formatDate('yy/mm/dd', new Date(i_checkout.datepicker("getDate")));
                            var sel_day = $.datepicker.formatDate('yy/mm/dd', new Date(date));
                            var className = '';
                            if (this_day == sel_day) {

                                className = 'checked_day';
                            }
                            return [true, className];
                        },
                        beforeShow:function () {
                            $('#ui-datepicker-div').removeClass('dp_checkout').addClass('dp_checkin');
                        },
                        onSelect:function (selectedDate) {
                            changed = true;
                            i_checkout.datepicker("option", "minDate", $().incrementDate(selectedDate, 1))
                                .datepicker("setDate", $().incrementDate(selectedDate, 1));
                        },
                        onClose:function (selectedDate) {
                            if (changed) {
                                setTimeout(function () {
                                    i_checkout.datepicker("show")
                                }, 100);

                                obj.calculateDays(i_checkin.datepicker("getDate"), i_checkout.datepicker("getDate"));
                            }
                            changed = false;
                        }
                    });
                    i_checkout.datepicker({
                        dateFormat:'yy/mm/dd',
                        showOtherMonths:true,
                        minDate:"+1d",
                        changeMonth:false,
                        numberOfMonths:1,
                        stepMonths:1,
                        altField:d_checkout,
                        altFormat:"d M",
                        selectOtherMonths:true,
                        showAnim:false,
                        beforeShowDay:function (date) {
                            var this_day = $.datepicker.formatDate('yy/mm/dd', new Date(i_checkin.datepicker("getDate")));
                            var sel_day = $.datepicker.formatDate('yy/mm/dd', new Date(date));
                            var className = '';
                            if (this_day == sel_day) {

                                className = 'checked_day';
                            }
                            return [true, className];
                        },
                        beforeShow:function () {
                            $('#ui-datepicker-div').removeClass('dp_checkin').addClass('dp_checkout');
                        },
                        onClose:function (selectedDate) {
                            obj.calculateDays(i_checkin.datepicker("getDate"), i_checkout.datepicker("getDate"));
                        }
                    });

                    if (axSettings.initFromInputs) {
                        i_checkin.datepicker("setDate", i_checkin.val());
                        i_checkout.datepicker("setDate", i_checkout.val());
                        obj.calculateDays(i_checkin.datepicker("getDate"), i_checkout.datepicker("getDate"));
                    }

                    d_checkin.on('click focus', function () {
                        i_checkin.datepicker("show")
                    });
                    d_checkout.on('click focus', function () {
                        i_checkout.datepicker("show")
                    });
                };
                $().enableCalendars();
            });
        },
        cities:function (options) {
            var settings = $.extend({
                nameInput:false,
                codeInput:false
            }, options);
            $(this).data('options', settings);
            return this.each(function () {
                var axSettings = $(this).data('options'),
                    parent = $(this).parents('.e_cl_wrapper');

                $.fn.selectCity = function () {
                    var lnk = $('a', this);
                    if (axSettings.nameInput) {
                        var country = lnk.attr('data-country'),
                            city = lnk.text(),
                            countryCity = city + ', ' + country;
                        $(axSettings.nameInput).val(countryCity);
                        $('.btn_close_name_search').show();
                    }
                    if (axSettings.codeInput) {
                        var cityCode = lnk.attr('data-city-code');
                        $(axSettings.codeInput).val(cityCode);
                    }
                    return this;
                };

                $(this).on('click', function (e) {
                    e.preventDefault();
                    $('.active', parent).removeClass('active');
                    $(this).addClass('active');
                    $(this).selectCity();
                });

                $('.e_cl_wrapper li.active').selectCity();

            });
        },
        formatNumber:function (options) {
            var settings = $.extend({
                round:false,
                currency:'rub'
            }, options);
            $(this).data('options', settings);
            return this.each(function () {
                var axSettings = $(this).data('options'),
                    val = $(this).text().toString();

                if($(this).hasClass('e_text_field')) {
                    val = $(this).val();

                }
                if (axSettings.round) {
                    val = Math.round(val);
                }

                if (axSettings.currency == 'eur' || axSettings.currency == 'usd') {
                    val = addCommas(val);
                } else {
                    val = addCommas(val);
                }
                if($(this).hasClass('e_text_field')) {
                   $(this).val(val);
                } else {
                    $(this).text(val);
                }
            });
        },
        toggleNextBlock:function (options) {
            var settings = $.extend({
                hidden: false,
                storage:true
            }, options);
            $(this).data('options', settings);


            return this.each(function () {
                var toggleId = $(this).attr('data-toggler');
                var axSettings = $(this).data('options');
                if(axSettings.hidden) {
                    $(this).addClass('evt_hidden');
                }

                if(axSettings.storage && $.jStorage && $.jStorage.get(toggleId)) {
                    if($.jStorage.get(toggleId) == 'hidden') {
                        $(this).addClass('evt_hidden');
                    }
                    if($.jStorage.get(toggleId) == 'visible') {
                        $(this).removeClass('evt_hidden');
                    }
                }

                $(this).on('click', function() {
                    if($(this).hasClass('evt_hidden')) {
                        $(this).removeClass('evt_hidden');
                    } else {
                        $(this).addClass('evt_hidden');
                    }


                });

            });
        },
        rangeSlider: function(options) {
            var settings = $.extend({
                min: 0,
                max: 100,
                altTo: false,
                altFrom: false,
                values: false,
                onCreate: false
            }, options);
            $(this).data('options', settings);


            return this.each(function () {
                var axSettings = $(this).data('options'),
                    values = axSettings.values ? axSettings.values : [ axSettings.min, axSettings.max ];
                $(this).slider({
                    range: true,
                    min: axSettings.min,
                    max: axSettings.max,
                    step: axSettings.step,
                    values: values,
                    create: function( event, ui ){

                        $('#' + axSettings.altFrom).val(values[0]);
                        $('#' + axSettings.altTo).val(values[1]);

                        if (typeof axSettings.onCreate == "function") {
                            axSettings.onCreate();
                        }
                    },
                    slide: function( event, ui ) {
                        $('#' + axSettings.altFrom).val(ui.values[0]);
                        $('#' + axSettings.altTo).val(ui.values[1]);
                    }
                });
                $('#' + axSettings.altFrom).on('keydown', function (e) {
                    return false;
                });
                $('#' + axSettings.altTo).on('keydown', function (e) {
                    return false;
                });
            });
        },
        sortTabs: function(options) {
            var settings = $.extend({
                min: 0,
                max: 100,
                altTo: false,
                altFrom: false,
                values: false,
                onCreate: false
            }, options);
            $(this).data('options', settings);

            return this.each(function () {
                var axSettings = $(this).data('options');
                $('li', this).on('click', function(){
                    $(this).addClass('active').siblings('li').removeClass('active');
                });
            });
        }
    };

    $.fn.axControls = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.axControls');
        }
    };
})(jQuery);


function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}