// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function noop() {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

function get_correct_str($num, $str1, $str2, $str3) {
    var val = $num % 100;

    if (val > 10 && val < 20) return $str3;
    else {
        val = $num % 10;
        if (val == 1) return $str1;
        else if (val > 1 && val < 5) return $str2;
        else return $str3;
    }
}


var SOCIAL_MORE_MOUSE_ENTER = false;


$(function () {

    $.fn.focusAndClearForm = function (focus_field) {
        $(this).on('click', function () {
            $('input#' + focus_field).focus();
            $('#' + $(this).attr('rel')).clearForm();
        });
    };
    $.fn.loadTabContent = function () {
        $('li', this).on('click', function () {
            $(this).addClass('active').siblings('li').removeClass('active');
            var ref = $(this).attr('data-ref');
            if (ref) {
                window.location = ref;
            }
        });
    };
    $.fn.showSocialMore = function () {
        $(this).on('click', function () {
            $(this).toggleClass('active');
            $(this).next('.e_more_social').toggle();
        });
    };

    $.fn.staticSearchForm = function () {
        var obj = $(this);
        $(window).on('scroll', function () {
            var scrollTop = $(window).scrollTop() - 100;

            if (obj.scrollTop() <= scrollTop) {
                obj.css({
                    position:'fixed',
                    top:-63
                });
            } else {
                obj.css({
                    position:'absolute',
                    top:40
                });
            }
        });
    };

    $.fn.createStarsLine = function () {
        $(this).each(function () {
            var stars_count = $(this).text();
            $(this).text('');
            if (stars_count > 0) {
                for (var i = 1; i < 6; i++) {
                    if (i <= stars_count) {
                        var sep = '';
                        if ($(this).hasClass('big')) {
                            sep = '<b></b>';
                        }
                        $(this).append(sep + '<i class="active">*</i>');
                    } else {
                        if (!$(this).hasClass('big')) {
                            $(this).append('<i>*</i>');
                        }
                    }
                }

            }
        });
    };

    $.fn.addNightsText = function () {
        $(this).text($('#__nights_count_text').text());
    };

    $.fn.loadHotelsItems = function (page) {
        var obj = $(this);
        var template = $('#hotelCard').html();
        var btn_show_more = $('.hotels_bottom .btn_show_more');
        $.get('/test/hotels.php', {page:page}, function (data) {
            if (data && data.list) {
                var content = Mustache.to_html(template, data);
                obj.append(content);
                $('.e_stars_line', obj).createStarsLine();

                $('.e_hotel_item .nights i').addNightsText();
                if (page < data.pages) {
                    btn_show_more.attr('data-next', parseInt(page) + 1);
                    btn_show_more.show();
                } else {
                    btn_show_more.hide();

                }
                $('.hotels_bottom .show_total').text(data.show);
                $('.hotels_bottom i').text(get_correct_str(data.show, 'отель', 'отеля', 'отелей'));
                $('.hotels_bottom .total').text(data.total);

                $('.hotels_bottom').removeClass('invisible');
                $('.filters_block .h_count')
                    .text('Найдено ' + data.total + ' ' + get_correct_str(data.total, 'отель', 'отеля', 'отелей'))
                    .axControls('formatNumber')
                    .removeClass('invisible');

                $('.e_hotel_map_focus').loadMapBox();

                $(".pagePrintBtn").printPage();

                setTimeout(function () {
                    $('.inner_page .index_content_block').addClass('loaded');
                }, 200);
            }
        });
    };

    $.fn.loadFeedbackItems = function (page) {
        var obj = $(this);
        var template = $('#feedbackItem').html();
        var btn_show_more = $('.fb_show_more', obj);

        if($(this).length) {
            $.fn.loadItems = function (page) {

                $.get('/test/feedback.php', {page:page}, function (data) {
                    if (data && data.list) {
                        var content = Mustache.to_html(template, data);

                        $('.feedback_list', obj).append(content);

                        if (page < data.pages) {
                            btn_show_more.attr('data-next', parseInt(page) + 1);
                            btn_show_more.show();
                        } else {
                            btn_show_more.hide();
                        }

                        var total_str = data.total + ' ' + get_correct_str(data.total, 'отзыв', 'отзыва', 'отзывов');
                        $('.hotel_card_wrapper .feedback_anchor ').text(total_str).show();
                        $('.fb_count', obj).text(total_str);

                        $('.e_rating_mini.noreact').on('click', function (e) {
                            e.preventDefault();
                            return false;
                        });

                    }
                });
            };

            $(this).loadItems(page);


            btn_show_more.on('click', function (e) {
                e.preventDefault();
                var page = $(this).attr('data-next');
                $(obj).loadItems(page);
            });
        }
    };


    $.fn.loadFilters = function () {

        if ($('.fc_stars_filters').length) {
            $.get('/test/lists.php', {type:'stars'}, function (data) {
                var template = $('#filterStars').html();
                var content = Mustache.to_html(template, data);
                $('.fc_stars_filters').html(content);
                $('.fc_stars_filters input').createCheckboxes().attr('name', 'fc_stars[]');
            });
        }
        if ($('.fc_locations').length) {
            $.get('/test/lists.php', {type:'locations'}, function (data) {
                var template = $('#filterOptionsList').html();
                var content = Mustache.to_html(template, data);
                $('.fc_locations ul').html(content);
                $('.fc_locations input').createCheckboxes().attr('name', 'fc_locations[]');
            });
        }
        if ($('.fc_zones_list').length) {
            $.get('/test/lists.php', {type:'zones'}, function (data) {
                var template = $('#filterOptionsList').html();
                var content = Mustache.to_html(template, data);
                $('.fc_zones_list').html(content);
                $('.fc_zones_list input').createCheckboxes().attr('name', 'fc_zones[]');
            });
        }
        if ($('.fc_facilities').length) {
            $.get('/test/lists.php', {type:'facilities'}, function (data) {
                var template = $('#filterOptionsList').html();
                var content = Mustache.to_html(template, data);
                $('.fc_facilities ul').html(content);
                $('.fc_facilities input').createCheckboxes().attr('name', 'fc_facilities[]');
            });
        }
        if ($('.fc_food_types').length) {
            $.get('/test/lists.php', {type:'food'}, function (data) {
                var template = $('#filterOptionsList').html();
                var content = Mustache.to_html(template, data);
                $('.fc_food_types ul').html(content);
                $('.fc_food_types input').createCheckboxes().attr('name', 'fc_food_types[]');
            });
        }
    };

    ZeroClipboard.setDefaults({ moviePath:'/js/vendor/ZeroClipboard.swf' });
    $.fn.copyToClipBoard = function (content_id) {

        var cont = $('#' + content_id);
        if (cont.length > 0) {

            var clip = new ZeroClipboard(cont);
            clip.on('complete', function (client, args) {
                $(this).removeClass('btn').addClass('copied').text('Скопировано');
            });

            clip.on('mousedown', function (client, args) {
                clip.setText(cont.val());
            });

            clip.glue($(this));
        }
    }


    $.fn.showPeopleCount = function () {
        this.each(function () {
            var count = $(this).text();
            var num_count = $(this).parents('.num_line').attr('data-count');
            $(this).text('');
            var i_src = '/i/e/man.png';
            if ($(this).hasClass('mini')) {
                i_src = '/i/e/man_mini.png';
            }
            for (var i = 0; i < count; i++) {
                $(this).append('<img src="' + i_src + '" alt="" />')
            }
            $(this).removeClass('invisible');
            $('.num_count').removeClass('invisible');
            $(this).attr('title', count + ' ' + get_correct_str(count, 'человек', 'человека', 'человек'));
        });
    };

    $.fn.feedbackSlider = function () {
        this.each(function () {
            var altTo = $('input', this),
                obj = $(this);

            $('.slider_container', this).slider({
                range:"min",
                value:0,
                min:0,
                max:5,
                step:0.5,
                slide:function (event, ui) {
                    $(altTo).val(addCommas(ui.value));
                }
            });
            $(altTo).on('focus', function () {
                if ($(this).val() == 0)
                    $(this).val('');
            });
            $(altTo).on('blur', function () {
                if ($(this).val() == 0)
                    $(this).val(0);
            });

            $(altTo).on('keyup', function (e) {
                var val = $(this).val();


                if (/^\d+\,?\d?$/.test(val)) {
                    var true_val = val.replace(',', '.');

                    if (true_val > 5) {
                        true_val = 5;
                        val = 5;
                    }
                    $(this).val(val);
                    $(this).attr('data-prev', val);
                    $('.slider_container', obj).slider('value', true_val);
                } else {
                    $(this).val('');
                }

            });
        });
    };

    $.fn.cancelBook = function() {
        $(this).each(function(){
            $(this).on('click', function(e){
                e.preventDefault();
                var link = $(this).attr('href');
                var obj = $(this);
                var parent = $(this).parent('div');
                $.get(link, function(data) {
                    if(data.ERROR) {
                        obj.after('<span class="error_text">' + data.ERROR + '</span>');
                    }
                    if(data.SUCCESS) {
                        obj.remove();
                        parent.text('Запрос отправлен');

                        setInterval(function(){
                            $('.e_pay_column .book_pay_opts').animate({ opacity: 0 }, 340, function(){
                                $(this).slideUp(300, function(){
                                    $(this).remove();
                                    $('.e_pay_column').addClass('e_pay_canceled');
                                });
                            });
                        }, 500);

                        $('.e_pay_column .status b').text('Ожидание отмены');
                    }
                });
            });
        });
    }

});
