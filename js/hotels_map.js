/* Contants in scope_objects.js */
$(function () {
    var mapOptions = {
        zoom:13,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        scrollwheel:true,
        disableDefaultUI:true,
        zoomControl:true,
        zoomControlOptions:{
            position:google.maps.ControlPosition.RIGHT_CENTER
        }
    };
    function closeInfobox() {
        if (opened_infobox) opened_infobox.close()
    }
    function openInfobox(infobox, pos, obj) {
        closeInfobox();
        infobox.open(h_map, obj);
        h_map.panTo(pos);
        opened_infobox = infobox;
    }
    function findMarkerById(hid) {
        var return_item = false;
        $.each(markers, function (index, item) {

            if (item.id == parseInt(hid)) {
                return_item = item;
            }
        });
        return return_item;
    }
    function initialize_hotels_map(data) {
        if (h_map)  {
            return true;
        } else {
            var points_list = data.list;
            var geocoder = new google.maps.Geocoder();
            var default_location = $('#__location').text();
            console.log(default_location);
            geocoder.geocode({ 'address': default_location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    default_center = results[0].geometry.location;
                    mapOptions.center = results[0].geometry.location;
                    var hotelsMap = document.getElementById("map_window_map");
                    h_map = new google.maps.Map(hotelsMap, mapOptions);
                    var i = 0;
                    $.each(points_list, function (index, item) {
                        var hotel = item.HOTEL;
                        if (hotel) {
                            var marker = new google.maps.Marker({
                                id:hotel.ID,
                                position:new google.maps.LatLng(hotel.GPS.LAT, hotel.GPS.LNG),
                                title:hotel.NAME,
                                icon:'/i/map/hotel_icon.png'
                            });
                            markers[i] = marker;
                            var infobox = new InfoBox({
                                id:hotel.ID,
                                content:document.getElementById("h_infobox_" + hotel.ID),
                                disableAutoPan:false,
                                maxWidth:284,
                                pixelOffset:new google.maps.Size(-162, -42),
                                zIndex:null,
                                boxStyle:{
                                    background:"",
                                    opacity:1,
                                    width:"282px"
                                },
                                closeBoxURL:"",
                                infoBoxClearance:new google.maps.Size(1, 1),
                                contextmenu:true,
                                alignBottom:true
                            });
                            marker.setMap(h_map);
                            i++;
                            google.maps.event.addListener(marker, 'click', function () {
                                openInfobox(infobox, marker.getPosition(), this)
                            })
                        }
                    });
                    google.maps.event.addListener(h_map, 'click', function () {
                        closeInfobox();
                    });
                    google.maps.event.addListener(h_map, 'zoom_changed', function () {
                        if (opened_infobox) opened_infobox.draw();

                    });
                } else {
                    $('#map_window_map').html('<p class="error e_ta_center e_fs_xxxl">При загрузке карты возникли ошибки.<br />Попробуйте <a href="#" class="e_sl" onclick="window.location.reload()">перезагрузить страницу</a>.</p>');
                }
            });
            return true;
        }
    }
    $.fn.loadMapBox = function () {
        $(this).on('click', function () {
            var hid = $(this).attr('data-hid'),
                marker = findMarkerById(hid);
            $('#map_window').openLightbox();
            google.maps.event.trigger(marker, 'click');
        });
    };
    $.fn.appendInfoboxes = function (data) {
        var template = $('#hotelInfobox').html();
        var content = Mustache.to_html(template, data);
        $(this).append(content);
        $('.infobox_content .e_stars_line').createStarsLine();
        $('.infobox_content .nights i').addNightsText();
        if($('body').hasClass('hotel_card_page')) {
            var hotels_count = data.total + ' ' + get_correct_str(data.total, 'отель', 'отеля', 'отелей');
            $('.e_back_to span').text('(' + hotels_count + ')');

            setTimeout(function(){
                $('.inner_page .index_content_block').addClass('loaded');
            }, 200);
            $('.e_hotel_map_focus').loadMapBox();
        }
    };

    $.fn.getHotelsPoints = function () {
        $.get('/test/hotels.php', { page:'gps' }, function (data) {
            $('body').appendInfoboxes(data);
            initialize_hotels_map(data);
        });
    };
    $().getHotelsPoints();
    $('.map_lb_link').axLightbox({
        beforeShow:function (obj) {
            var h4 = $('h4', obj),
                defaultLocation = $('#__location').text();
            h4.text(defaultLocation);
        },
        afterShow:function (obj) {
            google.maps.event.trigger(h_map, 'resize');

            if(h_map) {
                h_map.setCenter(default_center);
            }
            closeInfobox();
        }
    });
});