<? header('Content-Type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Elements prototypes list</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/buttons.css">
    <link rel="stylesheet" href="../css/widgets.css">
    <link rel="stylesheet" href="../css/search.css">

    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/elements.css">


    <script src="../js/vendor/modernizr-2.6.2.min.js"></script>

    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">


    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: 3447858, onlyWidgets: true});
    </script>
</head>
<body class="index_page">
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->

<div id="main">
<div id="header">
    <h1 class="e_mh e_bliss_bold e_white e_ts_dark">Elements prototypes list</h1>
</div>
<div id="content">
<div id="sections_list" class="e_texture_white e_ta_left">
    <ol class="e_list">
        <li><a class="e_bl" href="#helpers">Helpers</a></li>
        <li><a class="e_bl" href="#links">Links</a></li>
        <li><a class="e_bl" href="#headers">Headers</a></li>
        <li><a class="e_bl" href="#striked_headers">Striked headers</a></li>
        <li><a class="e_bl" href="#starred_headers">Starred headers</a></li>
        <li><a class="e_bl" href="#buttons">Buttons</a></li>
        <li><a class="e_bl" href="#widgets">Widgets</a></li>
        <li><a class="e_bl" href="#lightboxes">Lightboxes</a></li>
        <li><a class="e_bl" href="#hotels_search">Hotels search</a></li>
    </ol>
</div>
<!--<div class="cont_wrapper">

    <h3 class="e_mh e_bliss_bold e_mh_striked"><span>ToDo List</span></h3>

    <div class="cont_block">
        <dl class="e_list e_list_num e_ta_left e_size_s" style="width: 30%; margin: 0 auto;">
            <dt>Buttons with icons</dt>
            <dd>Email, Profile, etc</dd>

            <dt>Social icons</dt>
            <dd>Facebok, Twitter, Vkontakte etc</dd>

            <dt>Create menu</dt>
            <dd>And put this list below</dd>

            <dt>Layots</dt>
            <dd>Simle, 2-3-4 columns</dd>


            <dt>Paragraphs, lists, etc</dt>
            <dd>Real text examples</dd>

            <dt>Tables, forms</dt>
            <dd>Simple table, simple form with validation</dd>
        </dl>
    </div>
</div>-->
<div class="cont_wrapper e_texture_gray">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="helpers">Helpers</a></span></h3>

    <div class="cont_block">
        <div class="cont_div"><span class="e_dark">Normal type</span></div>

        <div class="cont_div cont_div_dark"><span class="e_white">White type</span></div>
        <div class="cont_div"><span class="e_bold">Bold</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="e_nobold">Nobold</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="e_dark e_italic">Italic</span></div>
        <div class="cont_div"><span class="e_bliss_bold">BlissProBold</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="e_bliss">BlissProRegular</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="e_bliss_light">BlissProLight</span></div>

    </div>
</div>
<div class="cont_wrapper e_texture_white">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="links">Links</a></span></h3>

    <div class="cont_block">
        <div class="cont_div"><a href="#" class="e_sl">Simple link</a></div>
        <div class="cont_div"><a href="#" class="e_bl e_dark">Bordered link dark</a></div>
        <div class="cont_div cont_div_dark"><a href="#" class="e_bl e_white">Bordered link light</a></div>
        <div class="cont_div"><a href="#" class="e_aj">Ajax link</a></div>
        <div class="cont_div"><a href="#" class="e_dsh">Dashed link</a></div>
    </div>
</div>
<div class="cont_wrapper e_texture_white">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="headers">Headers</a></span></h3>

    <div class="cont_block">
        <h1 class="e_mh e_bliss_bold">h1. Heading 1 (Bliss Bold)</h1>

        <h2 class="e_mh e_bliss_bold">h2. Heading 2 (Bliss Bold)</h2>

        <h3 class="e_mh e_bliss e_dark">h3. Heading 3 (Bliss Regular)</h3>
        <h4 class="e_mh e_bliss e_dark">h4. Heading 4 (Bliss Regular)</h4>
        <h5 class="e_mh e_bliss e_dark">h5. Heading 5 (Bliss Regular)</h5>
    </div>
</div>
<div class="cont_wrapper e_texture_gray">
    <h3 class="e_mh e_bliss_light e_mh_striked e_dark"><span><a name="striked_headers">Striked headers</a></span></h3>

    <div class="cont_block">
        <h1 class="e_mh e_bliss_bold e_mh_striked e_dark"><span>h1. Heading 1 (Bliss Bold)</span></h1>

        <h2 class="e_mh e_bliss_bold e_mh_striked e_dark"><span>h2. Heading 2 (Bliss Bold)</span></h2>

        <h3 class="e_mh e_bliss e_mh_striked e_dark"><span>h3. Heading 3 (Bliss Regular)</span></h3>
    </div>
</div>
<div class="cont_wrapper e_texture_gray">
    <h3 class="e_mh e_bliss_light e_mh_striked e_dark"><span><a name="starred_headers">Starred headers</a></span></h3>

    <div class="cont_block">
        <h2 class="e_mh e_bliss_bold e_mh_striked e_dark e_mh_starred"><span><i>Heading 2 Bliss Bold</i></span></h2>

        <h3 class="e_mh e_bliss e_mh_striked e_dark e_mh_starred"><span><i>Heading 3 Bliss Reg</i></span></h3>

        <h3 class="e_mh e_bliss_light e_mh_striked e_dark e_mh_starred"><span><i>Heading 3 Bliss Light</i></span></h3>
    </div>
</div>
<div class="cont_wrapper e_texture_gray">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="buttons">Buttons</a></span></h3>

    <div class="cont_block">
        <div class="cont_div "><a href="#" class="btn btn_search">Найти</a></div>
        <br/>

        <div class="cont_div "><a href="#" class="btn btn_book_xl_blue">Забронировать</a></div>
        <div class="cont_div "><a href="#" class="btn btn_book_xl_orange">Забронировать</a></div>
        <br/>

        <div class="cont_div"><a href="#" class="btn btn_book_l_blue">Забронировать</a></div>
        <div class="cont_div"><a href="#" class="btn btn_book_l_orange">Забронировать</a></div>
        <br/>

        <div class="cont_div"><a href="#" class="btn btn_book_enter">Вход</a><a href="#" class="btn btn_book_reg">Регистрация</a>
        </div>

        <div class="cont_div"><a href="#" class="btn btn_book_enter_xl">Вход</a><a href="#" class="btn btn_book_reg_xl">Регистрация</a>
        </div>
        <br/>

        <div class="cont_div"><a href="#" class="btn btn_show_more">Показать ещё</a></div>
        <div class="cont_div"><a href="#" class="btn btn_copy">Скопировать</a></div>
        <div class="cont_div"><a href="#" class="btn btn_subscribe_xl">Подписаться</a></div>
        <br/>

        <div class="cont_div"><a href="#" class="btn btn_subscribe">Подписаться</a></div>
        <br/>
        <div class="cont_div"><a href="#" class="btn btn_refresh">Обновить</a></div>
        <br/>

        <div class="cont_div"><a href="#" class="btn btn_facebook">Facebook</a></div>
        <div class="cont_div"><a href="#" class="btn btn_twitter">Twitter</a></div>
        <div class="cont_div"><a href="#" class="btn btn_vkontakte">Vkontakte</a></div>
    </div>
</div>
<div class="cont_wrapper e_texture_gray">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="widgets">Widgets</a></span></h3>

    <div class="cont_block">
        <div class="cont_div">
            <div class="e_block e_block_spec e_bs_dark">
                <div class="img">
                    <a href="#"><img src="/i/demo/offer_1.jpg" alt="Венецианский карнавал"/></a>

                    <div class="annotation e_ta_left">
                        <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Венецианский карнавал 2013</a></h4>

                        <p class="e_price e_fs_xxxl">от 2 409 <span class="e_ruble">a</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="cont_div">
            <div class="e_block e_block_sug_hotel e_bs_dark">
                <div class="cont">
                    <div class="img e_left"><a href="#"><img src="../i/demo/sug_hotel_1.png"
                                                             alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
                    </div>
                    <div class="info e_right e_ta_left">
                        <div class="stars_line">
                            <div class="e_stars_line e_left">
                                <i class="active"></i>
                                <i class="active"></i>
                                <i class="active"></i>
                                <i></i>
                                <i></i>
                            </div>
                            <div class="e_price e_fs_xl e_right e_ta_right">от 82 409 <span class="e_ruble">a</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite
                            Days Inn Southwest</a></div>
                        <div class="location e_fs_s">Рим, Италия</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <br/>

            <div class="e_block e_block_sug_hotel e_bs_dark">
                <i class="e_action_s"></i>

                <div class="cont">
                    <div class="img e_left"><a href="#"><img src="../i/demo/sug_hotel_1.png"
                                                             alt="Knossos Beach Hotel Bungalows Suite Days Inn Southwest"/></a>
                    </div>
                    <div class="info e_right e_ta_left">
                        <div class="stars_line">
                            <div class="e_stars_line e_left">
                                <i class="active"></i>
                                <i class="active"></i>
                                <i class="active"></i>
                                <i></i>
                                <i></i>
                            </div>
                            <div class="e_price e_fs_xl e_right e_ta_right">от 82 409 <span class="e_ruble">a</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="name e_bliss_medium"><a href="#" class="e_blh">Knossos Beach Hotel Bungalows Suite
                            Days Inn Southwest</a></div>
                        <div class="location e_fs_s">Рим, Италия</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br/><br/>

        <div class="cont_div">
            <div class="e_block e_block_popular_city">
                <div class="img e_bs_dark">
                    <a href="#"><img src="/i/demo/pop_city_1.png" alt="Амстердам"/></a>
                </div>
                <div class="info e_ta_left">
                    <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Амстердам</a> <span
                            class="hotels_count e_arial e_nobold e_fs_m">– <b>147</b> отелей</span></h4>

                    <div class="country"><a href="#" class="e_slh e_flag"><i
                            style="background-image: url('/i/demo/flags/spain.png')"></i>Нидерланды</a></div>
                </div>
            </div>
        </div>

        <div class="cont_div" style="top: -107px;">
            <div class="e_block e_bs_dark e_block_subscribe e_ta_left">
                <div class="top"></div>
                <div class="form">
                    <h5 class="e_mh e_bliss_light"><label for="subscribe_email">Подписаться на новости</label></h5>

                    <form method="get" id="subscribe_form">
                        <input type="text" name="email" class="e_text_field e_tf_mini" id="subscribe_email"
                               placeholder="Ваша электронная почта"/>
                        <input type="submit" value="Подписаться" class="btn btn_subscribe"/>
                    </form>
                </div>
            </div>
        </div>
        <br/><br/>

        <div class="cont_div">
            <div class="e_block e_news_item e_floatable e_ta_left">
                <div class="date e_left e_ta_center">
                    <div class="day e_bliss_bold e_ts_white e_bliss_bold e_fs_xxl">21</div>
                    <div class="month e_fs_s">февраля</div>
                </div>
                <div class="info e_left e_ta_left">
                    <h5 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Визу в Турцию упростили</a></h5>

                    <div class="cont">
                        <a href="#">МИД Турецкой республики проинформировал, что с&nbsp;1&nbsp;января 2013 года
                            гражданам, у
                            которых нет в&nbsp;паспорте свободного места для штампа, визовой марки или отметки, не&nbsp;смогут
                            въехать в&nbsp;страну.</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="cont_div">
            <div class="e_feedback e_ta_left">
                <div class="cont e_ta_center">
                    <div class="text e_fs_x">
                        <table>
                            <tr>
                                <td>«Нельзя просто так взять и&nbsp;решить куда поехать отдыхать, но&nbsp;портал AnyFly
                                    решил эту проблему, за&nbsp;это ему очень благодарен!»
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="sig ">
                    <div class="photo e_left"><img src="/i/demo/photo.png" alt="Константинопольский Констатин"/></div>
                    <div class="name e_left">
                        <h5 class="e_mh e_bliss_bold">Константинопольский Константин</h5>

                        <p>ОАО «Русское географическое общество»<br/>Путешественник</p>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>


        </div>


    </div>
</div>
<div class="cont_wrapper e_texture_white">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="lightboxes">Lightboxes</a></span></h3>

    <div class="cont_block">

        <div class="cont_div">
            <div class="lb-overlay" id="registration_success_window">
                <div class="">
                    <a href="#" class="lb_close lb_hide"></a>


                    <div class="lb_content e_ta_center">
                        <h3 class="e_mh_lb e_bliss_light e_ts_white">Спасибо!</h3>

                        <div class="lb_text">
                            <p>На указанный вами E-mail было<br />выслано письмо с активацией вашей<br />учетной записи.</p>
                        </div>



                        <div class="buttons_area">
                            <a class="btn btn_continue lb_hide">Продолжить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="cont_div">
            <div class="lb-overlay" id="login_window">
                <div class="">
                    <a href="#" class="lb_close lb_hide"></a>


                    <div class="lb_content e_ta_left">
                        <form method="post" class="lb_form ax_form" id="login_form" action="/test/login.php" callback="cb_login_success">
                            <h3 class="e_mh_lb e_bliss_light e_ts_white">Регистрация</h3>

                            <div class="lb_text">
                                <div class="e_input_line ">
                                    <label class="e_tf_label e_fs_x" for="login_email">E-mail</label>
                                    <input type="text" name="email" class="e_text_field" id="login_email" />
                                </div>
                                <div class="e_input_line e_input_group">
                                    <label class="e_tf_label e_fs_x" for="login_password">Пароль</label>
                                    <input type="password" name="password" class="e_text_field" id="login_password" />
                                </div>
                                <div class="e_input_line">
                                    <label class="e_tf_checkbox_label" for="login_remember_me"><input type="checkbox" name="remember_me" id="login_remember_me" class="e_f_checkbox"/>
                                        <i></i><span>Запомнить меня</span></label>
                                </div>
                            </div>

                            <div class="err_handler"></div>


                            <div class="buttons_area center">
                                <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_enter_xl"/>

                                <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="cont_div">
            <div class="lb-overlay" id="registration_window">
                <div class="">
                    <a href="#" class="lb_close lb_hide"></a>


                    <div class="lb_content e_ta_left">
                        <form method="post" class="lb_form ax_form" id="registration_form" action="/test/reg.php">
                            <h3 class="e_mh_lb e_bliss_light e_ts_white">Регистрация</h3>

                            <div class="lb_text">
                                <div class="e_input_line e_input_group">
                                    <label class="e_tf_label e_fs_x" for="reg_email">E-mail</label>
                                    <input type="text" name="email" class="e_text_field" id="reg_email" autocomplete="off"/>
                                    <p class="e_tf_annotation e_fs_s">На указанный выше e-mail придет<br />запрос на подтверждение регистрации</p>
                                </div>
                                <div class="e_input_line">
                                    <label class="e_tf_label e_fs_x" for="reg_password">Пароль</label>
                                    <input type="password" name="password" class="e_text_field" id="reg_password" autocomplete="off" />
                                </div>
                                <div class="e_input_line e_input_group">
                                    <label class="e_tf_label e_fs_x" for="reg_password_confirm">Подтверждение пароля</label>
                                    <input type="password" name="password_confirm" class="e_text_field" id="reg_password_confirm" autocomplete="off" />
                                </div>
                                <div class="e_input_line e_input_group">
                                    <label class="e_tf_label e_fs_x" for="reg_promo_code">Промо-код</label>
                                    <input type="text" name="promo_code" class="e_text_field" id="reg_promo_code" autocomplete="off" />
                                </div>
                                <div class="e_input_line">
                                    <label class="e_tf_checkbox_label" for="reg_jur_lico"><input type="checkbox" name="jur_lico" id="reg_jur_lico" class="e_f_checkbox"/>
                                        <i></i><span>Юридическое лицо</span></label>
                                </div>
                            </div>

                            <div class="err_handler"></div>


                            <div class="buttons_area center">
                                <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_reg_xl"/>

                                <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="cont_wrapper e_texture_white" style="width: 1004px;">
    <h3 class="e_mh e_bliss_light e_mh_striked"><span><a name="hotels_search">Hotels search</a></span></h3>

    <div class="cont_block" style='background: url("../i/index_book_block_bg.jpg") 0 0 repeat-x; position: relative; width: 1004px; padding-top: 16px; min-height: 0; height: 70px;'>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs e_st_3">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active"><span>Отели</span><i></i></li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                </ul>

            </div>
            <div class="e_search_form_content" style="min-height: 0; height: 20px;"></div>
        </div>
    </div>

    <div class="cont_block" style='background: url("../i/index_book_block_bg.jpg") 0 0 repeat-x; height: 70px; position: relative; width: 1004px; padding-top: 16px'>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs e_st_4">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active"><span>Отели</span><i></i></li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                </ul>

            </div>
            <div class="e_search_form_content" style="min-height: 0; height: 20px;"></div>
        </div>
    </div>

    <div class="cont_block" style='background: url("../i/index_book_block_bg.jpg") 0 0 repeat-x; height: 454px; position: relative; width: 1004px; padding-top: 16px'>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active"><span>Отели</span><i></i></li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                    <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                </ul>
                <div class="e_social_line">
                    <div id="vk_like"></div>
                    <script type="text/javascript">
                        VK.Widgets.Like("vk_like", {type: "button", height: 18});
                    </script>

                    <div class="fb-like" data-href="http://www.anyfly.ru" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>

                    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                    <a href="#" class="btn btn_social_more">Ещё</a>
                    <? /* ToDo: Полный список лайков */ ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <form method="post" action="/test/search.php">
                <div class="e_search_form_content">
                    <div class="e_search_line">
                        <div class="e_left name_search">
                            <i></i>
                            <label class="e_tf_label e_bold" for="hs_search_name">Страна, город или отель</label>
                            <input class="e_text_field e_fs_x e_bold" name="name" value="" id="hs_search_name" autocomplete="off"/>
                            <input type="hidden" id="__city_code" name="city_code" value="" autocomplete="off"/>
                        </div>
                        <div class="e_left date_checkin">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkin">Заезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkin_str" value="" id="hs_date_checkin" autocomplete="off"/>
                            <input type="text" name="date_checkin" id="__date_checkin" class="visuallyhidden" value="<?=date('Y/m/d')?>"/>
                        </div>
                        <div class="e_left e_date_days_count e_ta_center">
                            <b>0</b><span>ночей</span>
                            <input type="hidden" id="__nights_count" name="nights_count" value="0" autocomplete="off"/>
                        </div>
                        <div class="e_left date_checkout">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkout">Выезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkout_str" value="" id="hs_date_checkout" autocomplete="off"/>
                            <input type="text" name="date_checkout" id="__date_checkout" class="visuallyhidden" value="<?=date('Y/m/d', strtotime('+1 day'))?>"/>
                        </div>
                        <div class="e_left e_sl_count_selector adults_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_adults_count">Взрослых</label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="adults_count" id="hs_adults_count" autocomplete="off"/>
                        </div>
                        <div class="e_left e_sl_count_selector children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_children_count">Детей <span class="e_fs_s e_nobold">2–15 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="children_count" id="hs_children_count" autocomplete="off"/>
                        </div>
                        <div class="e_left e_sl_count_selector l_children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_l_children_count"><span class="e_fs_s e_nobold">до 2 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="l_children_count" id="hs_l_children_count" autocomplete="off"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="e_cities_list">
                        <p class="e_bold e_fs_m">Популярные города</p>
                        <div class="e_cl_wrapper">
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Австрия" data-city-code="1"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Вена</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Чехия" data-city-code="2"><i
                                            style="background-image: url('/i/demo/flags/spain.png')"></i>Прага</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Монте-Карло" data-city-code="3"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Монте-Карло</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Франция" data-city-code="4"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Париж</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Германия" data-city-code="5"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Берлин</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Греция" data-city-code="6"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Крит</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Греция" data-city-code="7"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Родос</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Венгрия" data-city-code="8"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Бадапешт</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="9"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Флоренция</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="10"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Милан</a></li>
                                <li class="active"><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="11"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Рим</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="12"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Сорренто-Неаполь</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Италия" data-city-code="13"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Венеция</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Россия" data-city-code="14"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Москва</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Испания" data-city-code="15"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Бареселона</a></li>
                            </ul>
                            <ul class="e_flat_list e_left">
                                <li><a href="#" class="e_slh e_flag" data-country="Испания" data-city-code="16"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Мадрид</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="Великобритания" data-city-code="17"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Лондон</a></li>
                                <li><a href="#" class="e_slh e_flag" data-country="США" data-city-code="18"><i
                                        style="background-image: url('/i/demo/flags/spain.png')"></i>Нью-Йорк</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <div class="e_earch_buttons_area e_ta_center">
                    <label class="e_tf_checkbox_label e_tf_chb_dark e_white" for="hs_search_aval_only"><input type="checkbox" name="search_aval_only" id="hs_search_aval_only" class="e_f_checkbox" checked="checked">
                        <i class="active"></i><span>поиск только доступных отелей</span></label>
                    <input type="submit" name="submit" value="Искать" class="btn btn_search" />
                </div>
            </form>
        </div>
    </div>
</div>


<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="/js/lib/ax_lightbox.js"></script>
<script src="/js/lib/ax_controls.js"></script>
<script src="/js/lib/ax_form.js"></script>
<script src="/js/ax_rules.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/main.js"></script>

</body>
</html>
