<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/skin.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets: true});
    </script>
</head>
<body class="inner_page hotel_card_page hotels_list book_page static_page">

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->


<div class="main_wrapper">
    <div class="page_header noprint">
        <div class="content_block">
            <div class="wrapper header_content">
                <div class="e_left social_links">
                    <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                       class="btn btn_facebook_white">Facebook</a>
                    <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                    <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                       class="btn btn_vkontakte_white">Vkontakte</a>
                </div>
                <div class="e_left">
                    <ul class="e_floatable e_flat_list page_top_menu">
                        <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                        <li class="sep e_left"></li>
                        <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                        <li class="sep e_left"></li>
                        <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                        <li class="sep e_left"></li>
                        <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                    </ul>
                </div>
                <div class="e_left subsribe_block">
                    <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                       data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
                </div>

                <div class="e_right profile_block">
                    <div class="login_btns">
                        <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                          class="btn btn_book_reg lb_link"
                                                                                                          data-rel="registration_window">Регистрация</a>
                    </div>
                    <div class="profile_btns">
                        <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                         class="btn btn_logout">Выход</a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    <div class="index_book_block noprint">

        <div class="content_block">
            <div class="e_left site_logo"><a href="/"><img src="/i/logo_mini.png" alt="AnyFly"/></a></div>
            <div class="e_phones_block e_ts_dark">
                <div class="e_floatable e_light_blue">
                    <div class="e_left"><b class="e_white"><a href="tel:88008008080" class="phone">8 800 800 80
                                80</a></b><br>из Москвы
                    </div>
                    <div class="e_right"><b class="e_white"><a href="tel:88008008070" class="phone">8 800 800 80
                                70</a></b><br>из
                        России
                    </div>
                </div>
            </div>
            <div class="e_search_form e_ta_left">
                <div class="e_search_tabs">
                    <ul class="e_flat_list e_fs_xl">
                        <li class="hotels e_ta_center e_left e_bliss_bold active" data-ref="/"><span>Отели</span><i></i>
                        </li>
                        <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                        <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                        <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                        <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                    </ul>
                </div>
                <form method="post" action="/italy/rome/">
                    <div class="e_search_form_content">
                        <div class="e_search_line">

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="book_bg"></div>
    </div>


    <div class="index_content_block e_texture_black noprint">
        <div class="content_block">
            <div class="index_wrapper">
                <div class="wrapper_content">

                    <div class="hotel_card_wrapper hotel_pay_numbers_info book_wrapper">
                        <div class="top">
                            <div class="e_left e_back_to noprint"><a
                                    href="/italy/rome/eurostars-international-palace/"
                                    class="e_bl w_icon e_wicon e_back_to_dark"><i></i>Вернуться
                                    на страницу отеля</a></div>
                            <div class="extra_links e_right">
                                <a class="e_wicon e_hotels_print_link_dark light noprint" href="#"
                                   onclick="javascript:window.print()">
                                    <i></i><span class="e_aj">Распечатать</span>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="index_content_block index_static_content_block e_texture_white">
        <div class="content_block">
            <div class="index_wrapper">
                <div class="wrapper_content">
                    <h1>Правила оплаты и возврата денежных средств за отмененные бронирования</h1>

                    <h2>Оплата банковскими картами </h2>

                    <p>Обработка платежей по&nbsp;банковским картам и&nbsp;электронными деньгами обеспечивается
                        компанией RBK Money. К&nbsp;оплате принимаются банковские карты (кредитные и&nbsp;дебетовые):
                        «MasterCard»
                        и&nbsp;«Visa»<br/>
                    <h5>Процесс оплаты</h5>

                    <p>После выбора и&nbsp;бронирования услуг на&nbsp;сайте www.anyfly.ru выбранных Вами услуг&nbsp;Вы
                        будете перенаправлены на&nbsp;сервер компании RBK Money, которая имеет все необходимые лицензии
                        и&nbsp;сертификаты на&nbsp;проведение платежей через Интернет.</p>

                    <p>Напоминаем, что единственным официальным сайтом компании AnyFly™ (торговая марка ООО «Трэвел
                        Эстейт») является сайт www.anyfly.ru.<br/><br/>
                        Оплата по&nbsp;банковским картам в&nbsp;сети Интернет производится путем переадресации на&nbsp;сайт
                        системы электронных платежей RBK Money. Передача данных происходит в&nbsp;защищенном режиме, на&nbsp;сервер
                        компании RBK Money, при котором полностью исключена возможность перехвата информации о&nbsp;вашей
                        карте.
                    </p>

                    <h2>Отмена бронирования и&nbsp;условия возврата денежных средств</h2>

                    <p>Пользователь может отменить произведенное бронирование проживания в&nbsp;гостинице и&nbsp;другие
                        услуги, предоставляемые партнерами сайта. При условии отмены бронирования в&nbsp;каждом
                        конкретном случае возможно вступление в&nbsp;силу штрафных санкций в&nbsp;зависимости от&nbsp;условий,
                        предъявляемых поставщиками услуг, вплоть до&nbsp;применения правила о&nbsp;безотзывности тарифа,
                        при котором штрафы составляют сто процентов платежа за&nbsp;бронирование. Указанные специальные
                        условия бронирования приведены в&nbsp;информации о&nbsp;бронируемых отелях и&nbsp;других
                        бронируемых в&nbsp;системе услугах.</p>
                    <br/>

                    <p>В&nbsp;случае любой отмены бронирования пользователем, подразумевающий возврат денежных средств,
                        система электронных платежей RBK Money удерживает&nbsp;3% от&nbsp;суммы платежа за&nbsp;каждую
                        проведенную транзакцию (оплата/возврат).</p>

                    <p>Факт оплаты забронированных услуг в&nbsp;системе онлайн бронирования AnyFly.ru (<a
                            href="http://www.anyfly.ru">www.anyfly.ru</a>) является фактом согласия с&nbsp;правилами и&nbsp;условиями
                        бронирования, отмены бронирования и&nbsp;возврата денежных средств в&nbsp;случае отмены
                        забронированных услуг.</p>
                </div>
            </div>
        </div>
    </div>

    <? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>










<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/vendor/jquery.jcarousel.min.js"></script>
<script src="/js/vendor/jquery.columnizer.min.js"></script>

<script src="/js/lib/ax_controls.js?<?= time() ?>"></script>
<script src="/js/lib/ax_lightbox.js?<?= time() ?>"></script>
<script src="/js/ax_rules.js?<?= time() ?>"></script>
<script src="/js/lib/ax_form.js?<?= time() ?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?= time() ?>"></script>
<script src="/js/main.js?<?= time() ?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


