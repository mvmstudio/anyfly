<div class="lb-overlay" id="registration_window">
    <div>
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <form method="post" class="lb_form ax_form" id="registration_form" action="/test/reg.php"
                  data-callback="cb_registration_success">
                <h3 class="e_mh_lb e_bliss_light e_ts_white">Регистрация</h3>

                <div class="lb_text">
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_email">E-mail</label>
                        <input type="text" name="email" class="e_text_field" id="reg_email" autocomplete="off"/>

                        <p class="e_tf_annotation e_fs_s">На указанный выше e-mail придет<br/>запрос на подтверждение
                            регистрации</p>
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_label e_fs_x" for="reg_password">Пароль</label>
                        <input type="password" name="password" class="e_text_field" id="reg_password"
                               autocomplete="off"/>
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_password_confirm">Подтверждение пароля</label>
                        <input type="password" name="password_confirm" class="e_text_field" id="reg_password_confirm"/>
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="reg_promo_code">Промо-код</label>
                        <input type="text" name="promo_code" class="e_text_field" id="reg_promo_code"/>
                        <i class="discount"></i>
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_checkbox_label" for="reg_jur_lico"><input type="checkbox" name="jur_lico"
                                                                                     id="reg_jur_lico"
                                                                                     class="e_f_checkbox"/><span>Юридическое лицо</span></label>
                    </div>
                </div>

                <div class="err_handler"></div>


                <div class="buttons_area center">
                    <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_reg_xl"/>

                    <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="lb-overlay" id="login_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <form method="post" class="lb_form ax_form" id="login_form" action="/test/login.php"
                  data-callback="cb_login_success">
                <h3 class="e_mh_lb e_bliss_light e_ts_white">Авторизация</h3>

                <div class="lb_text">
                    <div class="e_input_line ">
                        <label class="e_tf_label e_fs_x" for="login_email">E-mail</label>
                        <input type="text" name="email" class="e_text_field" id="login_email"/>
                    </div>
                    <div class="e_input_line e_input_group">
                        <label class="e_tf_label e_fs_x" for="login_password">Пароль</label>
                        <input type="password" name="password" class="e_text_field" id="login_password"/>
                    </div>
                    <div class="e_input_line">
                        <label class="e_tf_checkbox_label" for="login_remember_me"><input type="checkbox"
                                                                                          name="remember_me"
                                                                                          id="login_remember_me"
                                                                                          class="e_f_checkbox"/><span>Запомнить меня</span></label>
                    </div>
                </div>

                <div class="err_handler"></div>

                <div class="buttons_area center">
                    <input type="submit" name="submit" value="Зарегистрироваться" class="btn btn_book_enter_xl"/>

                    <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="lb-overlay" id="subscribe_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <form method="post" class="lb_form ax_form" id="subscribe_form" action="#"
                  data-callback="cb_login_success">
                <h3 class="e_mh_lb e_bliss_light e_ts_white">Подписаться на новости</h3>

                <div class="lb_text">
                    <div class="e_input_line ">
                        <label class="e_tf_label e_fs_x" for="login_email">E-mail</label>
                        <input type="text" name="email" class="e_text_field" id="subscribe_email"/>
                    </div>
                </div>

                <div class="err_handler"></div>

                <div class="buttons_area center">
                    <input type="submit" value="Подписаться" class="btn btn_subscribe lb_link"
                           data-rel="subscribe_success_window">

                    <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="lb-overlay" id="subscribe_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Поздравляем!</h3>

            <div class="lb_text">
                <p>Вы успешно подписались на нашу рассылку.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>
<div class="lb-overlay" id="registration_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Спасибо!</h3>

            <div class="lb_text">
                <p>На ваш адрес отправлено письмо<br>с подтверждением регистрации.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>