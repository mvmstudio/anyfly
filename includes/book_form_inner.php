<div class="index_book_block noprint">

    <div class="content_block">
        <div class="e_left site_logo"><a href="/"><img src="/i/logo_mini.png" alt="AnyFly"/></a></div>
        <div class="e_phones_block e_ts_dark">
            <div class="e_floatable e_light_blue">
                <div class="e_left"><b class="e_white"><a href="tel:88008008080" class="phone">8 800 800 80
                    80</a></b><br>из Москвы
                </div>
                <div class="e_right"><b class="e_white"><a href="tel:88008008070" class="phone">8 800 800 80
                    70</a></b><br>из
                    России
                </div>
            </div>
        </div>
        <div class="e_search_form e_ta_left">
            <div class="e_search_tabs">
                <ul class="e_flat_list e_fs_xl">
                    <li class="hotels e_ta_center e_left e_bliss_bold active" data-ref="/"><span>Отели</span><i></i></li>
                    <li class="rent_car e_ta_center e_left e_bliss_bold "><span>Прокат авто</span><i></i></li>
                    <li class="transfers e_ta_center e_left e_bliss_bold"><span>Трансферы</span><i></i></li>
                    <li class="avia e_ta_center e_left e_bliss_bold"><span>Авиабилеты</span><i></i></li>
                    <li class="ensur e_ta_center e_left e_bliss_bold "><span>Страхование</span><i></i></li>
                </ul>
            </div>
            <form method="post" action="/italy/rome/">
                <div class="e_search_form_content">
                    <div class="e_search_line">
                        <div class="e_left name_search">
                            <i></i>
                            <label class="e_tf_label e_bold" for="hs_search_name">Страна, город или отель</label>
                            <input class="e_text_field e_fs_x e_bold" name="name" value="Рим, Италия"
                                   id="hs_search_name" autocomplete="off" tabindex="1"/>
                            <input type="hidden" id="__city_code" name="city_code" value="1"/>
                        </div>
                        <div class="e_left date_checkin">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkin">Заезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkin_str" value=""
                                   id="hs_date_checkin" autocomplete="off" tabindex="2"/>
                            <input type="text" name="date_checkin" id="__date_checkin" class="visuallyhidden"
                                   value="<?=date('Y/m/d', strtotime('+14 days'))?>"/>
                        </div>
                        <div class="e_left e_date_days_count e_ta_center">
                            <b>0</b><span>ночей</span>
                            <input type="hidden" id="__nights_count" name="nights_count" value="0"/>
                        </div>
                        <div class="e_left date_checkout">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_date_checkout">Выезд</label>
                            <input class="e_date_input e_text_field e_fs_x e_bold" name="date_checkout_str" value=""
                                   id="hs_date_checkout" autocomplete="off" tabindex="3"/>
                            <input type="text" name="date_checkout" id="__date_checkout" class="visuallyhidden"
                                   value="<?=date('Y/m/d', strtotime('+31 days'))?>"/>
                        </div>
                        <div class="e_left e_sl_count_selector adults_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_adults_count">Взрослых</label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="adults_count"
                                   id="hs_adults_count" autocomplete="off" tabindex="4" value="2"/>
                        </div>
                        <div class="e_left e_sl_count_selector children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_children_count">Детей <span
                                    class="e_fs_s e_nobold">2–15 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="children_count"
                                   id="hs_children_count" autocomplete="off" tabindex="5"/>
                        </div>
                        <div class="e_left e_sl_count_selector l_children_count">
                            <label class="e_tf_label e_bold e_ta_left" for="hs_l_children_count"><span
                                    class="e_fs_s e_nobold">до 2 лет</span></label>
                            <input class="e_count_field e_text_field e_fs_x e_bold" name="l_children_count"
                                   id="hs_l_children_count" autocomplete="off" tabindex="6"/>
                        </div>
                        <div class="e_left e_sl_submit_area">
                            <input type="submit" name="submit" value="Обновить" class="btn btn_refresh"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="book_bg"></div>
</div>