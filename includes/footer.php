<div class="page_useful_links noprint">
    <div class="content_block">
        <table>
            <tr>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">О компании</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Контактная информация</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Реквизиты</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Пресс-кит</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Корпоративным клиентам</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Частые вопросы</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Заявка на договор</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Помощь</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Условия использования сервиса</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Как забронировать отель?</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Частые вопросы</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Агентствам</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Войти</a></li>
                        <li><a href="#" class="e_sl e_light_blue ">Зарегистрироваться</a></li>
                    </ul>
                </td>
                <td>
                    <ul class="e_flat_list">
                        <li><a href="#" class="e_sl e_light_blue e_bold">Партнерская программа</a></li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="page_footer">
    <div class="content_block">
        <div class="footer_content e_white e_floatable">
            <div class="e_left copyright e_fs_11">Система онлайн бронирования AnyFly™<br>AnyFly™ — торговая марка
                ООО
                «Трэвел Эстейт»
            </div>
            <div class="e_left pay_methods"><a href="http://visa.com.ru/main.jsp" target="_blank"
                                               rel="nofollow"><img
                    src="/i/card_visa.png" alt="VISA"/></a><a
                    href="http://www.mastercard.com/ru/consumer/index.html"
                    target="_blank" rel="nofollow"><img
                    src="/i/card_master_card.png" alt="Master Card"/></a></div>
            <div class="e_right inxl e_fs_11 e_ta_right">
                <a href="http://www.inxl.ru" target="_blank" class="e_white">Разработка сайта<br>интерактивное
                    агентство
                    <img src="/i/inxl.png" alt="inXL" class="noprint"/><img src="/i/inxl_mono.png" alt="inXL" class="printonly"/></a>
            </div>
        </div>
    </div>
</div>