<?
if($_POST) {
    header('Location: ' . $_SERVER['REQUEST_URI']);
    die();
}

header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets:true});
    </script>
</head>
<body class="inner_page hotels_list">
<input type="hidden" name="currency" value="RUB"/>

<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->


<div class="main_wrapper">
<div class="printonly">
    <h3 class="e_mh" id="__location" style="margin-bottom: 9px">Рим, Италия</h3>
    <h4 class="e_mh" id="__days">18 марта — 4 апреля (17 ночей)</h4>
    <h4 class="e_mh" id="__people">2 взрослых, 3 детей</h4>
    <h4 class="e_mh" id="__nights_count_text" style="display: none">17 ночей</h4>

</div>
<div class="page_header noprint">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                   class="btn btn_facebook_white">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                   class="btn btn_vkontakte_white">Vkontakte</a>
            </div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left subsribe_block">
                <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                   data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
            </div>

            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link" data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                     class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<? include('includes/book_form_inner.php') ?>


<div class="index_content_block e_texture_white">
<div class="content_block">
<div class="index_wrapper">
<div class="wrapper_content">
<div class="e_left filters_column noprint">
<form method="post" action="/test/search.php">
<div class="filters_block">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white invisible h_count">Отелей не найдено</h3>

    <p><label class="e_tf_checkbox_label fc_immediate_confirm"
              for="fc_immediate_confirm"><input type="checkbox" name="immediate_confirm"
                                                id="fc_immediate_confirm"
                                                class="e_f_checkbox">
        <span class="e_fs_s">Немедленное подтверждение</span><sup><a href="#"
                                                               class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup></label>
    </p>
</div>
<div class="filters_block fb_name">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white"><label for="fc_hotel_name">Название отеля<sup><a href="#"
                                                                                                    class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup></label></h3>

    <p class="e_search_field"><i></i><input class="e_text_field e_fs_m " name="hotel_name" value="" id="fc_hotel_name"
                                            autocomplete="off" maxlength="300"></p>
</div>
<div class="filters_block fb_stars">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white"><label for="fc_hotel_name">Количество звёзд<sup><a href="#"
                                                                                                      class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup></label></h3>

    <div class="fc_stars_filters">
    </div>
</div>
<div class="filters_block fb_prices_range">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white">Стоимость</h3>

    <div class="fc_price">
        <select name="currency" id="currencySelector" style="width: 90px" class="invisible">
            <option value="RUB">RUB</option>
            <option value="USD">USD</option>
            <option value="EUR">EUR</option>
        </select>

        <div class="e_price_slider invisible">
            <div class="inputs">
                <input class="e_text_field e_fs_xl from e_ta_right e_bold e_left" name="price_from" value="10000"
                       id="fc_price_from" autocomplete="off" maxlength="10" >
                <input class="e_text_field e_fs_xl to e_ta_right e_bold e_right" name="price_to" value="100000"
                       id="fc_price_to" autocomplete="off" maxlength="10" >

                <div class="clearfix"></div>
            </div>
            <div class="slider_area">
                <div class="slider" id="fc_prices_range"></div>
            </div>

        </div>
    </div>
</div>
<div class="filters_block fb_locations">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white e_vis_toggler" data-toggler="fc_locations">Расположение<sup><a
            href="#"
            class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup><i></i></h3>

    <div class="fc_locations">
        <ul class="e_flat_list fc_options_list">

        </ul>

        <h4 class="e_bliss_light e_fs_xl e_ts_white e_vis_toggler" data-toggler="fc_zones">Районы<sup><a
                href="#"
                class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup><i></i></h4>

        <div>
            <ul class="e_flat_list fc_options_list fc_zones_list"></ul>
        </div>

    </div>
</div>

<div class="filters_block fb_facilities">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white e_vis_toggler" data-toggler="fc_locations">Дополнительно<br>в
        отеле<sup><a
                href="#"
                class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup><i></i></h3>

    <div class="fc_facilities">
        <ul class="e_flat_list fc_options_list">

        </ul>
    </div>
</div>

<div class="filters_block fb_food_types">
    <h3 class="e_bliss_light e_fs_xxxl e_ts_white e_vis_toggler" data-toggler="fc_locations">Питание<sup><a
            href="#"
            class="e_aj">?</a><b class="e_bs_dark e_fs_m e_arial">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. </b></sup><i></i></h3>

    <div class="fc_food_types">
        <ul class="e_flat_list fc_options_list">

        </ul>
    </div>
</div>
</form>
</div>
<div class="e_left items_list_column">
    <div class="controls_row noprint">
        <div class="sorting_options e_fs_x e_left">
            <ul class="e_flat_list" id="hotelsSorting">
                <li class="price e_left active"><span>Цена<i>&#8595;</i></span></li>
                <li class="stars e_left"><span>Звезды<i>&#8595;</i></span></li>
                <li class="rating e_left"><span>Рейтинг<i>&#8595;</i></span></li>
                <li class="a_z e_left"><span>A-Z</span></li>
                <li class="z_a e_left"><span>Z-A</span></li>
            </ul>
        </div>
        <div class="extra_links e_left">
            <a class="e_wicon e_hotels_map_link_dark map_lb_link" href="#" data-rel="map_window">
                <i></i><span class="e_aj">Отели на карте</span>
            </a>
            <a class="e_wicon e_hotels_share_link_dark " href="#">
                <i></i><span class="e_aj">Поделиться с друзьями</span>
            </a>
            <div class="e_share_block e_bs_dark">
                <a href="#" class="lb_close lb_hide"></a>
                <? $link = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] ?>
                <ul class="e_flat_list e_floatable">
                    <li class="e_left"><a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&amp;t.s=1&amp;st.comments=AnyFly.ru%20—%20Рим,%20Италия&amp;st._surl=<?=$link?>" target="_blank" title="Поделиться в Одноклассниках" class="btn btn_od_share">Поделиться в Одноклассниках</a></li>
                    <li class="e_left"><a href="http://vkontakte.ru/share.php?url=<?=$link?>" target="_blank" title="Поделиться вКонтакте" class="btn btn_vk_share">Поделиться вКонтакте</a></li>
                    <li class="e_left"><a href="http://www.facebook.com/sharer.php?u=<?=$link?>" target="_blank" title="Поделиться в Facebook" class="btn btn_fb_share">Поделиться на Facebook</a></li>
                    <li class="e_left"><a href="http://twitter.com/share?text=AnyFly.ru%20—%20Рим,%20Италия%20—&amp;url=<?=$link?>" title="Добавить в Twitter" target="_blank" class="btn btn_tw_share">Твитнуть</a></li>
                    <li class="e_left"><a href="https://plus.google.com/share?url=<?=$link?>" title="Поделиться в Google Plus" target="_blank" class="btn btn_gg_share">Поделиться в Google Plus</a></li>
                </ul>
                <input type="text" class="e_text_field e_fs_m" id="share_link" value="<?=$link?>"/>
                <a href="#" class="btn btn_copy_s" id="copy_share_btn">Скопировать</a>
            </div>
            <a class="e_wicon e_hotels_print_link_dark"  href="#" onclick="javascript:window.print()">
                <i></i><span class="e_aj">Распечатать</span>
            </a>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="hotels_list" id="hotels_list">

    </div>
    <div class="hotels_bottom e_ts_white invisible">
        <div class="e_left">Показано <span class="show_total">0</span> <i>отелей</i> из <span class="total">0</span>
        </div>
        <div class="e_right"><a href="#" class="e_ta_right e_aj e_scroll_to_top noprint">Подняться наверх</a></div>
        <a href="#" class="btn btn_show_more noprint">Показать ещё</a>
    </div>
</div>
<div class="clearfix"></div>
</div>


</div>
</div>
</div>
<? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>
<div class="lb-overlay" id="map_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Отели на карте</h3>
            <h4 class="e_fs_m e_nobold"></h4>

            <div class="map_area" id="map_window_map" style="height: 508px; width: 702px;">

            </div>
        </div>
    </div>
</div>


<script id="hotelCard" type="text/html">
    {{#list}}
    {{#BANNER}}
    <div class="e_hotel_item e_hotel_item_b noprint">
        <a href="{{BANNER.LINK}}"{{#BANNER.TARGET}} target="{{BANNER.TARGET}}"{{/BANNER.TARGET}}><img
            src="{{BANNER.PICTURE}}" alt="{{BANNER.NAME}}"/></a>
    </div>
    {{/BANNER}}
    {{#HOTEL}}
    <div class="e_hotel_item e_bs_dark {{HOTEL.SPECIAL_OFFER_ITEM_CLASS}}" data-hid="{{HOTEL.ID}}">
        {{#HOTEL.SPECIAL_OFFER_ITEM_CLASS}}<i class="special_offer"></i>{{/HOTEL.SPECIAL_OFFER_ITEM_CLASS}}
        <a href="{{HOTEL.LINK}}" class="btn btn_print_hcard pagePrintBtn">Распечатать</a>

        <div class="e_left photo"><a href="{{HOTEL.LINK}}"><img src="{{HOTEL.PICTURE}}" alt="{{HOTEL.NAME}}"/></a></div>
        <div class="e_left hotel_info">
            <div class="top e_fs_s">
                <div class="e_stars_line e_left">{{HOTEL.STARS}}</div>
                <div class="address e_hotel_map_focus e_left " data-rel="map_window" data-hid="{{HOTEL.ID}}">
                    <i></i><span class="e_aj">{{HOTEL.ADDRESS}}</span>
                </div>
                {{#HOTEL.RATING}}
                <div class="rating e_right e_ta_right">Рейтинг: <b>{{HOTEL.RATING}} / 10</b></div>
                {{/HOTEL.RATING}}
                <div class="clearfix"></div>
            </div>
            <div class="middle e_floatable e_bliss e_fs_xxxl">
                <a href="{{HOTEL.LINK}}" class="e_sl">{{HOTEL.NAME}}</a>
            </div>
            <div class="bottom e_floatable e_fs_s">
                <div class="e_left">{{#HOTEL.FEEDBACK_COUNT}}<a href="{{HOTEL.LINK}}#feedback" class="e_sl noprint">{{HOTEL.FEEDBACK_COUNT}}</a>{{/HOTEL.FEEDBACK_COUNT}}{{#HOTEL.BOOK_INFO}}<span>{{HOTEL.BOOK_INFO}}</span>{{/HOTEL.BOOK_INFO}}
                </div>
                <div class="e_right e_ta_right"><span class="num_left {{HOTEL.SPECIAL_OFFER_ROOMS_LEFT_CLASS}}">{{HOTEL.ROOMS_LEFT}}</span>
                </div>
            </div>
        </div>
        <div class="e_right e_ta_right price_info">
            <div class="price e_bliss_bold e_fs_big"><i>{{HOTEL.BEST_PRICE_STR}}</i><span
                    class="{{HOTEL.CURRENCY.CLASS}}">{{HOTEL.CURRENCY.VALUE}}</span>
            </div>
            <div class="nights e_fs_s">За <i></i></div>
            <div class="book">
                <a href="{{HOTEL.BOOK_LINK}}" class="btn {{HOTEL.BTN_CLASS}}">Забронировать</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {{/HOTEL}}
    {{/list}}
</script>


<script id="hotelInfobox" type="text/html">
    {{#list}}
    {{#HOTEL}}
    <div class="infobox-wrapper " data-hid="{{HOTEL.ID}}">
        <div class="infobox_content e_bs_dark" id="h_infobox_{{HOTEL.ID}}">
            <div class="ic_wrapper">
                <div class="top">
                    <div class="e_stars_line e_left">{{HOTEL.STARS}}</div>
                    {{#HOTEL.RATING}}<a href="{{HOTEL.LINK}}" class="e_rating_mini">{{HOTEL.RATING}}</a>{{/HOTEL.RATING}}
                    <div class="clearfix"></div>
                </div>
                <h4 class="e_mh e_bliss_medium"><a href="{{HOTEL.LINK}}">{{HOTEL.NAME}}</a></h4>

                <div class="middle">
                    <div class="e_left location">{{HOTEL.LOCATION}}</div>
                    {{#HOTEL.FEEDBACK_COUNT}}
                    <div class="e_right feedback_count"><a href="{{HOTEL.LINK}}#feedback" class="e_bl noprint">{{HOTEL.FEEDBACK_COUNT}}</a>
                    </div>
                    {{/HOTEL.FEEDBACK_COUNT}}

                    <div class="e_right"><a href="{{HOTEL.LINK}}#description" class="e_bl">Описание</a></div>
                    <div class="clearfix"></div>
                </div>

                <div class="photos e_floatable">
                    <img src="/i/demo/hib1.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib2.jpg" alt="" class="e_left">
                    <img src="/i/demo/hib3.jpg" alt="" class="e_left">
                </div>
                <div class="buttons_area">
                    <div class="e_left price">
                        <div class="price e_bliss_medium e_fs_xxxl"><i>{{HOTEL.BEST_PRICE_STR}}</i><span
                                class="{{HOTEL.CURRENCY.CLASS}}">{{HOTEL.CURRENCY.VALUE}}</span></div>
                        <div class="nights e_fs_s">За <i></i></div>
                    </div>
                    <div class="e_right book">
                        <a href="{{HOTEL.BOOK_LINK}}" target="_blank" class="btn btn_book_s_blue">Забронировать</a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="pipe"></div>
        </div>
    </div>
    {{/HOTEL}}
    {{/list}}
</script>


<script id="filterStars" type="text/html">
    {{#list}}
    <label class="e_tf_checkbox_label e_tf_checkbox_label_top fc_stars"
           for="fc_stars_{{STAR}}"><input type="checkbox" name=""
                                   id="fc_stars_{{STAR}}"
                                   class="e_f_checkbox"{{#CHECKED}} checked="checked"{{/CHECKED}}/>
        <span>{{STAR}}</span></label>
    {{/list}}
</script>


<script id="filterOptionsList" type="text/html">
    {{#list}}
    <li><label class="e_tf_checkbox_label">
        <input type="checkbox" name="" value="{{ID}}" class="e_f_checkbox"{{#CHECKED}} checked="checked"{{/CHECKED}}>
        <span>{{NAME}}</span></label>
    </li>
    {{/list}}
</script>


<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>

<script src="/js/lib/ax_controls.js?<?=time()?>"></script>
<script src="/js/lib/ax_lightbox.js?<?=time()?>"></script>
<script src="/js/ax_rules.js?<?=time()?>"></script>
<script src="/js/lib/ax_form.js?<?=time()?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


