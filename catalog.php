<? header('Content-Type: text/html; charset=utf-8');
$vk = '3447916';
if ($_SERVER['SERVER_NAME'] == 'anyfly.dev') $vk = '3447858';

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Система бронирования AnyFly</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/css/skin.css">
    <link rel="stylesheet" href="/css/chosen.css">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/buttons.css">
    <link rel="stylesheet" href="/css/widgets.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

    <script type="text/javascript">
        VK.init({apiId: <?=$vk?>, onlyWidgets: true});
    </script>
</head>
<body class="inner_page hotel_card_page hotels_list catalog_page">
<input type="hidden" name="currency" value="RUB"/>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->


<div class="main_wrapper">
<div class="printonly">
    <h3 class="e_mh" id="__location" style="margin-bottom: 9px">Рим, Италия</h3>
    <h4 class="e_mh" id="__days">18 марта — 4 апреля (17 ночей)</h4>
    <h4 class="e_mh" id="__people">2 взрослых, 3 детей</h4>
    <h4 class="e_mh" id="__nights_count_text" style="display: none">17 ночей</h4>

</div>
<div class="page_header noprint">
    <div class="content_block">
        <div class="wrapper header_content">
            <div class="e_left social_links">
                <a href="http://www.facebook.com/pages/AnyFlyru/403161666487" target="_blank"
                   class="btn btn_facebook_white">Facebook</a>
                <a href="https://twitter.com/#!/anyfly_ru" target="_blank" class="btn btn_twitter_white">Twitter</a>
                <a href="http://vkontakte.ru/anyfly_club" target="_blank"
                   class="btn btn_vkontakte_white">Vkontakte</a>
            </div>
            <div class="e_left">
                <ul class="e_floatable e_flat_list page_top_menu">
                    <li class="active e_left"><a href="/" class="e_bl e_white">Города и страны</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/news/" class="e_bl e_white">Новости</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/offers/" class="e_bl e_white">Спецпредложения</a></li>
                    <li class="sep e_left"></li>
                    <li class="e_left"><a href="/guide/" class="e_bl e_white">Путеводитель</a></li>
                </ul>
            </div>
            <div class="e_left subsribe_block">
                <a class="e_white e_wicon e_subscribe_link_white lb_link" href="#"
                   data-rel="subscribe_window"><i></i><span class="e_aj">Подписаться на новости</span></a>
            </div>

            <div class="e_right profile_block">
                <div class="login_btns">
                    <a href="#" class="btn btn_book_enter lb_link" data-rel="login_window">Вход</a><a href="#"
                                                                                                      class="btn btn_book_reg lb_link"
                                                                                                      data-rel="registration_window">Регистрация</a>
                </div>
                <div class="profile_btns">
                    <a href="/profile/" class="btn btn_profile">Личный кабинет</a><a href="#"
                                                                                     class="btn btn_logout">Выход</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<? include('includes/book_form_inner.php') ?>


<div class="index_content_block e_texture_black cat_top">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">

                <div class="hotel_card_wrapper">
                    <div class="top">
                        <div class="e_left e_back_to noprint"><a href="/"
                                                                 class="e_bl w_icon e_wicon e_back_to_dark"><i></i>Вернуться
                                на главную</a></div>

                        <h2 class="e_bliss_medium e_ta_center">Каталог стран</h2>


                        <div class="clearfix"></div>
                    </div>

                </div>

                <div class="clearfix"></div>
            </div>


        </div>
    </div>
</div>
<div class="index_promo_block e_texture_gray">
    <div class="content_block">
        <div class="index_wrapper">

            <div class="wrapper_content">
                <div class="e_block_spec e_bs_dark e_left">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_1.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Венецианский карнавал 2013</a>
                            </h4>

                            <p class="e_price e_fs_xxxl">от 2 409 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="e_block_spec e_bs_dark e_left">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_2.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Миланский мебельный салон</a>
                            </h4>

                            <p class="e_price e_fs_xxxl">от 1 200 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="e_block_spec e_bs_dark e_right">
                    <div class="img">
                        <a href="#"><img src="/i/demo/offer_3.jpg" alt="Венецианский карнавал"/></a>

                        <div class="annotation e_ta_left">
                            <h4 class="e_mh e_bliss_bold"><a href="#" class="e_blh">Карнавал в Ницце</a></h4>

                            <p class="e_price e_fs_xxxl">от 6 685 <span class="e_ruble">a</span></p>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>

<div class="hotel_card_description e_texture_white" id="description">
    <div class="content_block">
        <div class="index_wrapper">
            <div class="wrapper_content">
                <div class="catalog_left e_left">
                    <div class="static_text"><p>Чем мы&nbsp;руководствуемся при бронировании отеля, помимо
                            звездности и&nbsp;расположения? Конечно, стоимостью гостиницы. Островок гарантирует
                            по-настоящему низкие цены на&nbsp;бронирование отелей. Как нам это удается? Все просто&nbsp;—
                            мы&nbsp;не&nbsp;берем с&nbsp;клиентов комиссий и&nbsp;работаем по&nbsp;прямой
                            договоренности с&nbsp;отелями, предоставляющими нам лучшие условия. С&nbsp;нашей помощью
                            можете не&nbsp;только забронировать отель, но&nbsp;и&nbsp;получить помощь и&nbsp;необходимую
                            информацию в&nbsp;любой момент поездки и&nbsp;по&nbsp;возвращению из&nbsp;нее. Чем мы&nbsp;руководствуемся
                            при бронировании отеля, помимо звездности и&nbsp;расположения? Конечно, стоимостью
                            гостиницы. Островок гарантирует по-настоящему низкие цены.</p>

                        <p>Почему бронирование отелей онлайн удобнее? Во-первых, Островок предоставляет вам отличный
                            сервис по&nbsp;бронированию гостиниц. Мы&nbsp;делаем все, чтобы выбор отеля проходил
                            быстро и&nbsp;легко. Наш отдел по&nbsp;работе с&nbsp;клиентами круглосуточно
                            предоставляет всю необходимую информацию о&nbsp;гостиницах и&nbsp;помогает в&nbsp;решении
                            любых вопросов, связанных с&nbsp;бронированием отелей. Во-вторых, в&nbsp;нашей базе
                            более 130 тысяч отелей в&nbsp;200 странах мира. Вы&nbsp;с&nbsp;легкостью сможете выбрать
                            и&nbsp;забронировать отель всего за&nbsp;пару минут и&nbsp;несколько кликов.</p></div>

                    <div class="alphabet">
                        <ul class="e_flat_list">
                            <li><a href="#">А</a></li>
                            <li><a href="#">Б</a></li>
                            <li><a href="#">В</a></li>
                            <li><a href="#">Г</a></li>
                            <li><a href="#">Д</a></li>
                            <li><a href="#">Е</a></li>
                            <li><a href="#">Ж</a></li>
                            <li><a href="#">З</a></li>
                            <li><a href="#">И</a></li>
                            <li><a href="#">Й</a></li>
                            <li><a href="#">К</a></li>
                            <li><a href="#">Л</a></li>
                            <li><a href="#">М</a></li>
                            <li><a href="#">Н</a></li>
                            <li><a href="#">О</a></li>
                            <li><a href="#">П</a></li>
                            <li><a href="#">Р</a></li>
                            <li class="active"><a href="#">С</a></li>
                            <li><a href="#">Т</a></li>
                            <li><a href="#">У</a></li>
                            <li><a href="#">Ф</a></li>
                            <li><a href="#">Х</a></li>
                            <li><a href="#">Ц</a></li>
                            <li><a href="#">Ч</a></li>
                            <li><a href="#">Ш</a></li>
                            <li><a href="#">Э</a></li>
                            <li><a href="#">Ю</a></li>
                            <li><a href="#">Я</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="countries_list e_bliss">
                        <div class="header e_floatable">
                            <div class="e_left">Страна</div>
                            <div class="e_right">Города</div>
                        </div>
                        <div class="cont">
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Северные Марианские острова</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сент-Китс и Невис</a></div>
                                <div class="e_right e_ta_right">22</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Самоа</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Саудовская Аравия</a></div>
                                <div class="e_right e_ta_right">12</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сенегал</a></div>
                                <div class="e_right e_ta_right">4</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сербия</a></div>
                                <div class="e_right e_ta_right">5</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сейшелы</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сингапур</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Словакия</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Словения</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сент-Люсия</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сент-Винсент и Гренадины</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Свазиленд</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">Сирийская Арабская Республика</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                            <div class="row e_floatable">
                                <div class="e_left"><a href="/catalog_country.php">США</a></div>
                                <div class="e_right e_ta_right">2</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog_right e_left">
                    <div class="e_block e_bs_dark e_block_subscribe e_ta_left e_block_catalog_sub">
                        <div class="top"></div>
                        <div class="form">
                            <h5 class="e_mh e_bliss_light"><label for="subscribe_email">Подписаться на каталог</label></h5>

                            <form method="get" id="subscribe_form">
                                <input type="text" name="email" class="e_text_field e_tf_mini" id="subscribe_email" placeholder="Ваша электронная почта">
                                <input type="submit" value="Подписаться" class="btn btn_subscribe_wide lb_link btn_cat_subscribe" data-rel="subscribe_success_window">
                            </form>
                        </div>
                    </div>
                    <div class="social_block">
                        <div class="fb-like-box" data-href="https://www.facebook.com/anyfly.ru" data-width="185" data-height="412"
                             data-show-faces="true" data-stream="false" data-border-color="aaaaaa" data-header="false"></div>
                    </div>
                    <div class="social_block">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?79"></script>

                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode:0, width:"185", height:"415"}, 31694316);
                        </script>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>
<? include('includes/footer.php') ?>
</div>


<? include('includes/main_windows.php') ?>
<div class="lb-overlay" id="feedback_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>

        <div class="lb_content e_ta_left">
            <a href="#" class="e_rating_mini big noreact">Рейтинг отеля <i>8,9</i></a>

            <form method="post" action="/test/fb_post.php" class="ax_form" data-callback="on_feedback_success">
                <div class="top e_texture_gray">
                    <h3 class="e_mh_lb e_bliss_light e_ts_white e_ta_center">The Inn at the Roman Forum Small Luxury
                        Hotels</h3>

                    <div class="sliders">
                        <div class="slider">
                            <div class="label e_left"><label for="fb_cleanness">Чистота</label></div>
                            <div class="slider_wrapper e_left">
                                <div class="slider_container"></div>
                            </div>
                            <div class="slider_alt e_left"><input type="text" name="cleanness" class="e_text_field"
                                                                  id="fb_cleanness" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_comfort">Комфорт</label></div>
                            <div class="slider_wrapper e_left">
                                <div class="slider_container"></div>
                            </div>
                            <div class="slider_alt e_left"><input type="text" name="comfort" class="e_text_field"
                                                                  id="fb_comfort" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_location">Расположение</label></div>
                            <div class="slider_wrapper e_left">
                                <div class="slider_container"></div>
                            </div>
                            <div class="slider_alt e_left"><input type="text" name="location" class="e_text_field"
                                                                  id="fb_location" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_service">Услуги</label></div>
                            <div class="slider_wrapper e_left">
                                <div class="slider_container"></div>
                            </div>
                            <div class="slider_alt e_left"><input type="text" name="service" class="e_text_field"
                                                                  id="fb_service" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slider">
                            <div class="label e_left"><label for="fb_personal">Персонал</label></div>
                            <div class="slider_wrapper e_left">
                                <div class="slider_container"></div>
                            </div>
                            <div class="slider_alt e_left"><input type="text" name="personal" class="e_text_field"
                                                                  id="fb_personal" value="0"/></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="bottom e_texture_white">
                    <div class="comment_area">
                        <label for="fb_comment">Комментарий</label>
                        <textarea class="e_textarea" name="comment" id="fb_comment"></textarea>
                    </div>
                    <div class="buttons_area center">
                        <input type="submit" name="submit" value="Отправить" class="btn btn_send" maxlength="3"
                               tabindex="10"/>

                        <div class="ortext">или <a href="#" class="lb_hide e_bl">отменить</a></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="lb-overlay" id="feedback_success_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_center">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Спасибо!</h3>

            <div class="lb_text">
                <p>Мы учли ваш голос.</p>
            </div>

            <div class="buttons_area">
                <a class="btn btn_continue lb_hide">Продолжить</a>
            </div>
        </div>
    </div>
</div>


<div class="lb-overlay" id="map_window">
    <div class="">
        <a href="#" class="lb_close lb_hide"></a>


        <div class="lb_content e_ta_left">
            <h3 class="e_mh_lb e_bliss_light e_ts_white">Отели на карте</h3>
            <h4 class="e_fs_m e_nobold"></h4>

            <div class="map_area" id="map_window_map" style="height: 508px; width: 702px;">

            </div>
        </div>
    </div>
</div>


<script id="feedbackItem" type="text/html">
    {{#list}}
    <div class="e_hotel_feedback">
        <div class="author e_left">
            <div class="photo e_left"><img src="{{PHOTO}}" alt="{{NAME}}"/></div>
            <div class="creditionals e_left">
                <div class="date">{{DATE}}</div>
                <div class="name e_fs_xl e_bold e_ts_white">{{NAME}}</div>
                {{#POSITION}}
                <div class="position">{{POSITION}}</div>
                {{/POSITION}}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="text e_left e_bs_dark e_fs_x">
            <i class="pipe"></i>
            {{#RATING}}<a href="#" class="e_rating_mini maxi noreact"><i>{{RATING}}</i></a>{{/RATING}}
            <div class="wrap">
                <p {{#RATING}}class="wrating"{{/RATING}}>{{{TEXT}}}</p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {{/list}}
</script>


<div class="lb_bg"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>

<script type="text/javascript"
        src="//maps.googleapis.com/maps/api/js?key=AIzaSyDh8zliJhNC1-p1f3kORNWgDQajbekGBTg&amp;sensor=false"></script>
<script type="text/javascript"
        src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script>window.jQuery.Widget || document.write('<script src="../js/vendor/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/vendor/jquery.ui.datepicker-ru.js"></script>
<script src="//raw.github.com/andris9/jStorage/master/jstorage.js"></script>
<script src="/js/vendor/chosen.jquery.min.js"></script>
<script src="/js/vendor/mustache.js"></script>
<script src="/js/vendor/md5-min.js"></script>
<script src="/js/vendor/ZeroClipboard.min.js"></script>
<script src="/js/vendor/jquery.printPage.js"></script>
<script src="/js/vendor/jquery.jcarousel.min.js"></script>
<script src="/js/vendor/jquery.columnizer.min.js"></script>

<script src="/js/lib/ax_controls.js?<?= time() ?>"></script>
<script src="/js/lib/ax_lightbox.js?<?= time() ?>"></script>
<script src="/js/ax_rules.js?<?= time() ?>"></script>
<script src="/js/lib/ax_form.js?<?= time() ?>"></script>

<script src="/js/scope_objects.js"></script>
<script src="/js/plugins.js?<?= time() ?>"></script>
<script src="/js/main.js?<?= time() ?>"></script>
<script src="/js/hotels_map.js"></script>
</body>
</html>


